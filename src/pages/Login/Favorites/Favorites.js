import React from "react";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import SubCategoryData from "../../../SampleData/Category/SubCategoryData";
import {MowProductList} from "../../../components/ui/AppSpecifics/MowProductList";

export default class Favorites extends React.Component {

    state = {
        favoriteData: SubCategoryData,
        favoriteDataListKey: 0,
    };

    render() {

        let {favoriteData} = this.state;

        return(

            <MowContainer
                title={mowStrings.favoritesPage.title}>

                <MowProductList
                    boxView={false}
                    listData={favoriteData}/>

            </MowContainer>

        )

    }

}
