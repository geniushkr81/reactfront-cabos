import React from "react";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {borderStyle, fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {View, Text} from "react-native";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {navigate} from "../../../RootMethods/RootNavigation";
import {MowPicker} from "../../../components/ui/Common/Picker/MowPicker";
import CountryList from "../../../SampleData/CountryList";
import CityList from "../../../SampleData/CityList";
import TownList from "../../../SampleData/TownList";
import {MowCheckBox} from "../../../components/ui/Common/CheckBox/MowCheckBox";

export default class NewAddress extends React.Component {

    state = {
        name: "",
        surname: "",
        phone: "",
        countryId: null,
        country: "Country - 1",
        cityId: null,
        city: "City - 1",
        townId: null,
        town: "Town - 1",
        fullAddress: "",
        addressName: "",
        pickerData: [],
        pickerType: 0,
        pickerVisible: false,
        pickerTitle: "",
        addressTitle: ""
    };

    // to store entered regular from user
    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    /**
     *      type -->
     *              1: country
     *              2: city
     *              2: town
     * */
    _onSelect(selectedItem) {

        this.setState({pickerVisible: false});

        let type = this.state.pickerType;

        if (type === 1) {
            this.setState({country: selectedItem["title"]})
        }
        else if (type === 2){
            this.setState({city: selectedItem["title"]})
        }
        else if (type === 3) {
            this.setState({town: selectedItem["title"]})
        }

    }

    _renderInputView(title, icon, key, value, textArea = false) {
        return(

            <View>

                {/* title text */}
                <Text
                    style={{
                        color: mowColors.titleTextColor,
                        fontSize: hp(1.8),
                        fontWeight: "500"
                    }}>

                    {title}

                </Text>

                <MowInput
                    multiline={textArea}
                    textArea={textArea}
                    value={value}
                    containerStyle={{
                        backgroundColor: mowColors.viewBGColor
                    }}
                    onChangeText={value => this.onChangeText(key, value)}
                    // placeholder={title}
                    iconColor={mowColors.mainColor}
                    leftIcon={icon}/>

            </View>

        )
    }

    _renderPickerButton(title, buttonText, icon, pickerData, pickerType, pickerTitle) {
        return(

            <View
                style={{marginVertical: 5}}>

                {/* title text */}
                <Text
                    style={{
                        color: mowColors.titleTextColor,
                        fontSize: hp(1.8),
                        fontWeight: "500"
                    }}>

                    {title}

                </Text>

                <MowButton
                    buttonText={buttonText}
                    onPress={() => {
                        this.setState({
                            pickerData: pickerData,
                            pickerVisible: true,
                            pickerType: pickerType,
                            pickerTitle: pickerTitle
                        })
                    }}
                    leftIcon={icon}
                    leftIconStyle={{color: mowColors.mainColor}}
                    textStyle={{
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        fontSize: hp(1.8),
                        fontFamily: fontFamily.regular,
                        color: mowColors.textColor,
                        paddingLeft: wp(4)
                    }}
                    containerStyle={{
                        ...borderStyle,
                        borderStyle: "solid",
                        borderWidth: 1,
                        borderColor: "#afafaf",
                        backgroundColor: mowColors.viewBGColor,
                        justifyContent: "flex-start",
                        paddingLeft: wp(3)
                    }}/>

            </View>

        )
    }

    render() {

        let {
            name, surname, phone, addressTitle,
            country, city, town, fullAddress,
            pickerData, pickerTitle, pickerVisible
        } = this.state;

        return(

            <MowContainer
                title={mowStrings.button.addNewAddress}>

                <KeyboardAwareScrollView
                    style={pageContainerStyle}
                    showsVerticalScrollIndicator={false}>

                    {/* name input view*/}
                    {this._renderInputView(mowStrings.placeholder.name, "user", "name", name)}

                    {/* surname input view */}
                    {this._renderInputView(mowStrings.placeholder.surname, "user", "surname", surname)}

                    {/* phone input */}
                    {this._renderInputView(mowStrings.placeholder.phone, "phone", "phone", phone)}

                    {/* address title input view */}
                    {this._renderInputView(mowStrings.placeholder.address, "navigation", "addressTitle", addressTitle)}

                    {/* country picker view */}
                    {this._renderPickerButton(mowStrings.placeholder.country, country, "globe", CountryList, 1, mowStrings.placeholder.country)}

                    {/* city picker view */}
                    {this._renderPickerButton(mowStrings.placeholder.city, city, "globe", CityList, 2, mowStrings.placeholder.city)}

                    {/* town picker */}
                    {this._renderPickerButton(mowStrings.placeholder.town, town, "globe", TownList, 3, mowStrings.placeholder.town)}

                    {/* full address view */}
                    {this._renderInputView(mowStrings.placeholder.fullAddress, "navigation", "fullAddress", fullAddress, true)}

                </KeyboardAwareScrollView>

                <View
                    style={{marginHorizontal: wp("3%")}}>

                    {/* save address button */}
                    <MowButton
                        buttonText={mowStrings.button.saveAddress}
                        onPress={() => {navigate("AddressList")}}
                        size={"medium"}
                        type={"success"}/>

                </View>

                <MowPicker
                    pickerOnClose={() => this.setState({pickerVisible: false})}
                    pickerVisible={pickerVisible}
                    pickerTitle={pickerTitle}
                    search={false}
                    data={pickerData}
                    onSelect={(obj) => {this._onSelect(obj)}}/>

            </MowContainer>

        )

    }

}
