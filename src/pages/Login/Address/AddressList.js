import React from "react";
import {FlatList, Text, View, TouchableOpacity} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {shadowStyle} from "../../../values/Styles/MowStyles";
import {MowCheckBox} from "../../../components/ui/Common/CheckBox/MowCheckBox";
import Address from "../../../SampleData/Address";
import {navigate} from "../../../RootMethods/RootNavigation";
import {warningDialog} from "../../../components/global/common/MowDialogFunctions";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import FAIcon from "react-native-vector-icons/FontAwesome";

export default class AddressList extends React.Component {

    state = {
        checkBoxArr:[],
        addressSelected: false,
        addressListKey: 0,
        cart: this.props.route.params["cart"] || false
    };

    _handleAddressSelection(index, item) {
        let checkBoxArr = this.state.checkBoxArr;

        let length = Address.length;

        for (let i = 0; i < length; i++) {
            if (i != index) {
                // to set false all array values except selected index
                checkBoxArr[i] = false;
            }
        }

        // to update selected item as its opposite
        checkBoxArr[index] = !checkBoxArr[index];

        this.setState({checkBoxArr: checkBoxArr, addressSelected: checkBoxArr[index], addressListKey: this.state.addressListKey + 1});
    }

    _goToPayment() {
        if (this.state.addressSelected) {
            navigate("PaymentInformation");
        }
        else {
            warningDialog(mowStrings.appName, mowStrings.alertDialogs.addressSelection);
        }
    }

    render() {

        let {cart, checkBoxArr, addressListKey} = this.state;

        return(

            <MowContainer
                style={{backgroundColor: mowColors.pageBGDarkColor}}
                title={cart ? mowStrings.addressList.deliveryInformation : mowStrings.addressList.title}>

                <FlatList
                    key={addressListKey}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={Address}
                    style={{marginTop: 10}}
                    renderItem={({ item, index }) => (

                        <TouchableOpacity
                            onPress={() => {this._handleAddressSelection(index, item)}}
                            key={index}
                            style={{
                                flexDirection: "row",
                                ...shadowStyle,
                                backgroundColor: mowColors.viewBGColor,
                                margin: 10,
                                padding: 10,
                                borderRadius: 5,
                                alignItems: "center"
                            }}>

                            {/* home icon */}
                            <FAIcon
                                name={"home"}
                                style={{
                                    fontSize: hp(2.5),
                                    marginRight: 10,
                                    color: mowColors.textColor
                                }}/>

                            <View
                                style={{flex: 1}}>

                                {/* title view */}
                                <Text
                                    style={{
                                        fontSize: hp(1.8),
                                        fontWeight: "500",
                                        fontStyle: "normal",
                                        letterSpacing: -0.01,
                                        textAlign: "left",
                                        color: mowColors.titleTextColor,
                                        width: "95%"
                                    }}>

                                    {item["title"]}

                                </Text>

                                {/* address view */}
                                <Text
                                    style={{
                                        marginVertical: 3,
                                        opacity: 0.72,
                                        fontSize: hp(1.7),
                                        fontWeight: "500",
                                        fontStyle: "normal",
                                        lineHeight: 20,
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        color: mowColors.titleTextColor,
                                        width: "90%"
                                    }}>

                                    {item["fullAddress"]}

                                </Text>

                                <View
                                    style={{flexDirection: "row"}}>

                                    {/* name text */}
                                    <Text
                                        style={{
                                            fontSize: hp(1.6),
                                            opacity: 0.72,
                                            fontWeight: "500",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                        }}>

                                        {item["name"]}

                                    </Text>

                                    {/* number text */}
                                    <Text
                                        style={{
                                            fontSize: hp(1.6),
                                            opacity: 0.72,
                                            fontWeight: "500",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                            marginLeft: 20
                                        }}>

                                        {item["number"]}

                                    </Text>

                                </View>

                            </View>

                            {
                                cart &&

                                <View
                                    style={{marginLeft: 5, alignItems: "center", justifyContent: "center"}}>

                                    <MowCheckBox
                                        onPress={() => {this._handleAddressSelection(index, item)}}
                                        checkedColor={mowColors.mainColor}
                                        checked={checkBoxArr[index]}
                                        containerStyle={{alignSelf: "center"}}/>

                                </View>
                            }

                        </TouchableOpacity>

                    )}
                />

                {/*/!* add new address button view *!/*/}
                <View
                    style={{
                        width: wp("45%"),
                        marginRight: 10,
                        marginTop: 10,
                        alignSelf: "flex-end"
                    }}>

                    {/* add new address button */}
                    <MowButton
                        buttonText={mowStrings.button.addNewAddress}
                        onPress={() => navigate("NewAddress")}
                        size={"xSmall"}
                        stickyIcon={true}
                        leftIcon={"plus"}
                        type={"success"}/>

                </View>

                {/*/!* show the view if the navigation comes from cart *!/*/}
                {
                    cart &&

                        <View
                            style={{margin: 10, marginTop: 0}}>

                            <MowButton
                                buttonText={mowStrings.button.goToPayment}
                                onPress={() => {this._goToPayment()}}
                                type={"success"}/>

                        </View>
                }

            </MowContainer>

        )

    }

}
