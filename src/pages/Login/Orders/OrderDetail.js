import React from "react";
import {View, Image, Text, TouchableOpacity, ScrollView} from "react-native";
import FeatherIcon from "react-native-vector-icons/Feather";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {cardStyle, fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import MowProductInfoView from "../../../components/ui/AppSpecifics/MowProductInfoView";
import {navigate} from "../../../RootMethods/RootNavigation";

export default class OrderDetail extends React.Component {

    _renderSummary(title, info, colorful = false) {
        return(

            <View
                style={{
                    flexDirection: "row",
                    width: "100%",
                    alignItems: "center"
                }}>

                {/* title text */}
                <Text
                    style={{
                        color: mowColors.titleTextColor,
                        fontSize: hp(1.7),
                        fontFamily: fontFamily.regular,
                        fontWeight: "600"
                    }}>

                    {title}

                </Text>

                {/* info text */}
                <Text
                    style={{
                        color: colorful ? mowColors.mainColor : mowColors.textColor,
                        fontSize: hp(1.7),
                        marginLeft: 10
                    }}>

                    {colorful ? "$" : ""}{info}

                </Text>

            </View>

        )
    }

    _renderPaymentItem(title, info, colorful = false) {
        return (

            <View
                style={{
                    flexDirection: "row",
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "space-between"
                }}>

                {/* title text */}
                <Text
                    style={{
                        color: mowColors.titleTextColor,
                        fontSize: hp(1.7),
                        fontFamily: fontFamily.regular,
                        fontWeight: "600"
                    }}>

                    {title}

                </Text>

                {/* info text */}
                <Text
                    style={{
                        color: colorful ? mowColors.mainColor : mowColors.textColor,
                        fontSize: hp(1.7),
                        marginLeft: 10
                    }}>

                     ${info}

                </Text>

            </View>

        )
    }

    _renderDetailButton(icon, text, navigateTo, product) {
        return(

            <TouchableOpacity
                onPress={() => {navigate(navigateTo, {product: product})}}
                style={{
                    flexDirection: "row",
                    marginTop: hp("2.5%"),
                    alignItems: "center"
                }}>

                <FeatherIcon
                    name={icon}
                    style={{
                        color: mowColors.mainColor,
                        fontSize: hp("2")
                    }}/>

                <Text
                    style={{
                        marginLeft: 10,
                        fontSize: hp("1.5"),
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "center",
                        color: mowColors.titleTextColor
                    }}>

                    {text}

                </Text>

            </TouchableOpacity>

        )
    }

    _renderSectionTitle(icon, text) {
        return(

            <View
                style={{
                    flexDirection: "row",
                    alignItems: "center",
                    alignSelf: "flex-start",
                }}>

                <FeatherIcon
                    name={icon}
                    style={{
                        color: mowColors.mainColor,
                        fontSize: hp("2")
                    }}/>

                <Text
                    style={{
                        marginLeft: 10,
                        fontSize: hp("1.7"),
                        fontWeight: "600",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "center",
                        color: mowColors.titleTextColor
                    }}>

                    {text}

                </Text>

            </View>

        )
    }

    render() {

        // product info
        const product = this.props.route.params["product"];

        // to control the product has returned or canceled
        const cancelReturn = product["cancel"] || product["return"];

        return(

            <MowContainer
                title={mowStrings.orderDetail.title}>

                <ScrollView
                    style={pageContainerStyle}>

                    {/* product summary view */}
                    <View
                        style={{padding: 15, ...cardStyle, backgroundColor: mowColors.categoryBGColor}}>

                        {/* order no view */}
                        {this._renderSummary(mowStrings.orderDetail.orderNo, product["orderNo"])}

                        {/* order date view */}
                        {this._renderSummary(mowStrings.orderDetail.orderDate, product["date"])}

                        {/* total price view */}
                        {this._renderSummary(mowStrings.orderDetail.total, product["price"], true)}

                    </View>

                    <View
                        style={{padding: 15, ...cardStyle, backgroundColor: mowColors.categoryBGColor, marginTop: 15}}>

                        {/* product summary */}
                        <MowProductInfoView
                            opacity={cancelReturn}
                            product={product}/>

                        {
                            !cancelReturn &&

                                <View
                                    style={{width: "100%"}}>

                                    {/* cargo tracking button*/}
                                    {this._renderDetailButton("box", mowStrings.orderDetail.cargoTracking, "CargoTracking", product)}

                                    {/* rate product button*/}
                                    {this._renderDetailButton("message-square", mowStrings.orderDetail.rateProduct, "RateProduct", product)}

                                    {/* cancel request button*/}
                                    {this._renderDetailButton("x-circle", mowStrings.orderDetail.cancelRequest, "ReturnRequest", product)}

                                </View>
                        }

                    </View>

                    {/* address & payment view */}
                    {
                        !cancelReturn &&

                            <View
                                style={{paddingBottom: 10}}>

                                {/* product address view */}
                                <View
                                    style={{padding: 15, ...cardStyle, marginTop: 15, backgroundColor: mowColors.categoryBGColor}}>

                                    {/* address title view */}
                                    {this._renderSectionTitle("map-pin", mowStrings.orderDetail.addressInfo)}

                                    {/* address text */}
                                    <Text
                                        style={{
                                            width: "100%",
                                            fontSize: hp("1.6"),
                                            fontWeight: "normal",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            lineHeight: 25,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                            marginTop: 10,
                                            marginLeft: 5
                                        }}>

                                        {product["address"]}

                                    </Text>

                                </View>

                                {/* product payment view */}
                                <View
                                    style={{padding: 15, ...cardStyle, marginTop: 15, backgroundColor: mowColors.categoryBGColor}}>

                                    {/* payment title view */}
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            alignItems: "center",
                                            width: "100%",
                                            justifyContent: "space-between"
                                        }}>

                                        <View
                                            style={{alignItems: "center"}}>

                                            {/* render payment info title view */}
                                            {this._renderSectionTitle("box", mowStrings.orderDetail.paymentInfo)}

                                        </View>

                                        <View
                                            style={{flexDirection: "row", alignItems: "center"}}>

                                            <Image
                                                resizeMode={"contain"}
                                                style={{width: wp("5%"), height: hp("5%")}}
                                                source={require("../../../assets/logo/visa.png")}/>

                                            {/* credit card text */}
                                            <Text
                                                style={{
                                                    fontSize: hp("1.4"),
                                                    fontWeight: "normal",
                                                    fontStyle: "normal",
                                                    letterSpacing: 0,
                                                    lineHeight: 25,
                                                    textAlign: "left",
                                                    color: mowColors.titleTextColor,
                                                    marginLeft: 5
                                                }}>

                                                {product["creditCard"]}

                                            </Text>

                                        </View>

                                    </View>

                                    {/* payment info view */}
                                    <View
                                        style={{width: "100%"}}>

                                        {/* subTotal view */}
                                        {this._renderPaymentItem(mowStrings.orderDetail.subTotal, product["subTotal"])}

                                        {/* subTotal view */}
                                        {this._renderPaymentItem(mowStrings.orderDetail.shipment, product["shipment"])}

                                        {/* subTotal view */}
                                        {this._renderPaymentItem(mowStrings.orderDetail.total, product["price"], true)}

                                    </View>

                                </View>

                            </View>
                    }

                    {/* cancel || return reason view */}
                    {
                        cancelReturn &&

                            <View
                                style={{padding: 15, ...cardStyle, marginTop: 15, backgroundColor: mowColors.categoryBGColor}}>

                                {/* cancel || return title view */}
                                {this._renderSectionTitle("message-square", product["cancel"] ? mowStrings.orderDetail.cancelReason : mowStrings.orderDetail.returnReason)}

                                {/* description text */}
                                <Text
                                    style={{
                                        padding: 10,
                                        fontSize: hp("1.8"),
                                        fontWeight: "normal",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        color: mowColors.textColor
                                    }}>

                                    {product["reasonText"]}

                                </Text>

                            </View>
                    }

                </ScrollView>

            </MowContainer>

        )

    }

}
