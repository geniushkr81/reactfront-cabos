import React from "react";
import {Text, View} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {cardStyle, fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {MowPicker} from "../../../components/ui/Common/Picker/MowPicker";
import MowProductInfoView from "../../../components/ui/AppSpecifics/MowProductInfoView";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import ReturnReasons from "../../../SampleData/ReturnReasons";
import {MowCheckBox} from "../../../components/ui/Common/CheckBox/MowCheckBox";
import {TouchableOpacity} from "react-native-gesture-handler";

export default class ReturnRequest extends React.Component {

    state = {
        requestText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        returnReasons: ReturnReasons,
        reason: "",
        selectedCheckBox: -1
    };

    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    _handleSelection(data, index) {
        let {selectedCheckBox} = this.state;

        let value = -1;
        let reason = "";

        if (selectedCheckBox !== index) {
            value = index;
            reason = data["title"];
        }

        this.setState({selectedCheckBox: value, reason: reason})
    }

    render() {

        const product = this.props.route.params["product"];

        let {reason, requestText, returnReasons, selectedCheckBox} = this.state;

        return(

            <MowContainer
                title={mowStrings.returnRequest.title}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={pageContainerStyle}>

                    {/* product summary view */}
                    <View
                        style={{padding: 15, ...cardStyle, backgroundColor: mowColors.categoryBGColor}}>

                        {/* product summary */}
                        <MowProductInfoView
                            product={product}/>

                    </View>

                    <View
                        style={{marginTop: 15, backgroundColor: mowColors.categoryBGColor, padding: 10}}>

                        {/* return request title */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "600",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor,
                                fontFamily: fontFamily.medium
                            }}>

                            {mowStrings.returnRequest.returnReason}

                        </Text>

                        {returnReasons.map((data, index) => {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => this._handleSelection(data, index)}
                                    style={{
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        paddingVertical: 10
                                    }}>

                                    <Text
                                        style={{
                                            color: mowColors.textColor,
                                            fontSize: hp(1.8),
                                            fontWeight: "500",
                                            fontFamily: fontFamily.regular
                                        }}>

                                        {data["title"]}

                                    </Text>

                                    <MowCheckBox
                                        onPress={() => this._handleSelection(data, index)}
                                        checkedColor={mowColors.mainColor}
                                        checked={selectedCheckBox === index}/>

                                </TouchableOpacity>
                            )
                        })}

                    </View>

                    <View
                        style={{marginTop: 15, backgroundColor: mowColors.categoryBGColor, padding: 10}}>

                        {/* return reason text title */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "600",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor,
                                fontFamily: fontFamily.medium
                            }}>

                            {mowStrings.returnRequest.returnTextTitle}

                        </Text>

                        {/* return reason input */}
                        <MowInput
                            textArea={true}
                            multiline={true}
                            containerStyle={{
                                width: "100%",
                                height: hp("20%")
                            }}
                            textInputStyle={{
                                fontSize: hp("1.8%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                fontFamily: fontFamily.regular,
                                textAlign: "left",
                                color: "#aeaeae",
                                width: "100%",
                                paddingBottom: 10
                            }}
                            value={requestText}
                            onChangeText={value => this.onChangeText('requestText', value)}/>

                    </View>

                </KeyboardAwareScrollView>

                {/* send button view */}
                <View
                    style={{marginHorizontal: wp("3%")}}>

                    {/* send button */}
                    <MowButton
                        buttonText={mowStrings.button.createReturnRequest}
                        type={"success"}/>

                </View>

            </MowContainer>

        )

    }

}
