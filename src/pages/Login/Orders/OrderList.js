import React from "react";
import {FlatList} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import MowOrderViewItem from "../../../components/ui/AppSpecifics/MowOrderViewItem";
import MyOrders from "../../../SampleData/Orders/MyOrders";

export default class OrderList extends React.Component {

    state = {
        showAll: true,
        canceled: false,
        returned: false,
        orderData: MyOrders
    };

    render() {

        let {orderData} = this.state;

        return(

            <MowContainer
                title={mowStrings.orderList.title}>

                <FlatList
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={orderData}
                    renderItem={({ item, index }) => (

                        <MowOrderViewItem
                            listKey={index}
                            product={item}/>

                    )}

                />

            </MowContainer>

        )

    }

}
