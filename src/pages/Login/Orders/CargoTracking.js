import React from "react";
import {FlatList, Text, View, ScrollView, Image} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {cardStyle, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import FeatherIcon from "react-native-vector-icons/Feather";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import MowProductInfoView from "../../../components/ui/AppSpecifics/MowProductInfoView";
import {TouchableOpacity} from "react-native-gesture-handler";
import Clipboard from '@react-native-clipboard/clipboard';
import {ShowToast} from "../../../components/global/common/Toasts";

export default class CargoTracking extends React.Component {

    render() {

        const product = this.props.route.params["product"];

        return(

            <MowContainer
                title={mowStrings.cargoTracking.title}>

                <ScrollView
                    style={pageContainerStyle}>

                    {/* product summary view */}
                    <View
                        style={{padding: 15, ...cardStyle, backgroundColor: mowColors.categoryBGColor}}>

                        {/* product summary */}
                        <MowProductInfoView
                            product={product}/>

                    </View>

                    {/* cargo firm & cargo tracking number */}
                    <View
                        style={{padding: 15, ...cardStyle, marginTop: 15, backgroundColor: mowColors.categoryBGColor}}>

                        {/* cargo firm logo image */}
                        <Image
                            resizeMode={"contain"}
                            style={{width: "50%", height: hp(10)}}
                            source={product["cargoFirmLogo"]}/>

                        {/*/!* cargo firm name *!/*/}
                        {/*<Text*/}
                        {/*    style={{*/}
                        {/*        color: mowColors.titleTextColor,*/}
                        {/*        fontSize: hp(1.8),*/}
                        {/*        fontWeight: "500"*/}
                        {/*    }}>*/}

                        {/*    {product["cargoFirm"]}*/}

                        {/*</Text>*/}

                        {/* cargo tracking no info view */}
                        <View
                            style={{marginTop: 10}}>

                            <Text
                                style={{
                                    textAlign: "center",
                                    marginBottom: 10,
                                    color: mowColors.titleTextColor
                                }}>

                                {mowStrings.cargoTracking.cargoTrackingNo}

                            </Text>

                            {/* cargo tracking no*/}
                            <TouchableOpacity
                                onPress={() => {
                                    // to copy cargo tracking number to the device clipboard
                                    Clipboard.setString(product["cargoTrackingNo"]);

                                    ShowToast.info(mowStrings.cargoTracking.copiedToClipBoard)
                                }}
                                style={{
                                    borderWidth: 1,
                                    borderColor: mowColors.mainColor,
                                    borderRadius: 10,
                                    padding: 10,

                                }}>

                                <Text
                                    style={{
                                        color: mowColors.titleTextColor,
                                        fontSize: hp(2),
                                        fontWeight: "500"
                                    }}>

                                    {product["cargoTrackingNo"]}

                                </Text>

                            </TouchableOpacity>

                        </View>

                    </View>

                    {/* timeline view - cargo tracking */}
                    <View
                        style={{padding: 15, ...cardStyle, marginTop: 15, backgroundColor: mowColors.categoryBGColor}}>

                        {/* title view */}
                        <View
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                                width: "100%",
                            }}>

                            <FeatherIcon
                                name={"truck"}
                                style={{
                                    color: mowColors.mainColor,
                                    fontSize: hp("3")
                                }}/>

                            <Text
                                style={{
                                    marginLeft: 10,
                                    fontSize: hp("1.5"),
                                    fontWeight: "normal",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "center",
                                    color: mowColors.titleTextColor
                                }}>

                                {mowStrings.cargoTracking.cargoMovements}

                            </Text>

                        </View>

                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={product["cargo"]}
                            style={{width: "100%", marginTop: 10}}
                            renderItem={({ item, index }) => (

                                <View
                                    style={{flexDirection: "row", height: hp("10%")}}>

                                    {/* line view */}
                                    <View
                                        style={{alignItems: "center", marginTop: 5}}>

                                        {/* circle view */}
                                        <View
                                            style={{
                                                width: hp("2%"),
                                                height: hp("2%"),
                                                backgroundColor: item["isHere"] ? mowColors.mainColor : "#b6babe",
                                                borderRadius: 100
                                            }}/>

                                        {/* line view */}
                                        {
                                            (product["cargo"].length - 1) !== index &&

                                            <View
                                                style={{
                                                    height: "100%",
                                                    width: 2,
                                                    backgroundColor: "#b6babe"
                                                }}/>
                                        }

                                    </View>

                                    <View
                                        style={{paddingBottom: 10, marginLeft: 20}}>

                                        {/* location text */}
                                        <Text
                                            style={{
                                                fontSize: hp("1.8"),
                                                fontWeight: "600",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "left",
                                                color: mowColors.titleTextColor,
                                                marginBottom: 5
                                            }}>

                                            {item["location"]}

                                        </Text>

                                        {/* date text */}
                                        {
                                            item["date"] != "" &&

                                                <Text
                                                    style={{
                                                        fontSize: hp("1.8"),
                                                        fontWeight: "500",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "left",
                                                        color: mowColors.textColor,
                                                        marginBottom: 5
                                                    }}>

                                                    {item["date"]}

                                                </Text>
                                        }

                                        {/* situation text */}
                                        <Text
                                            style={{
                                                fontSize: hp("1.8"),
                                                fontWeight: "normal",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "left",
                                                color: mowColors.textColor,
                                            }}>

                                            {item["situation"]}

                                        </Text>

                                    </View>

                                </View>

                            )}/>

                    </View>

                </ScrollView>

            </MowContainer>

        )

    }

}
