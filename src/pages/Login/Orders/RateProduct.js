import React from "react";
import {View, Text, TouchableOpacity} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {cardStyle, fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import MowProductInfoView from "../../../components/ui/AppSpecifics/MowProductInfoView";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import FAIcon from "react-native-vector-icons/FontAwesome";

export default class RateProduct extends React.Component {

    state = {
        starCount: 0,
        reviewText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    };

    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    _renderStar(index) {

        let starCount = this.state.starCount;

        let selected = index <= starCount;

        return(

            <TouchableOpacity
                onPress={() => {this.setState({starCount: index})}}
                style={{
                    marginHorizontal: 5
                }}>

                <FAIcon
                    name={"star"}
                    style={{
                        fontSize: hp(3),
                        color: selected ? mowColors.mainColor : mowColors.textColor
                    }}/>

            </TouchableOpacity>

        )
    }

    render() {

        const product = this.props.route.params["product"];

        return(

            <MowContainer
                title={mowStrings.rateProduct.title}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={pageContainerStyle}>

                    {/* product summary view */}
                    <View
                        style={{padding: 15, ...cardStyle, backgroundColor: mowColors.categoryBGColor}}>

                        {/* product summary */}
                        <MowProductInfoView
                            product={product}/>

                    </View>

                    {/* star selection view  */}
                    <View
                        style={{marginTop: 35, marginBottom: 10, backgroundColor: mowColors.categoryBGColor, paddingHorizontal: 10, borderRadius: 5, alignItems: "center"}}>

                        <Text
                            style={{
                                fontSize: hp(2),
                                color: mowColors.titleTextColor,
                                fontWeight: "500"
                            }}>

                            {mowStrings.rateProduct.title}

                        </Text>

                        {/* star view */}
                        <View
                            style={{flexDirection: "row", marginTop: 20}}>

                            {this._renderStar(1)}
                            {this._renderStar(2)}
                            {this._renderStar(3)}
                            {this._renderStar(4)}
                            {this._renderStar(5)}

                        </View>

                    </View>

                    <View
                        style={{marginTop: 15, backgroundColor: mowColors.categoryBGColor, padding: 10}}>

                        {/* message title */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "600",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.textColor,
                                fontFamily: fontFamily.medium
                            }}>

                            {mowStrings.rateProduct.reviewText}

                        </Text>

                        {/* message input */}
                        <MowInput
                            multiline={true}
                            textArea={true}
                            containerStyle={{
                                width: "100%",
                                height: hp("15%")
                            }}
                            textInputStyle={{
                                fontSize: hp("1.8%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                fontFamily: fontFamily.regular,
                                textAlign: "left",
                                color: "#aeaeae",
                                width: "100%",
                                paddingBottom: 10
                            }}
                            value={this.state.reviewText}
                            onChangeText={value => this.onChangeText('reviewText', value)}/>

                    </View>

                </KeyboardAwareScrollView>

                {/* send button view */}
                <View
                    style={{marginHorizontal: wp("3%")}}>

                    {/* send button */}
                    <MowButton
                        buttonText={mowStrings.button.send}
                        type={"success"}/>

                </View>

            </MowContainer>

        )

    }

}
