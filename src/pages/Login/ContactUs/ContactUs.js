import React from "react";
import {View, Image, Text} from "react-native";
import FeatherIcon from "react-native-vector-icons/Feather";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {pageContainerStyle, shadowStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

export default class ContactUs extends React.Component {

    _renderInfoView(title, icon, text) {
        return(

            <View
                style={{
                    marginLeft: 25,
                    marginVertical: 15
                }}>

                <Text
                    style={{
                        color: mowColors.titleTextColor,
                        fontSize: hp(2),
                        fontWeight: "500"
                    }}>

                    {title}

                </Text>

                <View
                    style={{flexDirection: "row", alignItems: "center", marginTop: 15}}>

                    <FeatherIcon
                        style={{
                            color: mowColors.mainColor,
                            fontSize: hp(3)
                        }}
                        name={icon}/>

                    <Text
                        style={{
                            color: mowColors.textColor,
                            fontSize: hp(1.7),
                            marginLeft: 10
                        }}>

                        {text}

                    </Text>

                </View>

            </View>

        )
    }

    render() {

        return(

            <MowContainer
                title={mowStrings.drawerMenu.contactUs}>

                <View
                    style={pageContainerStyle}>

                    {/* logo */}
                    <Image
                        style={{
                            width: wp(80),
                            height: hp(15),
                            alignSelf: "center",
                        }}
                        source={require("../../../assets/logo/logo_horizontal.png")}
                        resizeMode={"contain"}/>

                    {/* email view */}
                    {this._renderInfoView(mowStrings.placeholder.email, "mail", "info@mowega.com")}

                    {/* phone view */}
                    {this._renderInfoView(mowStrings.placeholder.phone, "phone", "+41 (547) 555 55 55")}

                    {/* address view */}
                    {this._renderInfoView(mowStrings.placeholder.address, "map", "City Center - mowega")}

                </View>

            </MowContainer>

        )

    }

}
