import React from "react";
import {View, Image, Text} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {navigate} from "../../../RootMethods/RootNavigation";

export default class CompleteOrder extends React.Component {

    render() {

        return(

            <MowContainer
                title={mowStrings.completeOrder.title}>

                <View
                    style={{...pageContainerStyle, paddingTop: 50}}>

                    <Image
                        source={require("../../../assets/icon/ic_confetti.png")}
                        resizeMode={"contain"}
                        style={{
                            marginVertical: hp("3%"),
                            width: hp("10%"),
                            height: hp("10%"),
                            alignSelf: "center"
                        }}/>

                    {/* congrats. text */}
                    <Text
                        style={{
                            fontSize: hp("4%"),
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "center",
                            color: mowColors.titleTextColor,
                            marginTop: 10
                        }}>

                        {mowStrings.completeOrder.congratulations}

                    </Text>

                    {/* payment success message text */}
                    <Text
                        style={{
                            fontSize: hp("3%"),
                            fontWeight: "400",
                            fontStyle: "normal",
                            lineHeight: 43,
                            letterSpacing: 0,
                            textAlign: "center",
                            color: mowColors.titleTextColor,
                            marginTop: 20
                        }}>

                        {mowStrings.completeOrder.completeMessage}

                    </Text>

                    {/* order number text */}
                    <Text
                        style={{
                            fontSize: hp("2.2%"),
                            fontWeight: "bold",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "center",
                            color: "#df5151",
                            marginTop: 20
                        }}>

                        {mowStrings.completeOrder.orderNumber}: 5468874541

                    </Text>

                    {/* order message text */}
                    <Text
                        style={{
                            fontSize: hp("2.2%"),
                            fontWeight: "bold",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "center",
                            color: mowColors.titleTextColor,
                            marginTop: 20
                        }}>

                        {mowStrings.completeOrder.orderMessage }

                    </Text>

                </View>

                <View
                    style={{width: "90%", alignSelf: "center"}}>

                    {/* complete payment button */}
                    <MowButton
                        buttonText={mowStrings.button.ok}
                        onPress={() => {navigate("Home")}}
                        size={"medium"}
                        type={"success"}/>

                </View>

            </MowContainer>

        )

    }

}
