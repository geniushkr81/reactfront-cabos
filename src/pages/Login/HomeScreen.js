import React from "react";
import {Text, View, Image, FlatList, TouchableOpacity, ScrollView, Dimensions} from "react-native";
import Swiper from 'react-native-swiper'
import {mowColors} from "../../values/Colors/MowColors";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowContainer} from "../../components/ui/Core/Container/MowContainer";
import {mowStrings} from "../../values/Strings/MowStrings";
import {categoryStyle, gPadding} from "../../values/Styles/MowStyles";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {MowButton} from "../../components/ui/Common/Button/MowButton";
import MowTitleView from "../../components/ui/AppSpecifics/MowTitleView";
import {MowCountDown} from "../../components/ui/AppSpecifics/CountDown/MowCountDown";
import {navigate, openDrawer} from "../../RootMethods/RootNavigation";
import {MowStarView} from "../../components/ui/AppSpecifics/StarView/MowStarView";
import TrendBrands from "../../SampleData/TrendBrands";
import BestSeller from "../../SampleData/BestSeller";
import Advantages from "../../SampleData/Advantages";
import SmartPhones from "../../SampleData/SmartPhones";
import CarAccessories from "../../SampleData/CarAccessories";
import TrendCategories from "../../SampleData/TrendCategories";
import TrendCampaign from "../../SampleData/Campaign/TrendCampaign";
import TodaysBestDiscounts from "../../SampleData/TodaysBestDiscounts";
import {isTablet, navbarHeight, platform} from "../../values/Constants/MowConstants";
import SportShoe from "../../SampleData/Product/SportShoe";
import Laptop from "../../SampleData/Product/Laptop";

import { LineChart } from 'react-native-chart-kit';

export default class HomeScreen extends React.Component {

    render() {

        return (

            <MowContainer
                footerActiveIndex={1}
                navbar={false}>

                {/* home screen navbar */}
                <View
                    style={{
                        height: navbarHeight,
                        alignItems: "flex-end",
                        justifyContent: "center",
                        flexDirection: 'row',
                        backgroundColor: mowColors.navBarColor,
                    }}>

                    {/* search button 
                    <TouchableOpacity
                        onPress={() => {navigate("HomeFilter")}}
                        style={{flex: 1.5, alignItems: "center", zIndex: 1}}>

                        <FAIcon
                            style={{fontSize: hp("2.5%")}}
                            color={"white"}
                            name={'search'}/>

                    </TouchableOpacity> */}

                    {/* logo view */}
                    <View
                        style={{
                            width:"100%",
                            alignSelf:"center",
                            alignItems:"center" ,
                            justifyContent:"center",
                            flex: 7
                        }}>

                        {/* logo with text */}
                        <Image
                            source={require("../../assets/logo/logo.png")}
                            style={{ height: hp("3%")}}
                            resizeMode={"contain"}/>
                        
                    </View>

                    {/* user button 
                    <TouchableOpacity
                        onPress={() => {openDrawer()}}
                        style={{flex: 1.5, alignItems: "center"}}>

                        <FAIcon
                            style={{fontSize: hp("3%")}}
                            color={"white"}
                            name={'bars'}/>

                    </TouchableOpacity>*/}

                </View>

                <ScrollView>

                <LineChart data={{
                            labels: ["January", "February", "March", "April", "May", "June"],
                            datasets: [
                                {
                                    data:[
                                        // axios call request http
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100
                                    ]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width}
                        height={220}
                        yAxisLabel="$"
                        yAxisSuffix="K"
                        yAxisInterval={1}
                        chartConfig={{

                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                              borderRadius: 16
                            },
                            propsForDots: {
                              r: "6",
                              strokeWidth: "2",
                              stroke: "#ffa726"
                            }
                          }}
                          bezier
                          style={{
                            marginVertical: 8,
                            borderRadius: 16
                          }}
                        />

<LineChart data={{
                            labels: ["January", "February", "March", "April", "May", "June"],
                            datasets: [
                                {
                                    data:[
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100
                                    ]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width}
                        height={220}
                        yAxisLabel="$"
                        yAxisSuffix="K"
                        yAxisInterval={1}
                        chartConfig={{

                            backgroundColor: "#e2ea99",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                              borderRadius: 16
                            },
                            propsForDots: {
                              r: "6",
                              strokeWidth: "2",
                              stroke: "#ffa726"
                            }
                          }}
                          bezier
                          style={{
                            marginVertical: 8,
                            borderRadius: 16
                          }}
                        />

                    {/* trend campaign view 
                    <View
                        style={[categoryStyle, {marginTop: 15, height: isTablet ? wp("65%") : wp(75), backgroundColor: mowColors.categoryBGColor}]}>
                   

                    </View>*/}

                    {/* today's best discounts view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* today's best discount title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.bestDiscounts}/>

                        {/* today's best discount list */}
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={TodaysBestDiscounts}
                            renderItem={({ item, index }) => (

                                //discount list item
                                <TouchableOpacity
                                    onPress={() => navigate("ProductDetail2", {product: SportShoe})}
                                    style={{
                                        width: isTablet ? wp("25%") : wp(35),
                                        height: platform === "android" ? hp(36) : hp(34),
                                        marginHorizontal: 10,
                                    }}
                                    key={index}>

                                    {/* image view */}
                                    <View
                                        style={{
                                            height: "60%",
                                            width: "100%",
                                            borderRadius: 10,
                                            borderStyle: "solid",
                                            borderWidth: 1,
                                            borderColor: "rgba(146, 146, 146, 0.41)"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 5, right: 5, zIndex: 99}}>

                                            <FAIcon
                                                style={{
                                                    color: mowColors.titleTextColor,
                                                    fontSize: hp("2%")
                                                }}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        <Image
                                            style={{
                                                height: "100%",
                                                width: "100%",
                                            }}
                                            resizeMode={"contain"}
                                            source={item["image"]}/>

                                    </View>

                                    {/* price & discount view */}
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            alignItems: "center",
                                            width: "100%",
                                            marginTop: 5
                                        }}>

                                        {/* last price text */}
                                        <Text
                                            style={{
                                                marginTop: 1,
                                                fontSize: hp("2%"),
                                                fontWeight: "bold",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "center",
                                                color: mowColors.titleTextColor
                                            }}>

                                            {item["lastPrice"]}

                                        </Text>

                                        {/* first price text view  */}
                                        <View
                                            style={{alignItems: "center", justifyContent: "center", marginLeft: 20}}>

                                            {/* first price text */}
                                            <Text
                                                style={{
                                                    fontSize: hp(1.7),
                                                    fontWeight: "300",
                                                    fontStyle: "normal",
                                                    letterSpacing: 0,
                                                    textAlign: "center",
                                                    color: mowColors.mainColor
                                                }}>

                                                {item["firstPrice"]}

                                            </Text>

                                            <View
                                                style={{
                                                    backgroundColor: mowColors.mainColor,
                                                    width: "100%",
                                                    height: hp("0.1%"),
                                                    position: "absolute",
                                                }}/>

                                        </View>

                                    </View>

                                    {/* about product - title & star view */}
                                    <View
                                        style={{flexDirection: "row", alignItems: "center", width: "100%"}}>

                                        {/* title text view */}
                                        <View
                                            style={{flex: 3}}>

                                            {/* title text */}
                                            <Text
                                                numberOfLines={2}
                                                style={{
                                                    marginTop: 5,
                                                    fontSize: hp("1.8%"),
                                                    fontWeight: "normal",
                                                    fontStyle: "normal",
                                                    letterSpacing: 0,
                                                    textAlign: "left",
                                                    color: mowColors.titleTextColor,
                                                }}>

                                                {item["title"]}

                                            </Text>

                                        </View>

                                        {/* star view */}
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                alignItems: "center",
                                                justifyContent: "center",
                                                backgroundColor: mowColors.mainColor,
                                                borderRadius: 5,
                                                padding: 3,
                                                flex: 1,
                                                marginLeft: 10
                                            }}>

                                            <FAIcon
                                                name={"star"}
                                                style={{
                                                    fontSize: hp(1.5),
                                                    color: "white"
                                                }}/>

                                            <Text
                                                style={{
                                                    color: "white",
                                                    fontSize: hp(1.4),
                                                    fontWeight: "500",
                                                    marginLeft: 5
                                                }}>

                                                {item["star"]}

                                            </Text>

                                        </View>

                                    </View>

                                    {/* add to cart button */}
                                    <MowButton
                                        onPress={() => navigate("Cart")}
                                        type={"success"}
                                        size={"xSmall"}
                                        buttonText={mowStrings.button.addToCart}
                                        containerStyle={{marginHorizontal: 0, borderWidth: 1, borderColor: mowColors.mainColor}}/>

                                </TouchableOpacity>

                            )}
                        />

                    </View>

                    {/* trend brands view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* trend brands title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.trendBrands}/>

                        {/* trend brands horizontal list */}
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={TrendBrands}
                            renderItem={({ item, index }) => (

                                // trend brands item touchable
                                <TouchableOpacity
                                    style={{
                                        width: hp(8),
                                        height: hp(8),
                                        justifyContent: "center",
                                        backgroundColor: "transparent",
                                        borderStyle: "solid",
                                        borderWidth : 1,
                                        borderColor: "rgba(146, 146, 146, 0.41)",
                                        borderRadius: 1000,
                                        marginHorizontal: 10,
                                        marginVertical: 5,
                                        alignItems: "center",
                                    }}
                                    key={index}>

                                    {/* brand image */}
                                    <Image
                                        style={{height: hp(5), width: hp(5)}}
                                        source={item["image"]}
                                        resizeMode={"contain"}/>

                                </TouchableOpacity>

                            )}
                        />

                    </View>

                    {/* best seller view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* best seller title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.bestSeller}/>

                        {/* best seller item list */}
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={BestSeller}
                            renderItem={({ item, index }) => (

                                //best seller list item
                                <TouchableOpacity
                                    onPress={() => navigate("ProductDetail3", {product: Laptop})}
                                    style={{
                                        width: isTablet ? wp("25%") : wp(35),
                                        height: hp("25%"),
                                        marginHorizontal: 10,
                                    }}
                                    key={index}>

                                    {/* image view */}
                                    <View
                                        style={{
                                            height: "60%",
                                            width: "100%",
                                            borderRadius: 10,
                                            borderStyle: "solid",
                                            borderWidth: 0.5,
                                            borderColor: "rgba(146, 146, 146, 0.41)",
                                            justifyContent: "center"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 5, right: 5, zIndex: 99}}>

                                            <FAIcon
                                                style={{
                                                    color: mowColors.titleTextColor,
                                                    fontSize: hp("2%")
                                                }}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        <Image
                                            style={{
                                                height: "100%",
                                                width: "100%",
                                            }}
                                            resizeMode={"contain"}
                                            source={item["image"]}/>

                                        {
                                            !item["stock"] &&

                                            // out of stock view
                                            <View
                                                style={{
                                                    position: "absolute",
                                                    opacity: 0.8,
                                                    backgroundColor: "#848484",
                                                    width: "100%"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontSize: hp("1.8%"),
                                                        fontWeight: "normal",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "center",
                                                        color: "#ffffff"
                                                    }}>

                                                    {mowStrings.homeScreen.outOfStock}

                                                </Text>

                                            </View>

                                        }

                                        {
                                            item["new"] &&

                                                <View
                                                    style={{
                                                        position: "absolute",
                                                        backgroundColor: mowColors.mainColor,
                                                        top: 5,
                                                        left: 5,
                                                        borderRadius: 200,
                                                        width: hp(4),
                                                        height: hp(4),
                                                        justifyContent: "center"
                                                    }}>

                                                    <Text
                                                        style={{
                                                            fontWeight: "bold",
                                                            textAlign: "center",
                                                            color: "#ffffff",
                                                            fontSize: hp(1.5)
                                                        }}>

                                                        {mowStrings.homeScreen.new}

                                                    </Text>

                                                </View>
                                        }

                                    </View>

                                    {/* price text */}
                                    <Text
                                        style={{
                                            fontSize: hp("2%"),
                                            fontWeight: "bold",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                            marginTop: 5,
                                        }}>

                                        {item["price"]}

                                    </Text>

                                    <Text
                                        numberOfLines={2}
                                        style={{
                                            marginTop: 5,
                                            fontSize: hp("1.8%"),
                                            fontWeight: "normal",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                        }}>

                                        {item["title"]}

                                    </Text>

                                </TouchableOpacity>

                            )}
                        />

                    </View>

                    {/* advantages view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* advantages title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.advantages}/>

                        {/* advantages horizontal list */}
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            data={Advantages}
                            renderItem={({ item, index }) => (

                                // advantage item view
                                <View
                                    key={index}
                                    style={{
                                        width: hp(13),
                                        paddingVertical: 10,
                                        marginRight: 10,
                                        justifyContent: "center",
                                        alignItems: "center",
                                        backgroundColor: mowColors.mainColor,
                                        borderRadius: 5
                                    }}>

                                    {/* advantage image */}
                                    <Image
                                        style={{height: hp(5)}}
                                        source={item["image"]}
                                        resizeMode={"contain"}/>

                                    {/* advantage text */}
                                    <Text
                                        style={{
                                            marginTop: 10,
                                            fontSize: hp(1.4),
                                            fontWeight: "bold",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "center",
                                            color: "white"
                                        }}>

                                        {item["title"]}

                                    </Text>

                                </View>

                            )}
                        />

                    </View>

                    {/* smart phones view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, paddingRight: gPadding, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* smart phones title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.smartPhones}/>

                        {/* smart phones list */}
                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={isTablet ? 3 : 2}
                            data={SmartPhones}
                            renderItem={({ item, index }) => (

                                //smart phone list item
                                <View
                                    style={
                                        isTablet ?

                                            {
                                                height: hp(30),
                                                width: wp(29.5),
                                                margin: 10,
                                                marginTop: 0
                                            }

                                            :

                                            {
                                                height: hp("30%"),
                                                flex: 1,
                                                margin: 10,
                                                marginTop: 0,
                                            }
                                    }
                                    key={index}>

                                    {/* image view */}
                                    <View
                                        style={{
                                            height: "60%",
                                            width: "100%",
                                            borderRadius: 10,
                                            borderStyle: "solid",
                                            borderWidth: 0.5,
                                            borderColor: "rgba(146, 146, 146, 0.41)",
                                            justifyContent: "center"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 5, right: 5, zIndex: 99}}>

                                            <FAIcon
                                                style={{
                                                    color: mowColors.titleTextColor,
                                                    fontSize: hp("2%")
                                                }}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        <Image
                                            style={{
                                                height: "100%",
                                                width: "100%",
                                            }}
                                            resizeMode={"contain"}
                                            source={item["image"]}/>

                                        {
                                            item["new"] &&

                                            <View
                                                style={{
                                                    position: "absolute",
                                                    backgroundColor: mowColors.mainColor,
                                                    top: 5,
                                                    left: 5,
                                                    borderRadius: 200,
                                                    width: hp(4),
                                                    height: hp(4),
                                                    justifyContent: "center"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        textAlign: "center",
                                                        color: "#ffffff",
                                                        fontSize: hp(1.5)
                                                    }}>

                                                    {mowStrings.homeScreen.new}

                                                </Text>

                                            </View>
                                        }

                                    </View>

                                    {/* title text */}
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            marginTop: 5,
                                            fontSize: hp("1.8%"),
                                            fontWeight: "normal",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor,
                                        }}>

                                        {item["title"]}

                                    </Text>

                                    {/* star view */}
                                    <View
                                        style={{flexDirection: "row", alignItems: "center", marginTop: 5}}>

                                        {/* stars*/}
                                        <MowStarView
                                            score={item["star"]}/>

                                        {/* vote count text */}
                                        <Text
                                            style={{
                                                marginLeft: 3,
                                                fontSize: hp("1.4%"),
                                                fontWeight: "normal",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "left",
                                                color: mowColors.textColor
                                            }}>

                                            {"("}{item["voteCount"]}{")"}

                                        </Text>

                                    </View>

                                    {/* price & discount view */}
                                    <View
                                        style={{flexDirection: "row", marginTop: 5, alignItems: "center"}}>

                                        {/* price view */}
                                        <View
                                            style={{flexDirection: "row", alignItems: "center"}}>

                                            {/* price view before discount */}
                                            <View
                                                style={{}}>

                                                {/* first price text view  */}
                                                <View
                                                    style={{alignItems: "center", justifyContent: "center"}}>

                                                    {/* first price text */}
                                                    <Text
                                                        style={{
                                                            fontSize: hp(1.3),
                                                            fontWeight: "300",
                                                            fontStyle: "normal",
                                                            letterSpacing: 0,
                                                            textAlign: "center",
                                                            color: mowColors.textColor
                                                        }}>

                                                        {item["firstPrice"]}

                                                    </Text>

                                                    <View
                                                        style={{
                                                            backgroundColor: mowColors.mainColor,
                                                            width: "90%",
                                                            height: hp("0.1%"),
                                                            position: "absolute",
                                                        }}/>

                                                </View>

                                                {/* second price text */}
                                                <Text
                                                    style={{
                                                        marginTop: 1,
                                                        fontSize: hp(1.3),
                                                        fontWeight: "500",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "center",
                                                        color: mowColors.textColor
                                                    }}>

                                                    {item["secondPrice"]}

                                                </Text>

                                            </View>

                                            {/* price view after discount */}
                                            <View
                                                style={{flex: 1}}>

                                                <View
                                                    style={{
                                                        alignItems: "center",
                                                        justifyContent: "center",
                                                        alignSelf: "flex-end"
                                                    }}>

                                                    {/* discount text */}
                                                    <Text
                                                        numberOfLines={2}
                                                        style={{
                                                            color: mowColors.textColor,
                                                            fontSize: hp(1.2),
                                                            textAlign: "center",
                                                            // width: "70%",
                                                        }}>

                                                        {item["discountRate"]} {mowStrings.homeScreen.cartDiscount}

                                                    </Text>

                                                    {/* last price text */}
                                                    <Text
                                                        style={{
                                                            marginTop: 3,
                                                            fontSize: hp(1.8),
                                                            fontWeight: "bold",
                                                            fontStyle: "normal",
                                                            letterSpacing: 0,
                                                            textAlign: "center",
                                                            color: mowColors.mainColor
                                                        }}>

                                                        {item["lastPrice"]}

                                                    </Text>

                                                </View>

                                            </View>

                                        </View>

                                    </View>

                                </View>

                            )}
                        />

                    </View>

                    {/* car accessories view */}
                    <View
                        style={[categoryStyle, {marginTop: 15, paddingRight: gPadding, backgroundColor: mowColors.categoryBGColor}]}>

                        {/* smart phones title view */}
                        <MowTitleView
                            showButton={false}
                            title={mowStrings.homeScreen.carAccessories}/>

                        {/* car accessories list */}
                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={isTablet ? 3 : 2}
                            data={CarAccessories}
                            renderItem={({ item, index }) => (

                                //car accessories list item
                                <View
                                    style={
                                        isTablet ?

                                            {
                                                height: hp(30),
                                                width: wp(29.5),
                                                margin: 10,
                                                marginTop: 0
                                            }

                                            :

                                            {
                                                height: hp("30%"),
                                                flex: 1,
                                                margin: 10,
                                                marginTop: 0,
                                            }
                                    }
                                    key={index}>

                                    {/* image view */}
                                    <View
                                        style={{
                                            height: "70%",
                                            width: "100%",
                                            borderRadius: 10,
                                            borderStyle: "solid",
                                            borderWidth: 0.5,
                                            borderColor: "rgba(146, 146, 146, 0.41)",
                                            justifyContent: "center"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 5, right: 5, zIndex: 99}}>

                                            <FAIcon
                                                style={{
                                                    color: mowColors.titleTextColor,
                                                    fontSize: hp("2%")
                                                }}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        <Image
                                            style={{
                                                height: "100%",
                                                width: "100%",
                                            }}
                                            resizeMode={"contain"}
                                            source={item["image"]}/>

                                        {
                                            item["new"] &&

                                            <View
                                                style={{
                                                    position: "absolute",
                                                    backgroundColor: mowColors.mainColor,
                                                    top: 5,
                                                    left: 5,
                                                    borderRadius: 200,
                                                    width: hp(4),
                                                    height: hp(4),
                                                    justifyContent: "center"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        textAlign: "center",
                                                        color: "#ffffff",
                                                        fontSize: hp(1.5)
                                                    }}>

                                                    {mowStrings.homeScreen.new}

                                                </Text>

                                            </View>
                                        }

                                    </View>

                                    {/* title text */}
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            marginTop: 5,
                                            fontSize: hp("1.8%"),
                                            fontWeight: "normal",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor
                                        }}>

                                        {item["title"]}

                                    </Text>

                                    {/* star view */}
                                    <View
                                        style={{flexDirection: "row", alignItems: "center", marginTop: 5}}>

                                        {/* stars*/}
                                        <MowStarView
                                            score={item["star"]}/>

                                        {/* vote count text */}
                                        <Text
                                            style={{
                                                marginLeft: 2,
                                                fontSize: hp("1.4%"),
                                                fontWeight: "normal",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "left",
                                                color: mowColors.textColor
                                            }}>

                                            {"("}{item["voteCount"]}{")"}

                                        </Text>

                                    </View>

                                    {/* price text */}
                                    <Text
                                        style={{
                                            marginTop: 5,
                                            fontSize: hp("2%"),
                                            fontWeight: "bold",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor
                                        }}>

                                        {item["price"]}

                                    </Text>

                                </View>

                            )}
                        />

                    </View>

                </ScrollView>

            </MowContainer>

        );
    }
}
