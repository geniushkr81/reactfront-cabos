import React from "react";
import {View, Image, Text} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {borderStyle, fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {MowPicker} from "../../../components/ui/Common/Picker/MowPicker";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import Gender from "../../../SampleData/Gender";
import Language from "../../../SampleData/Language";

export default class Profile extends React.Component {

    /**
     *  these style values are here because of the color change.
     *  when changed the color, styles that are outside the class, are not re-rendered!
     */

    inputStyle = {
        container: {
            marginVertical: 5,
        },
        titleText: {
            fontSize: hp("1.8%"),
            fontWeight: "600",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "left",
            color: mowColors.titleTextColor,
            fontFamily: fontFamily.medium
        },
        inputContainer: {
            width: "100%",
            backgroundColor: mowColors.viewBGColor
        },
        inputText: {
            fontSize: hp("1.8%"),
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            fontFamily: fontFamily.regular,
            textAlign: "left",
            color: mowColors.textColor
        }
    };

    pickerStyle = {
        container: {
            marginVertical: 5,
        },
        button: {
            ...borderStyle,
            borderStyle: "solid",
            borderWidth: 1,
            borderColor: "#afafaf",
            backgroundColor: mowColors.viewBGColor,
            justifyContent: "flex-start",
            paddingLeft: wp(7)
        },
        buttonText: {
            fontWeight: "normal",
            fontStyle: "normal",
            letterSpacing: 0,
            textAlign: "left",
            fontFamily: fontFamily.regular,
            color: mowColors.textColor,
            paddingLeft: wp(3)
        },
        buttonIcon: {
            color: mowColors.mainColor
        }
    };

    state = {
        fullName: "mowega DEV",
        username: "mowega.dev",
        email: "info@mowega.com",
        birthday: "1998-08-21",
        phone: "XXX XXX XX XX",
        genderPicker: false,
        languagePicker: false,
        pickerData: [],
        pickerType: 0,
        pickerVisible: false,
        gender: "Male",
        language: "English"
    };

    // to store entered regular from user
    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    /**
     *      type -->
     *              1: gender
     *              2: language
     * */
    _onSelect(selectedItem) {

        this.setState({pickerVisible: false});

        let type = this.state.pickerType;

        if (type === 1) {
            this.setState({gender: selectedItem["title"], pickerSelectedId: selectedItem["id"]})
        }
        else if (type === 2){
            this.setState({language: selectedItem["title"], pickerSelectedId: selectedItem["id"]})
        }

    }

    _renderInputView(title, leftIcon, key, value, keyboardType = "default") {
        return(

            <View
                style={{marginVertical: 5}}>

                {/* fullName title regular */}
                <Text
                    style={{
                        fontSize: hp("1.8%"),
                        fontWeight: "600",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.titleTextColor,
                        fontFamily: fontFamily.medium
                    }}>

                    {title}

                </Text>

                {/* fullName input */}
                <MowInput
                    keyboardType={keyboardType}
                    leftIcon={leftIcon}
                    containerStyle={{
                        width: "100%",
                        backgroundColor: mowColors.viewBGColor
                    }}
                    textInputStyle={{
                        fontSize: hp("1.8%"),
                        fontWeight: "normal",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        fontFamily: fontFamily.regular,
                        textAlign: "left",
                        color: mowColors.textColor
                    }}
                    value={value}
                    onChangeText={value => this.onChangeText(key, value)}/>

            </View>

        )
    }

    render() {

        let {fullName, username, email, gender, language, phone} = this.state;

        return(

            <MowContainer
                title={mowStrings.profilePage.title}>

                <View
                    style={pageContainerStyle}>

                    {/* user information input view */}
                    <KeyboardAwareScrollView
                        showsVerticalScrollIndicator={false}
                        style={{marginTop: hp("2%"), marginBottom: hp("7%")}}>

                        {/* full name input view */}
                        {this._renderInputView(mowStrings.placeholder.fullName, "user", "fullName", fullName)}

                        {/* username input view */}
                        {this._renderInputView(mowStrings.placeholder.username, "user", "username", username)}

                        {/* email input view */}
                        {this._renderInputView(mowStrings.placeholder.email, "mail", "email", email, "email-address")}

                        {/* phone number input view */}
                        {this._renderInputView(mowStrings.placeholder.phone, "phone", "phone", phone, "phone-pad")}

                        {/* gender picker button view */}
                        <View
                            style={this.inputStyle.container}>

                            {/* gender title regular */}
                            <Text
                                style={this.inputStyle.titleText}>

                                {mowStrings.placeholder.gender}

                            </Text>

                            {/* gender picker button */}
                            <MowButton
                                stickyIcon={true}
                                buttonText={this.state.gender}
                                onPress={() => {this.setState({pickerData: Gender, pickerVisible: true, pickerType: 1})}}
                                leftIcon={"user"}
                                leftIconStyle={this.pickerStyle.buttonIcon}
                                textStyle={this.pickerStyle.buttonText}
                                containerStyle={this.pickerStyle.button}/>

                        </View>

                        {/* language picker button view */}
                        <View
                            style={this.inputStyle.container}>

                            {/* language title regular */}
                            <Text
                                style={this.inputStyle.titleText}>

                                {mowStrings.placeholder.language}

                            </Text>

                            {/* language picker button */}
                            <MowButton
                                stickyIcon={true}
                                buttonText={this.state.language}
                                onPress={() => {this.setState({pickerData: Language, pickerVisible: true, pickerType: 2})}}
                                leftIcon={"globe"}
                                leftIconStyle={this.pickerStyle.buttonIcon}
                                textStyle={this.pickerStyle.buttonText}
                                containerStyle={this.pickerStyle.button}/>

                        </View>

                    </KeyboardAwareScrollView>

                    <MowButton
                        buttonText={mowStrings.button.save}
                        containerStyle={{position: "absolute", bottom: 0, alignSelf: "center"}}
                        onPress={() => {}}
                        type={"success"}/>

                </View>

                <MowPicker
                    pickerVisible={this.state.pickerVisible}
                    pickerOnClose={() => this.setState({pickerVisible: false})}
                    selectedValue={this.state.pickerSelectedId}
                    search={false}
                    data={this.state.pickerData}
                    onSelect={(obj) => {this._onSelect(obj)}}/>

            </MowContainer>

        )

    }

}
