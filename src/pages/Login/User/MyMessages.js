import React from "react";
import {FlatList, Text, View} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {TouchableOpacity} from "react-native-gesture-handler";
import MessageData from "../../../SampleData/User/MessageData";
import {mowColors} from "../../../values/Colors/MowColors";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import {fontFamily, shadowStyle} from "../../../values/Styles/MowStyles";

export default class MyMessages extends React.Component {

    _renderMessageItem(item, index) {
        return (

            <TouchableOpacity
                style={{
                    flexDirection: "row",
                    margin: 10,
                    ...shadowStyle,
                    backgroundColor: mowColors.viewBGColor,
                    padding: 10,
                    alignItems: "center",
                    borderRadius: 5
                }}
                onPress={() => {}}>

                {/* abbreviation view */}
                <View
                    style={{
                        borderRadius: 100,
                        width: hp(5),
                        height: hp(5),
                        backgroundColor: mowColors.mainColor,
                        alignItems: "center",
                        justifyContent: "center",

                    }}>

                    <Text
                        style={{
                            color: "white",
                            fontSize: hp(2)
                        }}>

                        {item.abbreviation}

                    </Text>

                </View>

                <View
                    style={{width: "90%", paddingHorizontal: 10}}>

                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",

                        }}>

                        {/* title text */}
                        <Text
                            style={{
                                color: mowColors.titleTextColor,
                                fontFamily: fontFamily.bold,
                                fontSize: hp(1.7)
                            }}>

                            {item.title}

                        </Text>

                        {/* date text */}
                        <Text
                            style={{
                                color: mowColors.titleTextColor,
                                fontFamily: fontFamily.regular,
                                fontSize: hp(1.6)
                            }}>

                            {item.date}

                        </Text>

                    </View>

                    <Text
                        numberOfLines={2}
                        style={{
                            marginTop: 5,
                            color: mowColors.textColor,
                            fontFamily: fontFamily.regular,
                            fontSize: hp(1.7)
                        }}>

                        {item.message}

                    </Text>

                </View>

            </TouchableOpacity>

        )
    }

    render() {

        return(

            <MowContainer
                title={mowStrings.myMessages.title}>

                <View
                    style={{flex: 1}}>

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={MessageData}
                        renderItem={({item, index}) => this._renderMessageItem(item, index)}/>

                </View>

            </MowContainer>

        )

    }

}
