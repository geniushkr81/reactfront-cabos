import React from "react";
import {Text, View, TouchableOpacity, ScrollView} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {cardStyle, fontFamily} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import MowListItemIcon from "../../../components/ui/ListItem/MowListItemIcon";
import {navigate} from "../../../RootMethods/RootNavigation";
import {heightPercentageToDP as hp} from "react-native-responsive-screen"

export default class MyAccount extends React.Component {

    _renderTopItem(icon, text, navigateTo = "") {
        return (
            <TouchableOpacity
                style={{
                    margin: 10,
                    padding: 5,
                    alignItems: "center",
                    justifyContent: "center",
                }}
                onPress={() => {navigate(navigateTo)}}>

                <MCIcon
                    style={{
                        color: mowColors.mainColor,
                        fontSize: hp(4),
                        marginBottom: 5
                    }}
                    name={icon}/>

                {/* item text */}
                <Text
                    style={{
                        fontSize: hp(1.5),
                        color: mowColors.titleTextColor,
                        fontFamily: fontFamily.regular,
                        textAlign: "center",
                    }}>

                    {text}

                </Text>

            </TouchableOpacity>
        )
    }

    _renderBottomItem(icon, text, navigateTo = "") {
        return(

            <MowListItemIcon
                iconName={icon}
                title={text}
                style={{
                    backgroundColor: mowColors.viewBGColor,
                    marginHorizontal: 15,
                    borderRadius: 5,
                    marginTop: 15,
                    height: hp(8)
                }}
                onPress={() => navigate(navigateTo)}/>

        )
    }

    render() {

        return(

            <MowContainer
                footerActiveIndex={4}
                title={mowStrings.myAccountScreen.title}>

                <View
                    style={{backgroundColor: mowColors.pageBGColor, flex: 1}}>

                    {/* top view */}
                    <View
                        style={{flex: 1}}>

                        <View
                            style={{flex: 2, backgroundColor: mowColors.mainColor}}/>

                        <View
                            style={{flex: 1, backgroundColor: mowColors.pageBGColor}}/>

                        {/* card view */}
                        <View
                            style={{
                                position: "absolute",
                                alignSelf: "center",
                                alignItems: "center",
                                justifyContent: "space-between",
                                flexDirection: "row",
                                backgroundColor: mowColors.viewBGColor,
                                top: 20,
                                ...cardStyle,
                            }}>

                           
                        </View>

                    </View>

                    {/* bottom view */}
                    <View
                        style={{flex: 5, paddingHorizontal: 10, marginTop: 20}}>

                        <ScrollView>

                            {/* my orders view */}
                            {this._renderBottomItem("box", mowStrings.myAccountScreen.myOrders, "OrderList")}

                            {/* my evaluations view */}
                            {this._renderBottomItem("message-square", mowStrings.myAccountScreen.myEvaluations, "")}

                            {/* my messages view */}
                            {this._renderBottomItem("mail", mowStrings.myAccountScreen.myMessages, "MyMessages")}

                            {/* notifications view */}
                            {this._renderBottomItem("bell", mowStrings.myAccountScreen.notifications, "Notifications")}

                            {/* account information view */}
                            {this._renderBottomItem("settings", mowStrings.myAccountScreen.accountInformation, "AccountInformation")}

                            {/* help view */}
                            {this._renderBottomItem("help-circle", mowStrings.myAccountScreen.help, "FAQ")}

                        </ScrollView>

                    </View>

                </View>

            </MowContainer>

        )

    }

}
