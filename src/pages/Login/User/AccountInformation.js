import React from "react";
import {View} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {pageContainerStyle} from "../../../values/Styles/MowStyles";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import MowListItemIcon from "../../../components/ui/ListItem/MowListItemIcon";
import {navigate} from "../../../RootMethods/RootNavigation";

export default class AccountInformation extends React.Component {

    _renderBottomItem(icon, text, navigateTo = "", params = {}) {
        return(

            <MowListItemIcon
                iconName={icon}
                title={text}
                onPress={() => navigate(navigateTo, params)}/>

        )
    }

    render() {

        return(

            <MowContainer
                title={mowStrings.accountInformationScreen.title}>

                <View
                    style={pageContainerStyle}>

                    {/* user information view */}
                    {this._renderBottomItem("user", mowStrings.accountInformationScreen.userInformation, "Profile")}

                    {/* user information view */}
                    {this._renderBottomItem("map", mowStrings.accountInformationScreen.addressInformation, "AddressList", {cart: false})}

                    {/* user information view */}
                    {this._renderBottomItem("lock", mowStrings.accountInformationScreen.changePassword, "Password")}


                </View>

            </MowContainer>

        )

    }

}
