import React from "react";
import {FlatList, View, Text, Image, TouchableOpacity, ScrollView} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {fontFamily, pageContainerStyle} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import SyncStorage from "sync-storage";
import {MowListItem} from "../../../components/ui/ListItem/MowListItem";
import {navigate} from "../../../RootMethods/RootNavigation";
import CategoriesData from "../../../SampleData/Category/CategoriesData";
import {mowColors} from "../../../values/Colors/MowColors";
import TrendCategories from "../../../SampleData/TrendCategories";

export default class Categories extends React.Component {

    state = {
        category: 1,
        selectedCategoryIndex: 0
    };

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener("focus", () => {

            // to get category value that selected from settings
            let category = SyncStorage.get("category");
            if (!category){
                // to set category value
                this.setState({category: 1});
            }
            else {
                // to set category value
                this.setState({category: category});
            }
        })
    }

    componentWillUnmount() {
        this.focusListener();
    }

    render() {

        let {category, selectedCategoryIndex} = this.state;

        return(

            <MowContainer
                footerActiveIndex={2}
                title={mowStrings.categories.title}>

                {/* category list 1 */}
                {
                    category === 1 &&

                    <View
                        style={{flexDirection: "row"}}>

                        <ScrollView
                            style={{
                                width: "25%",
                                borderRightWidth: 1,
                                borderRightColor: mowColors.mainColor
                            }}>

                            {
                                TrendCategories.map((item, index) => {
                                    return (
                                        <TouchableOpacity
                                            key={index}
                                            onPress={() => {this.setState({selectedCategoryIndex: index})}}
                                            style={{
                                                padding: 10,
                                                borderBottomWidth: CategoriesData.length !== index + 1 ? 1 : 0,
                                                borderBottomColor: mowColors.categoryTextColor,
                                                backgroundColor: mowColors.mainColor,
                                                alignItems: "center",
                                                justifyContent: "center",
                                            }}>

                                            <Image
                                                style={{width: hp("7%"), height: hp("7%")}}
                                                source={item["image"]}
                                                resizeMode={"contain"}/>

                                            <Text
                                                numberOfLines={2}
                                                style={{
                                                    marginTop: 5,
                                                    color: mowColors.categoryTextColor,
                                                    textAlign: "center",
                                                    paddingHorizontal: 1,
                                                    fontSize: hp(1.5)
                                                }}>

                                                {item["title"]}

                                            </Text>

                                        </TouchableOpacity>
                                    )
                                })
                            }

                        </ScrollView>

                        <View
                            style={{width: "75%"}}>

                            {/* title text */}
                            <Text
                                style={{
                                    color: mowColors.titleTextColor,
                                    fontSize: hp(2),
                                    textAlign: "center",
                                    marginTop: 20,
                                    fontWeight: "bold",
                                    fontFamily: fontFamily.bold
                                }}>

                                {TrendCategories[selectedCategoryIndex]["title"]}

                            </Text>

                            <View
                                style={{
                                    height: 1,
                                    width: "80%",
                                    backgroundColor: mowColors.mainColor,
                                    alignSelf: "center",
                                    marginTop: 5,
                                    opacity: 0.8
                                }}/>

                            <FlatList
                                key={selectedCategoryIndex}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                                data={TrendCategories[selectedCategoryIndex]["subCategory"]}
                                style={{marginTop: 10}}
                                renderItem={({ item, index }) => (

                                    // sub category item view
                                    <View
                                        key={index}
                                        style={{marginLeft: "3%", marginVertical: 10}}>

                                        {/* sub category item */}
                                        <MowListItem
                                            onPress={() => {navigate("ProductList")}}
                                            style={{height: hp("5%")}}
                                            title={item["title"]}/>

                                    </View>

                                )}/>

                        </View>

                    </View>

                }

                {/* category list 2 */}
                {
                    category === 2 &&

                    <FlatList
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={CategoriesData}
                        style={[pageContainerStyle]}
                        renderItem={({ item, index }) => (

                            // category item
                            <TouchableOpacity
                                onPress={() => {this.props.navigation.navigate("CategoryDetail")}}
                                key={index}
                                style={{flex: 1, margin: 10, marginHorizontal: 15, alignItems: "center", justifyContent: "flex-end", borderRadius: 5}}>

                                <Image
                                    style={{width: "100%", height: hp(18), borderRadius: 5}}
                                    source={item["image"]}
                                    resizeMode={"stretch"}/>

                                <View
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        position: "absolute",
                                        zIndex: 1,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        opacity: 0.45,
                                        borderRadius: 5,
                                        backgroundColor: "#000000"
                                    }}/>

                                <Text
                                    style={{
                                        position: "absolute",
                                        color: "white",
                                        fontSize: hp(2),
                                        textAlign: "center",
                                        zIndex: 2,
                                        fontWeight: "bold",
                                        fontStyle: "normal",
                                        lineHeight: 27,
                                        letterSpacing: 0,
                                        bottom: 10
                                    }}>

                                    {item["title"]}

                                </Text>

                            </TouchableOpacity>

                        )}
                    />
                }

                {/* category list 3 */}
                {
                    category === 3 &&

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={CategoriesData}
                        style={[pageContainerStyle]}
                        renderItem={({ item, index }) => (

                            // category item
                            <MowListItem
                                key={index}
                                style={{marginVertical: 5, borderRadius: 5}}
                                onPress={() => {navigate("CategoryDetail")}}
                                imagePath={item["image"]}
                                title={item["title"]}/>

                        )}
                    />
                }

            </MowContainer>

        )

    }

}
