import React from "react";
import {View, Text, Image, ScrollView, TouchableOpacity, FlatList} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {borderStyle, categoryStyleWithoutShadow, fontFamily, shadowStyle} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import Swiper from "react-native-swiper";
import {MowStarView} from "../../../components/ui/AppSpecifics/StarView/MowStarView";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {navigate} from "../../../RootMethods/RootNavigation";
import BodySize from "../../../SampleData/Product/BodySize";
import Colors from "../../../SampleData/Colors";
import CustomerComments from "../../../SampleData/CustomerComments";
import ShoeSize from "../../../SampleData/Product/ShoeSize";
import {isTablet} from "../../../values/Constants/MowConstants";

export default class ProductDetail2 extends React.Component {

    state = {
        product: this.props.route.params["product"],
        colorArr: [],
        colorListKey: 0,
        sizeArr: [],
        sizeListKey: 0,
        showMore: false,
        commentListKey: 0,
        buttonType: 1,
        activeSwiperButton: 1,
        index: 0
    };

    _handleSizeSelection(index) {
        let sizeArr = this.state.sizeArr;

        let length = BodySize.length;

        for (let i = 0; i < length; i++) {
            if (i != index) {
                // to set false all array values except selected index
                sizeArr[i] = false;
            }
        }

        // to update selected item as its opposite
        sizeArr[index] = !sizeArr[index];

        this.setState({sizeArr: sizeArr, sizeListKey: this.state.sizeListKey + 1})
    }

    _handleColorSelection(index) {
        let colorArr = this.state.colorArr;

        let length = Colors.length;

        for (let i = 0; i < length; i++) {
            if (i != index) {
                // to set false all array values except selected index
                colorArr[i] = false;
            }
        }

        // to update selected item as its opposite
        colorArr[index] = !colorArr[index];

        this.setState({colorArr: colorArr, colorListKey: this.state.colorListKey + 1})
    }

    _commentRow(item) {

        return(

            <View
                style={{
                    marginVertical: 5,
                    borderRadius: 5,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "rgba(146, 146, 146, 0.41)",
                    padding: 5
                }}>

                <View
                    style={{flexDirection: "row"}}>

                    {/* image view */}
                    <Image
                        style={{
                            width: hp("5%"),
                            height: hp("5%"),
                        }}
                        resizeMode={"contain"}
                        source={item["image"]}/>

                    <View
                        style={{marginLeft: 10}}>

                        {/* name text */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor
                            }}>

                            {item["name"]}

                        </Text>

                        {/* date text */}
                        <Text
                            style={{
                                fontSize: hp("1.4%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.textColor
                            }}>

                            {item["date"]}

                        </Text>

                    </View>

                    <View
                        style={{marginLeft: 10, justifyContent: "center"}}>

                        {/* star view */}
                        <MowStarView
                            score={item["score"]}/>

                    </View>

                </View>

                {/* description text */}
                <Text
                    style={{
                        marginTop: 5,
                        fontSize: hp("1.5%"),
                        fontWeight: "300",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.textColor
                    }}>

                    {item["description"]}

                </Text>

            </View>

        )

    }

    _handleSwipe(destinationIndex) {
        let index = this.state.index;

        // to handle first index
        if (index === 0) {
            this.swiper.scrollBy(destinationIndex);
        }
        // to handle second index
        else if (index === 1) {
            this.swiper.scrollBy(destinationIndex - index);
        }
        else if (index === 2) {
            this.swiper.scrollBy(-1 * (index - destinationIndex));
        }
    }

    _handleSwiperButtonClick(buttonType) {
        this.setState({activeSwiperButton: buttonType})
    }

    _renderProduct() {

        return (

            <View>

                {/* size view */}
                <View
                    style={[categoryStyleWithoutShadow, {backgroundColor: mowColors.categoryBGColor}]}>

                    {/* body size text */}
                    <Text
                        style={{
                            marginBottom: 10,
                            fontSize: hp("1.8%"),
                            fontWeight: "600",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "left",
                            color: mowColors.titleTextColor
                        }}>

                        {mowStrings.productDetail.shoeSize}

                    </Text>

                    {/* size list view */}
                    <FlatList
                        key={this.state.sizeListKey}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={ShoeSize}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity
                                onPress={() => {this._handleSizeSelection(index)}}
                                style={{
                                    width: hp("4%"),
                                    height: hp("4%"),
                                    marginLeft: index != 0 ? 10 : 3,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.sizeArr[index] ? mowColors.mainColor : "#ffffff",
                                    ...borderStyle,
                                    marginRight: 1,
                                    borderRadius: 100,
                                    ...shadowStyle,
                                    marginBottom: 10
                                }}
                                key={index}>

                                <Text
                                    style={{
                                        color: this.state.sizeArr[index] ? "white" : mowColors.mainColor,
                                        fontSize: hp("1.5%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "center",
                                    }}>

                                    {item["size"]}

                                </Text>

                            </TouchableOpacity>

                        )}
                    />

                </View>

                {/* color view */}
                <View
                    style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                    {/* color text */}
                    <Text
                        style={{
                            marginBottom: 10,
                            fontSize: hp("1.8%"),
                            fontWeight: "600",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "left",
                            color: mowColors.titleTextColor
                        }}>

                        {mowStrings.productDetail.color}

                    </Text>

                    {/* color list */}
                    <FlatList
                        key={this.state.colorListKey}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={Colors}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity
                                onPress={() => {this._handleColorSelection(index)}}
                                style={{
                                    width: hp("4%"),
                                    height: hp("4%"),
                                    borderRadius: 100,
                                    backgroundColor: item["color"],
                                    marginLeft: index != 0 ? 10 : 3,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginRight: 1,
                                    ...shadowStyle,
                                    marginBottom: 10
                                }}
                                key={index}>

                                {
                                    this.state.colorArr[index] &&

                                    <FAIcon
                                        style={{color: "white", fontSize: hp("2.5%")}}
                                        name={"check"}/>
                                }

                            </TouchableOpacity>

                        )}
                    />

                </View>

            </View>

        )
    }

    _renderDetails() {

        let product = this.state.product;

        return (

            <View
                style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                <Text
                    style={{
                        fontSize: hp("1.8%"),
                        fontWeight: "600",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.titleTextColor
                    }}>

                    {mowStrings.productDetail.productFeature}

                </Text>

                {/* product feature text */}
                <Text
                    style={{
                        marginTop: 2,
                        fontSize: hp("1.6%"),
                        fontWeight: "300",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.textColor,
                    }}>

                    {product["productFeature"]}

                </Text>

            </View>

        )
    }

    _renderReviews() {
        return (

            <View
                style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor, height: isTablet ? hp(40) : hp(30)}]}>

                {/* comment list */}
                <FlatList
                    nestedScrollEnabled={true}
                    key={this.state.commentListKey}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    style={{height: isTablet ? hp(40) : hp(30)}}
                    data={CustomerComments}
                    renderItem={({ item, index }) => (

                        <View
                            key={index}>

                            {this._commentRow(item, index)}

                        </View>

                    )}
                />

            </View>

        )
    }

    _renderSwiperButton(text, buttonType) {

        let selected = (buttonType === this.state.activeSwiperButton)

        return(

            <TouchableOpacity
                onPress={() => {
                    //to update button activity ui
                    this._handleSwiperButtonClick(buttonType)
                    // to swipe current page
                    this._handleSwipe(buttonType - 1);
                }}
                style={{
                    flex: 1,
                    margin: 10,
                    alignItems: "center",
                    justifyContent: "center",
                    padding: 10,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: mowColors.mainColor,
                    backgroundColor: selected ? mowColors.mainColor : "transparent"
                }}>

                <Text
                    style={{
                        textAlign: "center",
                        fontSize: hp(1.6),
                        color: selected ? "white" : mowColors.titleTextColor,
                        fontFamily: fontFamily.bold
                    }}>

                    {text}

                </Text>

            </TouchableOpacity>

        )
    }

    render() {

        const product = this.state.product;

        return(

            <MowContainer
                style={{backgroundColor: mowColors.pageBGDarkColor}}
                title={mowStrings.productDetail.title2}>

                <ScrollView>

                    {/* product info view */}
                    <View
                        style={[categoryStyleWithoutShadow, {backgroundColor: mowColors.categoryBGColor}]}>

                        {/* image swiper view */}
                        <View
                            style={{height: hp(35)}}>

                            {/* product image swiper */}
                            <Swiper
                                removeClippedSubviews={false}
                                ref='swiper'
                                pagingEnabled={true}
                                showsPagination={true}
                                horizontal={true}
                                loop={false}
                                dotColor={"grey"}
                                activeDotColor={mowColors.mainColor}
                                paginationStyle={{bottom: hp("1%")}}
                                autoplay={false}>

                                {
                                    product["images"].map((item, key) => {

                                        return (

                                            <Image
                                                key={key}
                                                style={{
                                                    height: hp(25),
                                                    width: "100%",
                                                }}
                                                resizeMode={"contain"}
                                                source={item["image"]}/>

                                        )

                                    })
                                }

                            </Swiper>

                            <View
                                style={{
                                    justifyContent: "space-between",
                                    marginVertical: 3,
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}>

                                {/* price text */}
                                <Text
                                    style={{
                                        fontSize: hp("1.8%"),
                                        fontWeight: "300",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        color: mowColors.mainColor
                                    }}>

                                    {product["currency"]}{product["lastPrice"]}

                                </Text>

                                <MowStarView
                                    score={product["star"]}/>

                            </View>

                            <View
                                style={{
                                    marginTop: 5,
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}>

                                {/* product name text */}
                                <Text
                                    style={{
                                        fontSize: hp("2%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        color: mowColors.titleTextColor,
                                        flex: 2
                                    }}>

                                    {product["title"]}

                                </Text>

                                {/* add to cart button view */}
                                <View
                                    style={{flex: 1}}>

                                    <MowButton
                                        buttonText={mowStrings.button.addToCart}
                                        containerStyle={{margin: 0}}
                                        onPress={() => {navigate("Cart")}}
                                        textStyle={{fontSize: hp("1.5%")}}
                                        size={"small"}
                                        type={"success"}/>

                                </View>

                            </View>

                        </View>

                    </View>

                    {/* content view */}
                    <View
                        style={{height: hp(40)}}>

                        {/* swiper button view */}
                        <View
                            style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>

                            {/* product tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.product, 1)}

                            {/* detail tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.detail, 2)}

                            {/* review tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.review, 3)}

                        </View>

                        <Swiper
                            removeClippedSubviews={false}
                            ref={(swiper) => {this.swiper = swiper}}
                            onIndexChanged={(index) => {
                                this.setState({index: index});

                                if (index === 0) {
                                    this._handleSwiperButtonClick(1)
                                }
                                else if (index === 1) {
                                    this._handleSwiperButtonClick(2)
                                }
                                else {
                                    this._handleSwiperButtonClick(3)
                                }
                            }}
                            pagingEnabled={true}
                            showsPagination={false}
                            horizontal={true}
                            loop={false}
                            autoplay={false}>

                            {/* render product */}
                            {this._renderProduct()}

                            {/* render details*/}
                            {this._renderDetails()}

                            {/* render reviews */}
                            {this._renderReviews()}

                        </Swiper>

                    </View>

                </ScrollView>

            </MowContainer>

        )

    }

}
