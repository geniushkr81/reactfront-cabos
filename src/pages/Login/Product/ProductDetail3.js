import React from "react";
import {View, Text, Image, ScrollView, TouchableOpacity, FlatList} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {
    categoryStyleWithoutShadow,
    fontFamily, gPadding,
    shadowStyle
} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import Swiper from "react-native-swiper";
import {MowStarView} from "../../../components/ui/AppSpecifics/StarView/MowStarView";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {navigate} from "../../../RootMethods/RootNavigation";
import CustomerComments from "../../../SampleData/CustomerComments";
import {isTablet} from "../../../values/Constants/MowConstants";

export default class ProductDetail3 extends React.Component {

    state = {
        product: this.props.route.params["product"],
        showMore: false,
        commentListKey: 0,
        buttonType: 1,
        activeSwiperButton: 1,
        index: 0
    };

    _commentRow(item) {

        return(

            <View
                style={{
                    marginVertical: 5,
                    borderRadius: 5,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "rgba(146, 146, 146, 0.41)",
                    padding: 5
                }}>

                <View
                    style={{flexDirection: "row"}}>

                    {/* image view */}
                    <Image
                        style={{
                            width: hp("5%"),
                            height: hp("5%"),
                        }}
                        resizeMode={"contain"}
                        source={item["image"]}/>

                    <View
                        style={{marginLeft: 10}}>

                        {/* name text */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor
                            }}>

                            {item["name"]}

                        </Text>

                        {/* date text */}
                        <Text
                            style={{
                                fontSize: hp("1.4%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.textColor
                            }}>

                            {item["date"]}

                        </Text>

                    </View>

                    <View
                        style={{marginLeft: 10, justifyContent: "center"}}>

                        {/* star view */}
                        <MowStarView
                            score={item["score"]}/>

                    </View>

                </View>

                {/* description text */}
                <Text
                    style={{
                        marginTop: 5,
                        fontSize: hp("1.5%"),
                        fontWeight: "300",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.textColor
                    }}>

                    {item["description"]}

                </Text>

            </View>

        )

    }

    _handleSwipe(destinationIndex) {
        let index = this.state.index;

        // to handle first index
        if (index === 0) {
            this.swiper.scrollBy(destinationIndex);
        }
        // to handle second index
        else if (index === 1) {
            this.swiper.scrollBy(destinationIndex - index);
        }
        else if (index === 2) {
            this.swiper.scrollBy(-1 * (index - destinationIndex));
        }
    }

    _handleSwiperButtonClick(buttonType) {
        this.setState({activeSwiperButton: buttonType})
    }

    _renderProduct() {

        let {product} = this.state;

        return (

            <View
                style={{
                    marginTop: 15,
                    backgroundColor: mowColors.categoryBGColor,
                    padding: gPadding,
                    ...shadowStyle
                }}>

                {/* brand view */}
                {this._renderProductProperties(mowStrings.productDetail.brand, product["brand"])}

                {/* os view */}
                {this._renderProductProperties(mowStrings.productDetail.os, product["os"])}

                {/* CPU view */}
                {this._renderProductProperties(mowStrings.productDetail.cpuManufacturer, product["cpu"])}

                {/* screen size view */}
                {this._renderProductProperties(mowStrings.productDetail.screenSize, product["screenSize"])}

                {/* memory size view */}
                {this._renderProductProperties(mowStrings.productDetail.computerMemorySize, product["memorySize"])}

            </View>

        )
    }

    _renderDetails() {

        let product = this.state.product;

        return (

            <View
                style={{
                    marginTop: 15,
                    backgroundColor: mowColors.categoryBGColor,
                    padding: gPadding,
                    ...shadowStyle
                }}>

                <Text
                    style={{
                        fontSize: hp("1.8%"),
                        fontWeight: "600",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.titleTextColor
                    }}>

                    {mowStrings.productDetail.productFeature}

                </Text>

                {/* product properties flatList */}
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={product["productFeatures"]}
                    style={{marginTop: 10}}
                    renderItem={({ item, index }) => (

                        <View
                            key={index}>

                            {this._renderProductDetail(item, index)}

                        </View>

                    )}
                />

            </View>

        )
    }

    _renderReviews() {
        return (

            <View
                style={{
                    marginTop: 15,
                    backgroundColor: mowColors.categoryBGColor,
                    padding: gPadding,
                    ...shadowStyle,
                    height: isTablet ? hp(40) : hp(30)
                }}>

                {/* comment list */}
                <FlatList
                    nestedScrollEnabled={true}
                    key={this.state.commentListKey}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    style={{height: isTablet ? hp(40) : hp(30)}}
                    data={CustomerComments}
                    renderItem={({ item, index }) => (

                        <View
                            key={index}>

                            {this._commentRow(item, index)}

                        </View>

                    )}
                />

            </View>

        )
    }

    _renderProductProperties(title, text) {
        return(

            <View
                style={{
                    flexDirection: "row",
                    marginVertical: 10,
                }}>

                {/* title text view */}
                <View
                    style={{flex: 3}}>

                    <Text
                        style={{
                            color: mowColors.titleTextColor,
                            fontWeight: "500",
                            fontSize: hp(1.7)
                        }}>

                        {title}

                    </Text>

                </View>

                {/* text view */}
                <View
                    style={{flex: 2}}>

                    <Text
                        style={{
                            color: mowColors.textColor,
                            fontWeight: "500",
                            fontSize: hp(1.6)
                        }}>

                        {text}

                    </Text>

                </View>

            </View>

        )
    }

    _renderProductDetail(item, index) {

        return(

            <View
                key={index}
                style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginVertical: 5
                }}>

                {/* title text view */}
                <View
                    style={{
                        width: hp(1.5),
                        height: hp(1.5),
                        borderRadius: 100,
                        backgroundColor: mowColors.mainColor
                    }}/>

                {/* text view */}
                <View
                    style={{flex: 1}}>

                    <Text
                        style={{
                            color: mowColors.textColor,
                            fontWeight: "500",
                            fontSize: hp(1.7),
                            marginLeft: 10
                        }}>

                        {item["feature"]}

                    </Text>

                </View>

            </View>

        )
    }

    _renderSwiperButton(text, buttonType) {

        let selected = (buttonType === this.state.activeSwiperButton)

        return(

            <TouchableOpacity
                onPress={() => {
                    //to update button activity ui
                    this._handleSwiperButtonClick(buttonType)
                    // to swipe current page
                    this._handleSwipe(buttonType - 1);
                }}
                style={{
                    flex: 1,
                    margin: 10,
                    alignItems: "center",
                    justifyContent: "center",
                    padding: 10,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: mowColors.mainColor,
                    backgroundColor: selected ? mowColors.mainColor : "transparent"
                }}>

                <Text
                    style={{
                        textAlign: "center",
                        fontSize: hp(1.6),
                        color: selected ? "white" : mowColors.titleTextColor,
                        fontFamily: fontFamily.bold
                    }}>

                    {text}

                </Text>

            </TouchableOpacity>

        )
    }

    render() {

        const product = this.state.product;

        return(

            <MowContainer
                style={{backgroundColor: mowColors.pageBGDarkColor}}
                title={mowStrings.productDetail.title3}>

                <ScrollView>

                    {/* product info view */}
                    <View
                        style={[categoryStyleWithoutShadow, {backgroundColor: mowColors.categoryBGColor}]}>

                        {/* image swiper view */}
                        <View
                            style={{height: hp(33)}}>

                            {/* product image swiper */}
                            <Swiper
                                removeClippedSubviews={false}
                                ref='swiper'
                                pagingEnabled={true}
                                showsPagination={true}
                                horizontal={true}
                                loop={false}
                                dotColor={"grey"}
                                activeDotColor={mowColors.mainColor}
                                paginationStyle={{bottom: hp("1%")}}
                                autoplay={false}>

                                {
                                    product["images"].map((item, key) => {

                                        return (

                                            <Image
                                                key={key}
                                                style={{
                                                    height: hp(20),
                                                    width: "100%",
                                                }}
                                                resizeMode={"contain"}
                                                source={item["image"]}/>

                                        )

                                    })
                                }

                            </Swiper>

                            <View
                                style={{
                                    justifyContent: "space-between",
                                    marginVertical: 3,
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}>

                                {/* price text */}
                                <Text
                                    style={{
                                        fontSize: hp("1.8%"),
                                        fontWeight: "300",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        color: mowColors.mainColor
                                    }}>

                                    {product["currency"]}{product["lastPrice"]}

                                </Text>

                                <MowStarView
                                    score={product["star"]}/>

                            </View>

                            <View
                                style={{
                                    marginTop: 5,
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}>

                                {/* product name text */}
                                <Text
                                    style={{
                                        fontSize: hp("2%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        color: mowColors.titleTextColor,
                                        flex: 2
                                    }}>

                                    {product["title"]}

                                </Text>

                                {/* add to cart button view */}
                                <View
                                    style={{flex: 1}}>

                                    <MowButton
                                        buttonText={mowStrings.button.addToCart}
                                        containerStyle={{margin: 0}}
                                        onPress={() => {navigate("Cart")}}
                                        textStyle={{fontSize: hp("1.5%")}}
                                        size={"small"}
                                        type={"success"}/>

                                </View>

                            </View>

                        </View>

                    </View>

                    {/* content view */}
                    <View
                        style={{height: hp(40)}}>

                        {/* swiper button view */}
                        <View
                            style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>

                            {/* product tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.product, 1)}

                            {/* detail tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.detail, 2)}

                            {/* review tab button */}
                            {this._renderSwiperButton(mowStrings.productDetail.review, 3)}

                        </View>

                        <Swiper
                            removeClippedSubviews={false}
                            ref={(swiper) => {this.swiper = swiper}}
                            onIndexChanged={(index) => {
                                this.setState({index: index});

                                if (index === 0) {
                                    this._handleSwiperButtonClick(1)
                                }
                                else if (index === 1) {
                                    this._handleSwiperButtonClick(2)
                                }
                                else {
                                    this._handleSwiperButtonClick(3)
                                }
                            }}
                            pagingEnabled={true}
                            showsPagination={false}
                            horizontal={true}
                            loop={false}
                            autoplay={false}>

                            {/* render product */}
                            {this._renderProduct()}

                            {/* render details*/}
                            {this._renderDetails()}

                            {/* render reviews */}
                            {this._renderReviews()}

                        </Swiper>

                    </View>

                </ScrollView>

            </MowContainer>

        )

    }

}
