import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {MowPicker} from "../../../components/ui/Common/Picker/MowPicker";
import {fontFamily} from "../../../values/Styles/MowStyles";
import {navigate} from "../../../RootMethods/RootNavigation";
import WomanClothing from "../../../SampleData/Product/WomanClothing";
import {MowProductList} from "../../../components/ui/AppSpecifics/MowProductList";

let pickerSortData = [
    {id: 4, title: mowStrings.picker.sort.lowestPrice},
    {id: 5, title: mowStrings.picker.sort.highestPrice},
    {id: 6, title: mowStrings.picker.sort.topRated},
    {id: 7, title: mowStrings.picker.sort.highestScore},
    {id: 8, title: mowStrings.picker.sort.lowestScore},
    {id: 1, title: mowStrings.picker.sort.smartSorting},
    {id: 2, title: mowStrings.picker.sort.bestSeller},
    {id: 3, title: mowStrings.picker.sort.newest}
];

export default class ProductList extends React.Component {

    state = {
        pickerVisible: false,
        pickerSelectedId: null,
        productList: WomanClothing,
        productListKey: 0,
        boxView: true,
    };

    // to handle (sort) picker selection
    _onSelect(selectedItem) {

        /**
         * id --> selected item id for sorting
         *
         *      1 --> according to the smart sorting
         *      2 --> according to the best seller
         *      3 --> according to the newest value
         *      4 --> according to the price (lowest -> highest)
         *      5 --> according to the price (highest -> lowest)
         *      6 --> according to the top rate
         *      7 --> according to the score (highest -> lowest)
         *      8 --> according to the score (lowest -> highest)
         *
         * */

        let id = selectedItem["id"];

        // selected id control
        switch (id) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                this._increaseSort("lastPrice");
                break;
            case 5:
                this._decreaseSort("lastPrice");
                break;
            case 6:
                this._decreaseSort("voteCount");
                break;
            case 7:
                this._decreaseSort("star");
                break;
            case 8:
                this._increaseSort("star");
                break;
        }

        // to update selected id & picker visibility
        this.setState({
            pickerSelectedId: id,
            pickerVisible: false
        })

    }

    // ascending order according to the key
    _increaseSort(productKey) {
        let products = this.state.productList;

        // to sort and update the product array
        products = products.sort((a,b) => parseFloat(a[productKey]) - parseFloat(b[productKey]));

        // to update list
        this.setState({productList: products, productListKey: this.state.productListKey});
    }

    // descending order according to the key
    _decreaseSort(productKey) {
        let products = this.state.productList;

        // to sort and update the product array
        products = products.sort((a,b) => parseFloat(b[productKey]) - parseFloat(a[productKey]));

        // to update list
        this.setState({productList: products, productListKey: this.state.productListKey});
    }

    render() {

        let {boxView, productList, productListKey} = this.state;

        return(

            <MowContainer
                title={"Woman Clothing"}>

                {/* filter view */}
                <View
                    style={{
                        marginVertical: hp("2%"),
                        marginHorizontal: wp("2%"),
                        borderRadius: 5,
                        backgroundColor: mowColors.filterHeaderBG,
                        padding: 10,
                        flexDirection: "row",
                    }}>

                    {/* icon view */}
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({boxView: !this.state.boxView, productListKey: this.state.productListKey + 1});

                        }}
                        style={{justifyContent: "center", alignItems: "center", flex: 2}}>

                        <FAIcon
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp("3%"),
                                flex: 1,
                                height: "100%",
                            }}
                            name={!this.state.boxView ? "th-large" : "list"}/>

                    </TouchableOpacity>

                    {/* vertical line view */}
                    <View
                        style={{
                            width: 1,
                            height: "100%",
                            backgroundColor: "#a4a4a4",
                        }}/>

                    {/* order by view */}
                    <TouchableOpacity
                        onPress={() => {this.setState({pickerVisible: true})}}
                        style={{flexDirection: "row", flex: 5, alignItems: "center", justifyContent: "center"}}>

                        {/* order icon */}
                        <FAIcon
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp("3%"),
                            }}
                            name={"sort"}/>

                        {/* order text */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "500",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "center",
                                color: mowColors.textColor,
                                marginLeft: 5,
                                fontFamily: fontFamily.light
                            }}>

                            {mowStrings.products.orderBy}

                        </Text>

                    </TouchableOpacity>

                    {/* vertical line view */}
                    <View
                        style={{
                            width: 1,
                            height: "90%",
                            backgroundColor: "#a4a4a4",
                        }}/>

                    {/* filter view */}
                    <TouchableOpacity
                        style={{flexDirection: "row", flex: 5, alignItems: "center", justifyContent: "center"}}
                        onPress={() => {navigate("Filter")}}>

                        {/* order icon */}
                        <FAIcon
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp("3%"),
                            }}
                            name={"filter"}/>

                        {/* order text */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "500",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "center",
                                color: mowColors.textColor,
                                marginLeft: 5,
                                fontFamily: fontFamily.light
                            }}>

                            {mowStrings.products.filter}

                        </Text>

                    </TouchableOpacity>

                </View>

                {/* product list */}
                <MowProductList
                    boxView={boxView}
                    key={productListKey}
                    listData={productList}
                    listStyle={{paddingHorizontal: wp("1%")}}/>

                <MowPicker
                    key={2}
                    selectedValue={this.state.pickerSelectedId}
                    onSelect={(obj) => {this._onSelect(obj)}}
                    pickerVisible={this.state.pickerVisible}
                    pickerOnClose={() => this.setState({pickerVisible: false})}
                    data={pickerSortData}/>

            </MowContainer>

        )

    }

}
