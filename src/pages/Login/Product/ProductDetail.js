import React from "react";
import {View, Text, Image, ScrollView, TouchableOpacity, FlatList} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {borderStyle, categoryStyleWithoutShadow} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import Swiper from "react-native-swiper";
import {MowStarView} from "../../../components/ui/AppSpecifics/StarView/MowStarView";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import {navigate} from "../../../RootMethods/RootNavigation";
import BodySize from "../../../SampleData/Product/BodySize";
import Colors from "../../../SampleData/Colors";
import CustomerComments from "../../../SampleData/CustomerComments";
import ShopListData from "../../../SampleData/Shop/ShopListData";

export default class ProductDetail extends React.Component {

    state = {
        product: this.props.route.params["product"],
        colorArr: [],
        colorListKey: 0,
        sizeArr: [],
        sizeListKey: 0,
        showMore: false,
        commentListKey: 0
    };

    _handleSizeSelection(index) {
        let sizeArr = this.state.sizeArr;

        let length = BodySize.length;

        for (let i = 0; i < length; i++) {
            if (i != index) {
                // to set false all array values except selected index
                sizeArr[i] = false;
            }
        }

        // to update selected item as its opposite
        sizeArr[index] = !sizeArr[index];

        this.setState({sizeArr: sizeArr, sizeListKey: this.state.sizeListKey + 1})
    }

    _handleColorSelection(index) {
        let colorArr = this.state.colorArr;

        let length = Colors.length;

        for (let i = 0; i < length; i++) {
            if (i != index) {
                // to set false all array values except selected index
                colorArr[i] = false;
            }
        }

        // to update selected item as its opposite
        colorArr[index] = !colorArr[index];

        this.setState({colorArr: colorArr, colorListKey: this.state.colorListKey + 1})
    }

    _commentRow(item) {

        return(

            <View
                style={{
                    marginVertical: 5,
                    borderRadius: 5,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "rgba(146, 146, 146, 0.41)",
                    padding: 5
                }}>

                <View
                    style={{flexDirection: "row"}}>

                    {/* image view */}
                    <Image
                        style={{
                            width: hp("5%"),
                            height: hp("5%"),
                        }}
                        resizeMode={"contain"}
                        source={item["image"]}/>

                    <View
                        style={{marginLeft: 10}}>

                        {/* name text */}
                        <Text
                            style={{
                                fontSize: hp("1.8%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor
                            }}>

                            {item["name"]}

                        </Text>

                        {/* date text */}
                        <Text
                            style={{
                                fontSize: hp("1.4%"),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.textColor
                            }}>

                            {item["date"]}

                        </Text>

                    </View>

                    <View
                        style={{marginLeft: 10, justifyContent: "center"}}>

                        {/* star view */}
                        <MowStarView
                            score={item["score"]}/>

                    </View>

                </View>

                {/* description text */}
                <Text
                    style={{
                        marginTop: 5,
                        fontSize: hp("1.5%"),
                        fontWeight: "300",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.textColor
                    }}>

                    {item["description"]}

                </Text>

            </View>

        )

    }

    _renderIcon(name) {
        return(

            <TouchableOpacity
                style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: mowColors.viewBGColor,
                    borderRadius: 100,
                    padding: 5,
                    marginVertical: 5
                }}>

                {/* like icon */}
                <FAIcon
                    name={name}
                    style={{
                        color: mowColors.mainColor,
                        fontSize: hp("2.5%")
                    }}/>

            </TouchableOpacity>

        )
    }

    render() {

        const product = this.state.product;

        return(

            <MowContainer
                style={{backgroundColor: mowColors.pageBGDarkColor}}
                title={mowStrings.productDetail.title}>

                <ScrollView>

                    {/* product info view */}
                    <View
                        style={[categoryStyleWithoutShadow, {backgroundColor: mowColors.categoryBGColor}]}>

                        {/* image swiper view */}
                        <View
                            style={{height: hp("42%")}}>

                            {/* product image swiper */}
                            <Swiper
                                removeClippedSubviews={false}
                                ref='swiper'
                                pagingEnabled={true}
                                showsPagination={true}
                                horizontal={true}
                                loop={false}
                                dotColor={"grey"}
                                activeDotColor={mowColors.mainColor}
                                paginationStyle={{bottom: hp("1%")}}
                                autoplay={false}>

                                {
                                    product["images"].map((item, index) => {

                                        return (

                                            <Image
                                                key={index}
                                                style={{
                                                    height: hp("38%"),
                                                    width: "100%",
                                                }}
                                                resizeMode={"contain"}
                                                source={item["image"]}/>

                                        )

                                    })
                                }

                            </Swiper>

                            {/* like report share view */}
                            <View
                                style={{
                                    position: "absolute",
                                    right: 0,
                                    top: 0
                                }}>

                                {/* like button */}
                                {this._renderIcon("heart", mowStrings.productDetail.like)}

                                {/* report button */}
                                {this._renderIcon("info-circle", mowStrings.productDetail.report)}

                                {/* share button */}
                                {this._renderIcon("share", mowStrings.productDetail.share)}

                            </View>

                        </View>

                        {/* go to dealer shop screen button */}
                        <TouchableOpacity
                            onPress={() => navigate("ShopDetail", {shop: ShopListData[0]})}
                            style={{
                                flexDirection: "row",
                                alignItems: "center"
                            }}>

                            {/* dealer text */}
                            <Text
                                style={{
                                    color: mowColors.titleTextColor,
                                    fontSize: hp(1.7),
                                    fontWeight: "600"
                                }}>

                                {product["shop"]}

                            </Text>

                            <FAIcon
                                name={"angle-right"}
                                style={{
                                    marginLeft: 5,
                                    color: mowColors.textColor,
                                    fontSize: hp(2.5)
                                }}/>

                        </TouchableOpacity>

                        {/* product name text */}
                        <Text
                            style={{
                                marginVertical: 10,
                                width: "100%",
                                textAlign: "left",
                                fontSize: hp("2%"),
                                fontWeight: "600",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                color: mowColors.titleTextColor

                            }}>

                            {product["title"]}

                        </Text>

                    </View>

                    {/* content view */}
                    <View>

                        {/* body size view */}
                        <View
                            style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                            {/* body size text */}
                            <Text
                                style={{
                                    marginBottom: 10,
                                    fontSize: hp("1.8%"),
                                    fontWeight: "600",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: mowColors.titleTextColor
                                }}>

                                {mowStrings.productDetail.bodySize}

                            </Text>

                            {/* body size list view */}
                            <FlatList
                                key={this.state.sizeListKey}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                                data={BodySize}
                                renderItem={({ item, index }) => (

                                    <TouchableOpacity
                                        onPress={() => {this._handleSizeSelection(index)}}
                                        style={{
                                            width: hp("4%"),
                                            height: hp("4%"),
                                            borderRadius: 3,
                                            marginLeft: index != 0 ? 10 : 0,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            backgroundColor: this.state.sizeArr[index] ? mowColors.mainColor : "#ffffff",
                                            ...borderStyle,
                                            marginRight: 1
                                        }}
                                        key={index}>

                                        <Text
                                            style={{
                                                color: this.state.sizeArr[index] ? "white" : mowColors.mainColor,
                                                fontSize: hp("1.5%"),
                                                fontWeight: "600",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "center",
                                            }}>

                                            {item["size"]}

                                        </Text>

                                    </TouchableOpacity>

                                )}
                            />

                        </View>

                        {/* color view */}
                        <View
                            style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                            {/* color text */}
                            <Text
                                style={{
                                    marginBottom: 10,
                                    fontSize: hp("1.8%"),
                                    fontWeight: "600",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: mowColors.titleTextColor
                                }}>

                                {mowStrings.productDetail.color}

                            </Text>

                            {/* color list */}
                            <FlatList
                                key={this.state.colorListKey}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                                data={Colors}
                                renderItem={({ item, index }) => (

                                    <TouchableOpacity
                                        onPress={() => {this._handleColorSelection(index)}}
                                        style={{
                                            width: hp("4%"),
                                            height: hp("4%"),
                                            borderRadius: 100,
                                            backgroundColor: item["color"],
                                            marginLeft: index != 0 ? 10 : 0,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            marginRight: 1
                                        }}
                                        key={index}>

                                        {
                                            this.state.colorArr[index] &&

                                            <FAIcon
                                                style={{color: "white", fontSize: hp("2.5%")}}
                                                name={"check"}/>
                                        }

                                    </TouchableOpacity>

                                )}
                            />

                        </View>

                        {/* product feature view */}
                        <View
                            style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                            <Text
                                style={{
                                    fontSize: hp("1.8%"),
                                    fontWeight: "600",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: mowColors.titleTextColor
                                }}>

                                {mowStrings.productDetail.productFeature}

                            </Text>

                            {/* product feature text */}
                            <Text
                                style={{
                                    marginTop: 2,
                                    fontSize: hp("1.6%"),
                                    fontWeight: "300",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: mowColors.textColor
                                }}>

                                {product["productFeature"]}

                            </Text>

                        </View>

                        {/* comment list view */}
                        <View
                            style={[categoryStyleWithoutShadow, {marginTop: 15, backgroundColor: mowColors.categoryBGColor}]}>

                            {/* title view */}
                            <View
                                style={{
                                    justifyContent: "space-between",
                                    alignItems: "center",
                                    marginBottom: 5,
                                    flexDirection: "row"
                                }}>

                                {/* title text  */}
                                <Text
                                    style={{
                                        fontSize: hp("1.7%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        color: mowColors.titleTextColor
                                    }}>

                                    {mowStrings.productDetail.customerComments} (1681)

                                </Text>

                                {/* all button vie  */}
                                <TouchableOpacity
                                    onPress={() => {}}
                                    style={{flexDirection: "row", alignItems: "center"}}>

                                    <Text
                                        style={{
                                            fontSize: hp("1.6%"),
                                            fontWeight: "600",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: mowColors.titleTextColor
                                        }}>

                                        {mowStrings.button.seeAll}

                                    </Text>

                                    <FAIcon
                                        name={"angle-right"}
                                        style={{
                                            marginLeft: 5,
                                            color: mowColors.textColor,
                                            fontSize: hp(2.2)
                                        }}/>

                                </TouchableOpacity>

                            </View>

                            {/* comment list */}
                            <FlatList
                                key={this.state.commentListKey}
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(item, index) => index.toString()}
                                data={CustomerComments}
                                renderItem={({ item, index }) => (

                                    <View
                                        key={index}>

                                        {this._commentRow(item, index)}

                                    </View>

                                )}
                            />

                        </View>

                    </View>

                </ScrollView>

                {/* discount rate & price & add to cart button */}
                <View
                    style={[categoryStyleWithoutShadow, {flexDirection: "row", alignItems: "center", marginTop: 10, backgroundColor: mowColors.categoryBGColor}]}>

                    {
                        product["discountRate"]

                            ?

                            //price & discount view
                            <View
                                style={{flexDirection: "row", alignItems: "center", flex: 1}}>

                                {/* discount rate view */}
                                <View
                                    style={{
                                        backgroundColor: mowColors.mainColor,
                                        borderRadius: 5,
                                        justifyContent: "center",
                                        alignItems: "center",
                                        width: hp("4.5%"),
                                        height: hp("4.5%")
                                    }}>

                                    <Text
                                        style={{
                                            fontSize: hp("1.8%"),
                                            fontWeight: "bold",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "left",
                                            color: "#ffffff"
                                        }}>

                                        {product["discountRate"]}

                                    </Text>

                                </View>

                                {/* price view */}
                                <View
                                    style={{marginLeft: 10}}>

                                    {/* first price text view  */}
                                    <View
                                        style={{alignItems: "center", justifyContent: "center"}}>

                                        {/* first price text */}
                                        <Text
                                            style={{
                                                fontSize: hp("1.6%"),
                                                fontWeight: "300",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "center",
                                                color: "#c2c2c2"
                                            }}>

                                            {product["currency"]}{product["firstPrice"]}

                                        </Text>

                                        <View
                                            style={{
                                                backgroundColor: mowColors.mainColor,
                                                width: "100%",
                                                height: hp("0.1%"),
                                                position: "absolute",
                                            }}/>

                                    </View>

                                    {/* last price text */}
                                    <Text
                                        style={{
                                            marginTop: 1,
                                            fontSize: hp("1.8%"),
                                            fontWeight: "bold",
                                            fontStyle: "normal",
                                            letterSpacing: 0,
                                            textAlign: "center",
                                            color: mowColors.mainColor
                                        }}>

                                        {product["currency"]}{product["lastPrice"]}

                                    </Text>

                                </View>

                            </View>

                            :

                            //price text
                            <Text
                                style={{
                                    flex: 1,
                                    fontSize: hp("2.2%"),
                                    fontWeight: "bold",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: "#575757"
                                }}>

                                {product["currency"]}{product["lastPrice"]}

                            </Text>
                    }


                    {/* button view */}
                    <View
                        style={{flex: 1}}>

                        <MowButton
                            buttonText={mowStrings.button.addToCart}
                            containerStyle={{margin: 0, height: hp(4), borderWidth: 1, borderColor: mowColors.mainColor}}
                            onPress={() => {navigate("Cart")}}
                            leftIconStyle={{fontSize: hp("2.2%")}}
                            textStyle={{fontSize: hp("1.6%")}}
                            stickyIcon={true}
                            leftIcon={"shopping-cart"}
                            size={"small"}
                            type={"success"}/>

                    </View>

                </View>

            </MowContainer>

        )

    }

}
