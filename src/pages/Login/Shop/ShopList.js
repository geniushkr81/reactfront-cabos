import React from "react";
import {Text, View, TouchableOpacity, FlatList, Image} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {cardStyle, fontFamily, pageContainerStyle, shadowStyle} from "../../../values/Styles/MowStyles";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {_MF} from "../../../components/global/MowFunctions/MowFunctions";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import ShopListData from "../../../SampleData/Shop/ShopListData";
import {mowColors} from "../../../values/Colors/MowColors";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {isTablet, platform} from "../../../values/Constants/MowConstants";
import {navigate} from "../../../RootMethods/RootNavigation";

export default class ShopList extends React.Component {

    state = {
        shopList: ShopListData,
        shopListFullData: ShopListData,
        listKey: 0,
        list: true,
        searchText: "",
    }

    shopListStyle = ({
        container: {
            width: "99%",
            alignSelf: "center",
            marginVertical: hp(1),
            ...cardStyle,
            alignItems: "flex-start",
            backgroundColor: mowColors.pageBGColor,
            paddingVertical: 10
        },
        banner: {
            width: "100%",
            height: isTablet ? wp(40) : wp(50.75)
        },
        logoView: {
            width: hp("8%"),
            height: hp("8%"),
            backgroundColor: "white",
            borderRadius: 100,
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            bottom: "25%",
            left: "3%",
            ...shadowStyle,
            marginBottom: 10
        },
        logo: {
            width: hp("5%"),
            height: hp("5%"),
        },
        infoView: {
            paddingLeft: wp(5),
            backgroundColor: mowColors.viewBGColor,
            width: "100%",
            marginVertical: hp(0.5),
            paddingVertical: 5
        },
        title: {
            fontSize: hp(2),
            fontWeight: "500",
            color: mowColors.titleTextColor
        },
        address: {
            fontSize: hp(1.8),
            color: mowColors.textColor,
            marginTop: hp(0.5),
        }
    });

    shopBoxStyle = ({
        container: {
            height: isTablet ? hp(27) : (platform === "android" ? hp(23) : hp(20)),
            width: isTablet ? "49%" : "48%",
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            marginHorizontal: isTablet ? wp(0.5) : wp(1),
            marginTop: hp(1),
            backgroundColor: mowColors.pageBGColor,
            ...cardStyle,
            marginBottom: 3
        },
        banner: {
            width: wp(45),
            height: wp(25.31)
        },
        logoView: {
            width: hp("5%"),
            height: hp("5%"),
            backgroundColor: "white",
            borderRadius: 100,
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            bottom: isTablet ? (platform === "android" ? "28%" : "30%") : (platform === "android" ? "35%" : "40%"),
            left: "3%",
            ...shadowStyle
        },
        logo: {
            width: hp("3.5%"),
            height: hp("3.5%")
        },
        infoView: {
            backgroundColor: mowColors.viewBGColor,
            width: "100%",
            paddingLeft: wp(2),
        },
        title: {
            fontSize: hp(1.5),
            fontWeight: "500",
            marginVertical: hp(0.5),
            color: mowColors.titleTextColor,
            fontFamily: fontFamily.bold
        },
        address: {
            fontSize: hp(1.4),
            marginVertical: hp(0.5),
            color: mowColors.textColor,
            fontFamily: fontFamily.regular
        }
    });

    // to handle searched data
    _searchData(search) {
        let fullData = this.state.shopListFullData;
        if (search == "") {
            this.setState({
                searchText: search,
                shopList: fullData,
            });
            return true;
        }
        else if (search.length >= 3) {
            let searchedData = _MF.mowSearch(fullData,"title", search);

            this.setState({
                searchText: search,
                shopList: searchedData,
            })
        }
        else {
            this.setState({
                searchText: search,
            })
        }
    }

    render() {

        let {searchText, shopList, list, listKey} = this.state;

        return(

            <MowContainer
                title={mowStrings.shopList.title}>

                <View
                    style={pageContainerStyle}>

                    <View
                        style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>

                        {/* search input */}
                        <MowInput
                            placeholder={mowStrings.shopList.search}
                            leftIcon={"search"}
                            keyboardType={"default"}
                            containerStyle={{width: "90%"}}
                            value={searchText}
                            onChangeText={value => this._searchData(value)}/>

                        <TouchableOpacity
                            onPress={() => {this.setState({list: !this.state.list, listKey: (this.state.listKey + 1)})}}>

                            <FAIcon
                                style={{fontSize: hp(4), color: mowColors.titleTextColor}}
                                name={"th-large"}/>

                        </TouchableOpacity>

                    </View>

                    {/* shop list */}
                    <FlatList
                        key={listKey}
                        numColumns={list ? 1 : 2}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={shopList}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity
                                onPress={() => {navigate("ShopDetail", {shop: item})}}
                                key={index}
                                style={[list ? this.shopListStyle.container : this.shopBoxStyle.container]}>

                                {/* shop banner image */}
                                <Image
                                    resizeMode={"contain"}
                                    style={list ? this.shopListStyle.banner : this.shopBoxStyle.banner}
                                    source={item["banner"]}/>

                                {/* shop logo view */}
                                <View
                                    style={list ? this.shopListStyle.logoView : this.shopBoxStyle.logoView}>

                                    {/* shop logo image */}
                                    <Image
                                        source={item["logo"]}
                                        resizeMode={"contain"}
                                        style={list ? this.shopListStyle.logo : this.shopBoxStyle.logo}/>

                                </View>

                                {/* footer info view */}
                                <View
                                    style={list ? this.shopListStyle.infoView : this.shopBoxStyle.infoView}>

                                    {/* shop title */}
                                    <Text
                                        numberOfLines={1}
                                        style={list ? this.shopListStyle.title : this.shopBoxStyle.title}>

                                        {item["title"]}

                                    </Text>

                                    {/* shop address */}
                                    <Text
                                        numberOfLines={1}
                                        style={list ? this.shopListStyle.address : this.shopBoxStyle.address}>

                                        {item["address"]}

                                    </Text>

                                </View>

                            </TouchableOpacity>

                        )}
                    />

                </View>

            </MowContainer>

        )

    }

}
