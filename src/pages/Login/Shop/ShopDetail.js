import React from "react";
import {Text, View, Image, TouchableOpacity, ImageBackground, FlatList} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {fontFamily, pageContainerStyle, shadowStyle} from "../../../values/Styles/MowStyles";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {deviceWidth, isTablet, pageHeight} from "../../../values/Constants/MowConstants";
import {mowColors} from "../../../values/Colors/MowColors";
import FAIcon from "react-native-vector-icons/FontAwesome";
import { Dialog, SlideAnimation } from 'react-native-popup-dialog';
import WomanClothing from "../../../SampleData/Product/WomanClothing";
import {MowProductList} from "../../../components/ui/AppSpecifics/MowProductList";

export default class ShopDetail extends React.Component {

    state = {
        title: this.props.route.params["shop"]["title"],
        shop: this.props.route.params["shop"],
        productList: WomanClothing,
        productListKey: 0,
        boxView: true,
        activeIndex: 0,
    }

    _renderDialogItem(icon, text) {
        return (
            <View>

                <TouchableOpacity
                    onPress={() => {}}
                    style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginHorizontal: 15,
                        marginVertical: hp("2%"),
                    }}>

                    <View
                        style={{
                            backgroundColor: "white",
                            borderRadius: 100,
                            height: hp("4.5%"),
                            width: hp("4.5%"),
                            justifyContent: "center",
                            alignItems: "center"
                        }}>

                        <FAIcon
                            style={{
                                color: mowColors.mainColor,
                                fontSize: hp("2.5%")
                            }}
                            name={icon} />

                    </View>

                    <Text
                        style={{
                            marginLeft: 15,
                            fontSize: hp(2.2),
                            fontWeight: "bold",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "left",
                            color: "#ffffff"
                        }}>

                        {text}

                    </Text>

                </TouchableOpacity>

                {/* line view */}
                <View
                    style={{
                        borderStyle: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "#ffffff",
                        opacity: 0.28
                    }} />

            </View>
        )
    }

    render() {

        let {title, shop, boxView, productList, productListKey} = this.state;

        return(

            <MowContainer
                title={title}>

                <View
                    style={{flex: 1}}>

                    {/* banner view */}
                    <ImageBackground
                        source={shop["banner"]}
                        resizeMode={"stretch"}
                        style={{ height: isTablet ? wp(40) : wp(56.25), width: wp(100), justifyContent: "flex-end", marginBottom: 10, ...shadowStyle}}>

                        <TouchableOpacity
                            onPress={() => { this.setState({ showMenu: true }) }}
                            style={{
                                zIndex: 2,
                                position: "absolute",
                                top: 10,
                                right: 10,
                                backgroundColor: mowColors.mainColor,
                                borderRadius: 100,
                                padding: 5,
                                width: hp("5%"),
                                height: hp("5%"),
                                justifyContent: "center",
                                alignItems: "center"
                            }}>

                            <FAIcon
                                style={{ fontSize: hp("3%"), color: "white" }}
                                name={"plus"} />

                        </TouchableOpacity>

                        {/* footer view */}
                        <View
                            style={{ flexDirection: "row", position: "absolute", bottom: 3, width: "100%" }}>

                            {/* overlay */}
                            <View
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    height: "100%",
                                    backgroundColor: "black",
                                    opacity: 0.65,
                                    position: "absolute"
                                }} />

                            {/* shop info view */}
                            <View
                                style={{ flex: 5, justifyContent: "center", marginLeft: 10, paddingVertical: 5 }}>

                                {/* title text */}
                                <Text
                                    style={{
                                        fontSize: hp("1.7%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        fontFamily: fontFamily.bold,
                                        color: mowColors.titleTextColor,
                                    }}>

                                    {shop["title"]}

                                </Text>

                                {/* address text */}
                                <Text
                                    style={{
                                        fontSize: hp("1.7%"),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "left",
                                        fontFamily: fontFamily.regular,
                                        color: mowColors.textColor,
                                    }}>

                                    {shop["address"]}

                                </Text>

                            </View>

                            <View
                                style={{
                                    ...shadowStyle,
                                    borderRadius: 100,
                                    width: hp("6%"),
                                    height: hp("6%"),
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: "#ffffff",
                                    marginHorizontal: 10,
                                    alignSelf: "center",
                                }}>

                                {/* shop logo */}
                                <Image
                                    style={{ width: hp("4%"), height: hp("4%") }}
                                    source={shop["logo"]}
                                    resizeMode={"contain"} />

                            </View>

                        </View>

                    </ImageBackground>

                    {/* product list */}
                    <MowProductList
                        boxView={boxView}
                        listData={productList}
                        listStyle={{paddingHorizontal: wp("1%")}}/>

                    {/* shop menu */}
                    <Dialog
                        overlayBackgroundColor={"#454545"}
                        width={isTablet ? (deviceWidth * 0.5) : (deviceWidth * 0.7)}
                        height={pageHeight}
                        visible={this.state.showMenu}
                        onTouchOutside={() => { this.setState({ showMenu: false }) }}
                        dialogAnimation={new SlideAnimation({ slideFrom: 'right' })}
                        dialogStyle={{ zIndex: 99, alignSelf: "flex-end", height: pageHeight - hp(1) }}>

                        <View
                            style={{ backgroundColor: mowColors.mainColor, height: pageHeight - hp(1) }}>

                            {/* menu close button */}
                            <TouchableOpacity
                                onPress={() => { this.setState({ showMenu: false }) }}
                                style={{
                                    width: hp("3.5%"),
                                    height: hp("3.5%"),
                                    backgroundColor: "white",
                                    borderRadius: 100,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    position: "absolute",
                                    right: 5,
                                    top: 5,
                                    zIndex: 1
                                }}>

                                <FAIcon
                                    style={{
                                        color: mowColors.mainColor,
                                        fontSize: hp("2%")
                                    }}
                                    name={"times"} />

                            </TouchableOpacity>

                            {/* header view */}
                            <View
                                style={{margin: 10, alignItems: "center"}}>

                                <View
                                    style={{
                                        width: hp("5.5%"),
                                        height: hp("5.5%"),
                                        backgroundColor: "white",
                                        borderRadius: 100,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}>

                                    <Image
                                        source={shop["logo"]}
                                        resizeMode={"contain"}
                                        style={{ width: hp("3.5%"), height: hp("3.5%") }} />

                                </View>

                                {/* shop title */}
                                <Text
                                    style={{
                                        marginTop: hp(3),
                                        fontSize: hp(2),
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff",
                                        width: "100%",
                                    }}>

                                    {shop["title"]}

                                </Text>

                            </View>

                            <View
                                style={{ marginTop: 10 }}>

                                {/* call shop view */}
                                {this._renderDialogItem("phone", mowStrings.shopDetail.callShop)}

                                {/* navigate view view */}
                                {this._renderDialogItem("location-arrow", mowStrings.shopDetail.navigateShop)}

                                {/* call shop view */}
                                {this._renderDialogItem("share-alt", mowStrings.shopDetail.shareShop)}

                            </View>

                            <Image
                                style={{
                                    width: "80%",
                                    height: "5%",
                                    position: "absolute",
                                    bottom: "4%",
                                    alignSelf: "center"
                                }}
                                resizeMode={"contain"}
                                source={require("../../../assets/logo/logo_white_horizontal.png")} />

                        </View>


                    </Dialog>

                </View>

            </MowContainer>

        )

    }

}
