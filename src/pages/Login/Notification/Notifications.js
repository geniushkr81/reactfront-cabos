import React from "react";
import {Text, View, FlatList, TouchableOpacity} from "react-native";
import {mowStrings} from "../../../values/Strings/MowStrings";
import FeatherIcon from "react-native-vector-icons/Feather";
import {borderStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowCheckBox} from "../../../components/ui/Common/CheckBox/MowCheckBox";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";
import NotificationData from "../../../SampleData/User/NotificationData";
import {isTablet} from "../../../values/Constants/MowConstants";

export default class Settings extends React.Component {

    state = {
        selectedArr: [],
        notificationListKey: 0
    };

    _handleNotificationRead(index) {

        let selectedArr = this.state.selectedArr;

        // to update selected item as its opposite
        selectedArr[index] = !selectedArr[index];

        this.setState({selectedArr: selectedArr, notificationListKey: this.state.notificationListKey + 1});

    }

    _setAsAllRead() {

        let selectedArr = this.state.selectedArr;

        let length = NotificationData.length;

        for (let i = 0; i < length; i++) {
            selectedArr[i] = true;
        }

        this.setState({selectedArr: selectedArr, notificationListKey: this.state.notificationListKey + 1});

    }

    _renderNotification(item, index) {

        let {selectedArr} = this.state;

        return(

            <TouchableOpacity
                onPress={() => {this._handleNotificationRead(index)}}
                key={index}
                style={{
                    flexDirection: "row",
                    backgroundColor: mowColors.viewBGColor,
                    ...borderStyle,
                    alignItems: "center",
                    paddingVertical: 10,
                    marginVertical: 5
                }}>

                <View
                    style={{width: isTablet ? "12%" : "20%"}}>

                    <View
                        style={{
                            width: hp(5),
                            height: hp(5),
                            borderColor: mowColors.mainColor,
                            borderWidth: 1,
                            borderRadius: 100,
                            justifyContent: "center",
                            alignItems: "center",
                            marginHorizontal: 20,
                        }}>

                        <FeatherIcon
                            style={{color: mowColors.mainColor, fontSize: hp(2.5)}}
                            name={"bell"}/>

                    </View>

                </View>

                <View
                    style={{width: isTablet ? "90%" : "80%", flexDirection: "row"}}>

                    <View
                        style={{width: "85%"}}>

                        {/* title text */}
                        <Text
                            style={{
                                color: mowColors.titleTextColor,
                                fontSize: hp(1.6),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                            }}>

                            {item["title"]}

                        </Text>

                        {/* message text */}
                        <Text
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp(1.5),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                            }}>

                            {item["message"]}

                        </Text>

                        {/* date text */}
                        <Text
                            style={{
                                color: mowColors.titleTextColor,
                                fontSize: hp(1.4),
                                fontWeight: "normal",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                marginTop: 8
                            }}>

                            {item["date"]}

                        </Text>

                    </View>

                    <View
                        style={{width: isTablet ? "10%" : "15%", alignItems: "center", justifyContent: "center"}}>

                        {/* checkbox view */}
                        <MowCheckBox
                            onPress={() => {this._handleNotificationRead(index)}}
                            checkedIcon={"dot-circle-o"}
                            unCheckedIcon={"circle-o"}
                            checkedColor={mowColors.mainColor}
                            checked={selectedArr[index]}
                            containerStyle={{alignSelf: "center", marginLeft: 10}}/>

                    </View>

                </View>

            </TouchableOpacity>

        )
    }

    render() {

        let {notificationListKey} = this.state;

        return(

            <MowContainer
                title={mowStrings.notifications.title}>

                <View
                    style={{flex: 1}}>

                    <FlatList
                        key={notificationListKey}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={NotificationData}
                        renderItem={({item, index}) => this._renderNotification(item, index)}/>

                </View>

                <View
                    style={{marginHorizontal: wp(3)}}>

                    <MowButton
                        buttonText={mowStrings.notifications.markAll}
                        type={"success"}
                        onPress={() => {this._setAsAllRead()}}/>

                </View>

            </MowContainer>

        )

    }

}
