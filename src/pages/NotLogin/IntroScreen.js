import React from "react";
import {View, Image, Text} from "react-native";
import Swiper from 'react-native-swiper'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowButton} from "../../components/ui/Common/Button/MowButton";
import {navigate} from "../../RootMethods/RootNavigation";
import {mowStrings} from "../../values/Strings/MowStrings";
import {MowContainer} from "../../components/ui/Core/Container/MowContainer";
import {isTablet, statusBarHeight} from "../../values/Constants/MowConstants";
import {mowColors} from "../../values/Colors/MowColors";

export default class IntroScreen extends React.Component {

    state = {
        step: 1,
    };

    dotStyle = {
        paginationView: {
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
        },
        activeDot: {
            height: hp("0.5%"),
            width: wp("5%"),
            borderRadius: 20,
            backgroundColor: mowColors.textColor,
            marginHorizontal: 5
        },
        activeDotWhite: {
            height: hp(0.5),
            width: wp(4),
            borderRadius: 20,
            backgroundColor: "white",
            marginHorizontal: 5
        },
        passiveDot: {
            opacity: 1,
            height: hp(0.5),
            width: wp(1),
            borderRadius: 100,
            backgroundColor: mowColors.textColor,
            marginHorizontal: 5
        },
    }

    _skipButtonClick() {
        navigate("Home")
    }

    _tutorialPage(title, text, buttonTitle, index, imagePath, bgColor) {
        return(

            <View
                style={{
                    flex: 1,
                    paddingBottom: hp(3),
                    paddingTop: statusBarHeight,
                    backgroundColor: bgColor
                }}>

                <View
                    style={{justifyContent: "center"}}>

                    <Text
                        style={{
                            // position: "absolute",
                            left: 10,
                            fontSize: hp(5),
                            fontWeight: "bold",
                            opacity: 0.57,
                            color: mowColors.titleTextColor,
                            zIndex: 2,
                        }}>

                        0{index + 1}

                    </Text>

                </View>

                {/* image view */}
                <View
                    style={{
                        width: "100%",
                        height: hp(40),
                        marginTop: hp(3),
                        borderRadius: 10,
                    }}>

                    <Image
                        source={imagePath}
                        resizeMode={"contain"}
                        style={{
                            width: "100%",
                            height: hp(40),
                            borderRadius: 10
                        }}/>

                </View>

                <View>

                    <View
                        style={{height: "45%", marginTop: hp(5), paddingHorizontal: 20}}>

                        {/* title */}
                        <Text
                            style={{
                                fontSize: hp(2.5),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "center",
                                color: mowColors.titleTextColor,
                                marginVertical: 20
                            }}>

                            {title}

                        </Text>

                        {/* description */}
                        <Text
                            style={{
                                fontSize: hp(2),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                lineHeight: isTablet ? 35 : 20,
                                letterSpacing: 0,
                                textAlign: "center",
                                color: "white",
                                width: "80%",
                                alignSelf: "center"
                            }}>

                            {text}

                        </Text>

                    </View>

                    {/* button view */}
                    <MowButton
                        buttonText={buttonTitle}
                        size={"big"}
                        shadow={true}
                        onPress={() => this._skipButtonClick()}
                        textStyle={{color: bgColor}}
                        containerStyle={{width: "70%"}}/>

                    {/* pagination & skip button view */}
                    <View
                        style={{
                            alignItems: "center",
                            justifyContent: "center",
                            marginTop: hp(3)
                        }}>

                        {/* pagination*/}
                        {
                            index === 0 &&

                            <View
                                style={this.dotStyle.paginationView}>

                                <View
                                    style={this.dotStyle.activeDot}/>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                            </View>
                        }

                        {
                            index === 1 &&

                            <View
                                style={this.dotStyle.paginationView}>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                                <View
                                    style={this.dotStyle.activeDot}/>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                            </View>
                        }

                        {
                            index === 2 &&

                            <View
                                style={this.dotStyle.paginationView}>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                                <View
                                    style={this.dotStyle.passiveDot}/>

                                <View
                                    style={this.dotStyle.activeDot}/>

                            </View>
                        }

                    </View>

                </View>

            </View>

        )
    }

    render() {

        return (

            <MowContainer
                footer={false}
                navbar={false}
                topBorder={false}
                statusBar={false}>

                <Swiper
                    loop={false}
                    showsPagination={false}
                    style={{}}>

                    {/* intro page 1 */}
                    {this._tutorialPage(mowStrings.introScreens.page1Title, mowStrings.introScreens.page1Content, mowStrings.button.skip, 0, introImagePaths.page1, mowColors.intro1)}

                    {/* intro page 2 */}
                    {this._tutorialPage(mowStrings.introScreens.page2Title, mowStrings.introScreens.page2Content, mowStrings.button.skip, 1, introImagePaths.page2, mowColors.intro2)}

                    {/* intro page 3 */}
                    {this._tutorialPage(mowStrings.introScreens.page3Title, mowStrings.introScreens.page3Content, mowStrings.button.letsStart, 2, introImagePaths.page3, mowColors.intro3)}

                </Swiper>

            </MowContainer>

        );
    }
}

const introImagePaths = {
    page1: require("../../assets/image/intro/intro_1.png"),
    page2: require("../../assets/image/intro/intro_2.png"),
    page3: require("../../assets/image/intro/intro_3.png"),
    header: require("../../assets/logo/logo_horizontal.png")
}
