import React from "react";
import {Image, Text, View} from "react-native";
import {MowContainer} from "../../components/ui/Core/Container/MowContainer";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {pageContainerStyle} from "../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {mowColors} from "../../values/Colors/MowColors";
import {mowStrings} from "../../values/Strings/MowStrings";
import {MowInput} from "../../components/ui/Common/Input/MowInput";
import {LoginContext} from "../../contexts/LoginContext";
import {User} from "../../components/global/user/User";
import {MowForwardBack} from "../../components/ui/Core/NavBar/MowForwardBack";
import {goBack, navigate} from "../../RootMethods/RootNavigation";
import {MowButton} from "../../components/ui/Common/Button/MowButton";

export default class NormalLogin extends React.Component {

    static contextType = LoginContext;

    state = {
        email: "",
        password: ""
    }

    // to store entered regular from user
    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    _handleLogin() {
        // to update user login situation
        new User().setLogin(true);
        // to let know the navigator user has login
        this.context.setLogin(true);
    }

    render() {

        let {email, password} = this.state;

        return (

            <MowContainer
                footer={false}
                hideStatusBar={true}
                navbar={false}
                style={{backgroundColor: mowColors.mainColor}}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={pageContainerStyle}>

                    {/* top navigation button area */}
                    <MowForwardBack
                        text={mowStrings.login}
                        leftOnPress={() => goBack()}
                        left={true}/>

                    <View
                        style={{...pageContainerStyle, marginTop: hp("5%")}}>

                        {/* username view */}
                        <View
                            style={inputStyle.container}>

                            <Text
                                style={inputStyle.titleText}>

                                {mowStrings.loginPage.username}

                            </Text>

                            <MowInput
                                value={email}
                                iconColor={"white"}
                                rightIcon={"check"}
                                containerStyle={inputStyle.inputContainer}
                                textInputStyle={inputStyle.inputText}
                                keyboardType={"email-address"}
                                onChangeText={value => this.onChangeText("email", value)}/>

                        </View>

                        {/* password view */}
                        <View
                            style={inputStyle.container}>

                            {/* title regular */}
                            <Text
                                style={inputStyle.titleText}>

                                {mowStrings.loginPage.password}

                            </Text>

                            <MowInput
                                value={this.state.password}
                                containerStyle={inputStyle.inputContainer}
                                textInputStyle={inputStyle.inputText}
                                onChangeText={value => this.onChangeText("password", value)}
                                secureTextEntry={true}
                                iconColor={"white"}
                                rightIcon={"eye"}/>

                        </View>

                        <MowButton
                            buttonText={mowStrings.login}
                            onPress={() => {this._handleLogin()}}
                            style={{marginTop: hp("3%")}}
                            containerStyle={{marginTop: hp("5%")}}
                            textStyle={{color: mowColors.mainColor, fontWeight: "normal", letterSpacing: 0}}
                            type={"default"}/>

                        {/* forgot password view */}
                        <View
                            style={{marginTop: hp(3)}}>

                            <Text
                                style={{
                                    color: "white",
                                    fontSize: hp(1.8),
                                    textAlign: "center"
                                }}>

                                {mowStrings.loginPage.cantAccessAccount}

                            </Text>

                            {/* forgot password button */}
                            <MowButton
                                buttonText={mowStrings.loginPage.forgotPassword}
                                onPress={() => {navigate("ForgotPassword")}}
                                containerStyle={{borderWidth: 0}}
                                filled={false}
                                type={"default"}/>

                        </View>

                    </View>

                </KeyboardAwareScrollView>

                {/* logo */}
                <Image
                    resizeMode={"contain"}
                    style={{
                        marginBottom: hp("5%"),
                        alignSelf: "center",
                        width: wp("60%"),
                        height: hp("4%")
                    }}
                    source={require("../../assets/logo/logo_white_horizontal.png")}/>

            </MowContainer>
        )
    }

}

export const inputStyle = ({
    container: {
        marginVertical: 10
    },
    titleText: {
        fontSize: hp("2%"),
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff",
        opacity: 0.8
    },
    inputContainer: {
        backgroundColor: "transparent",
        orderStyle: "solid",
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: "#ffffff",
        width: "100%"
    },
    inputText: {
        fontSize: hp("2.2%"),
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff",
        width: "85%"
    },
});
