import React from "react";
import {Image, Text, View} from "react-native";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {pageContainerStyle} from "../../../values/Styles/MowStyles";
import {mowColors} from "../../../values/Colors/MowColors";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowForwardBack} from "../../../components/ui/Core/NavBar/MowForwardBack";
import {goBack, navigate} from "../../../RootMethods/RootNavigation";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";

let iconColor = "white";

export default class NormalRegister extends React.Component {

    state = {
        name: "",
        email: "",
        password: ""
    }

    // to store entered regular from user
    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    render() {

        return (

            <MowContainer
                footer={false}
                hideStatusBar={true}
                navbar={false}
                style={{backgroundColor: mowColors.mainColor}}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={pageContainerStyle}>

                    {/* top navigation button area */}
                    <MowForwardBack
                        text={mowStrings.signUp}
                        leftOnPress={() => goBack()}
                        left={true}/>

                    <View
                        style={{...pageContainerStyle, marginTop: hp("5%")}}>

                        {/* name view */}
                        <View
                            style={inputStyle.container}>

                            <Text
                                style={inputStyle.titleText}>

                                {mowStrings.signUpPage.name}

                            </Text>

                            <MowInput
                                value={this.state.name}
                                iconColor={iconColor}
                                rightIcon={"check"}
                                containerStyle={inputStyle.inputContainer}
                                textInputStyle={inputStyle.inputText}
                                onChangeText={value => this.onChangeText("name", value)}/>

                        </View>

                        {/* username view */}
                        <View
                            style={inputStyle.container}>

                            <Text
                                style={inputStyle.titleText}>

                                {mowStrings.signUpPage.username}

                            </Text>

                            <MowInput
                                value={this.state.username}
                                iconColor={iconColor}
                                rightIcon={"check"}
                                containerStyle={inputStyle.inputContainer}
                                textInputStyle={inputStyle.inputText}
                                keyboardType={"email-address"}
                                onChangeText={value => this.onChangeText("username", value)}/>

                        </View>

                        {/* password view */}
                        <View
                            style={inputStyle.container}>

                            {/* title regular */}
                            <Text
                                style={inputStyle.titleText}>

                                {mowStrings.signUpPage.password}

                            </Text>

                            <MowInput
                                value={this.state.password}
                                containerStyle={inputStyle.inputContainer}
                                textInputStyle={inputStyle.inputText}
                                onChangeText={value => this.onChangeText("password", value)}
                                secureTextEntry={true}
                                iconColor={"white"}
                                rightIcon={"eye"}/>

                        </View>

                    </View>

                    <MowButton
                        buttonText={mowStrings.button.createAnAccount}
                        onPress={() => {navigate("Verification")}}
                        style={{marginTop: hp("5%")}}
                        containerStyle={{marginTop: hp("5%")}}
                        textStyle={{color: mowColors.mainColor, fontWeight: "normal", letterSpacing: 0}}
                        type={"default"}/>


                </KeyboardAwareScrollView>

                {/* logo */}
                <Image
                    resizeMode={"contain"}
                    style={{
                        marginBottom: hp("5%"),
                        alignSelf: "center",
                        width: wp("60%"),
                        height: hp("4%")
                    }}
                    source={require("../../../assets/logo/logo_white_horizontal.png")}/>

            </MowContainer>
        )
    }

}

export const inputStyle = ({
    container: {
        marginVertical: 10
    },
    titleText: {
        fontSize: hp("2%"),
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff",
        opacity: 0.8
    },
    inputContainer: {
        backgroundColor: "transparent",
        orderStyle: "solid",
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: "#ffffff",
        width: "100%"
    },
    inputText: {
        fontSize: hp("2%"),
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff",
        width: "85%"
    },
});
