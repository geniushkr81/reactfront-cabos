import React from "react";
import {View, Image, Text} from "react-native";
import {mowColors} from "../../../values/Colors/MowColors";
import {MowContainer} from "../../../components/ui/Core/Container/MowContainer";
import {pageContainerStyle} from "../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {MowInput} from "../../../components/ui/Common/Input/MowInput";
import {User} from "../../../components/global/user/User";
import {LoginContext} from "../../../contexts/LoginContext";
import {MowButton} from "../../../components/ui/Common/Button/MowButton";

export default class Verification extends React.Component {

    static contextType = LoginContext;

    state = {
        step: 1,
        securityCode1: "",
        securityCode2: "",
        securityCode3: "",
        securityCode4: "",
        securityCode5: "",
    };

    inputStyle = {
        container: {
            borderBottomWidth: 0,
            flex: 1,
            backgroundColor: "white",
            marginHorizontal: 10,
            marginTop: 40,
            justifyContent: "center"
        },
        text: {
            textAlign: "center",
            alignSelf: "center",
            color: mowColors.mainColor
        }
    }

    _handleLogin() {
        // to update user login situation
        new User().setLogin(true);
        // to let know the navigator user has login
        this.context.setLogin(true);
    }

    // to move forward to the next input when user touched the next
    _nextInput(refName) {
        this.refs[refName].textInputRef.focus();
    }

    render() {

        return(

            <MowContainer
                footer={false}
                navbar={false}
                style={{backgroundColor: mowColors.mainColor}}>

                <View
                    style={[pageContainerStyle]}>

                    {/* check icon */}
                    <Image
                        style={{
                            alignSelf: "center",
                            marginTop: hp("5%"),
                            height: hp("10%"),
                            width: hp("10%"),
                        }}
                        resizeMode={"contain"}
                        source={require("../../../assets/icon/ic_check.png")}/>

                    {
                        this.state.step === 1 &&

                            <View>

                                {/* almost done text */}
                                <Text
                                    style={{
                                        marginTop: hp("3%"),
                                        fontSize: hp("2.5%"),
                                        fontWeight: "bold",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}>

                                    {mowStrings.signUpPage.almostDone}

                                </Text>

                                {/* security code text */}
                                <Text
                                    style={{
                                        marginTop: hp("2%"),
                                        fontSize: hp("1.8%"),
                                        fontWeight: "normal",
                                        fontStyle: "normal",
                                        lineHeight: 28,
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}>

                                    {mowStrings.signUpPage.securityCodeMessage}

                                </Text>

                                {/* verification code info text */}
                                <Text
                                    style={{
                                        marginTop: hp("5%"),
                                        fontSize: hp("1.8%"),
                                        fontWeight: "normal",
                                        fontStyle: "normal",
                                        lineHeight: 38,
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}>

                                    {mowStrings.signUpPage.verificationCodeMessage}

                                </Text>

                                {/* code input view */}
                                <View
                                    style={{flexDirection: "row"}}>

                                    <MowInput
                                        maxLength={1}
                                        value={this.state.securityCode1}
                                        onChangeText={value => this.setState({securityCode1: value})}
                                        keyboardType={"number-pad"}
                                        textInputStyle={this.inputStyle.text}
                                        containerStyle={this.inputStyle.container}/>

                                    <MowInput
                                        value={this.state.securityCode2}
                                        onChangeText={value => this.setState({securityCode2: value})}
                                        maxLength={1}
                                        keyboardType={"number-pad"}
                                        textInputStyle={this.inputStyle.text}
                                        containerStyle={this.inputStyle.container}/>

                                    <MowInput
                                        maxLength={1}
                                        value={this.state.securityCode3}
                                        onChangeText={value => this.setState({securityCode3: value})}
                                        keyboardType={"number-pad"}
                                        textInputStyle={this.inputStyle.text}
                                        containerStyle={this.inputStyle.container}/>

                                    <MowInput
                                        value={this.state.securityCode4}
                                        onChangeText={value => this.setState({securityCode4: value})}
                                        maxLength={1}
                                        keyboardType={"number-pad"}
                                        textInputStyle={this.inputStyle.text}
                                        containerStyle={this.inputStyle.container}/>

                                    <MowInput
                                        value={this.state.securityCode5}
                                        onChangeText={value => this.setState({securityCode5: value})}
                                        maxLength={1}
                                        keyboardType={"number-pad"}
                                        textInputStyle={this.inputStyle.text}
                                        containerStyle={this.inputStyle.container}/>

                                </View>

                                {/* no verification code text */}
                                <Text
                                    style={{
                                        marginTop: hp("3%"),
                                        fontSize: hp("1.8%"),
                                        fontWeight: "normal",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}>

                                    {mowStrings.signUpPage.codeError}

                                </Text>

                                {/* re-send button */}
                                <MowButton
                                    buttonText={mowStrings.button.sendAgain}
                                    type={"success"}/>

                                {/* approve button */}
                                <MowButton
                                    buttonText={mowStrings.button.approve}
                                    onPress={() => {this.setState({step: 2})}}
                                    containerStyle={{marginTop: hp("5%")}}
                                    textStyle={{color: mowColors.mainColor}}
                                    type={"default"}/>

                            </View>
                    }

                    {
                        this.state.step === 2 &&

                            <View>

                                <Text
                                    style={{
                                        marginVertical: hp("5%"),
                                        fontSize: hp("2.2%"),
                                        fontWeight: "normal",
                                        fontStyle: "normal",
                                        lineHeight: 38,
                                        letterSpacing: 0,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}>

                                    {mowStrings.signUpPage.congratsMessage}

                                </Text>

                                <MowButton
                                    buttonText={mowStrings.button.continue}
                                    onPress={() => {this._handleLogin()}}
                                    textStyle={{color: mowColors.mainColor}}
                                    type={"default"}/>

                            </View>
                    }

                </View>

                {/* logo */}
                <Image
                    resizeMode={"contain"}
                    style={{
                        marginBottom: hp("5%"),
                        alignSelf: "center",
                        width: wp("60%"),
                        height: hp("4%")
                    }}
                    source={require("../../../assets/logo/logo_white_horizontal.png")}/>

            </MowContainer>

        )

    }

}
