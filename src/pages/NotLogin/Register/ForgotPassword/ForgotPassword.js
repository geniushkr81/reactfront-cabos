import React from "react";
import {Image, Text, View} from "react-native";
import {pageContainerStyle} from "../../../../values/Styles/MowStyles";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {mowColors} from "../../../../values/Colors/MowColors";
import {MowContainer} from "../../../../components/ui/Core/Container/MowContainer";
import {mowStrings} from "../../../../values/Strings/MowStrings";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MowInput} from "../../../../components/ui/Common/Input/MowInput";
import {MowForwardBack} from "../../../../components/ui/Core/NavBar/MowForwardBack";
import {goBack} from "../../../../RootMethods/RootNavigation";
import {MowButton} from "../../../../components/ui/Common/Button/MowButton";

export default class ForgotPassword extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
        }
    }

    // to store entered regular from user
    onChangeText = (key, value) => {
        this.setState({
            [key]: value,
        })
    };

    render() {

        return (

            <MowContainer
                footer={false}
                hideStatusBar={true}
                navbar={false}
                style={{backgroundColor: mowColors.mainColor}}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    style={pageContainerStyle}>

                    {/* top navigation button area */}
                    <MowForwardBack
                        text={mowStrings.forgotPasswordScreen.title}
                        leftOnPress={() => goBack()}
                        left={true}/>

                    <View
                        style={{...pageContainerStyle, marginTop: hp("5%")}}>

                        {/* email view */}
                        <View
                            style={{...pageContainerStyle, marginVertical: 10}}>

                            {/* email title text */}
                            <Text
                                style={{
                                    fontSize: hp("2%"),
                                    fontWeight: "normal",
                                    fontStyle: "normal",
                                    letterSpacing: 0,
                                    textAlign: "left",
                                    color: "#ffffff"
                                }}>

                                {mowStrings.placeholder.email}*

                            </Text>

                            {/* email input */}
                            <MowInput
                                containerStyle={{
                                    borderWidth: 0,
                                    borderBottomWidth: 1,
                                    borderBottomColor: "#ffffff",
                                }}
                                textInputStyle={{
                                    fontSize: hp("2.2%"),
                                    fontWeight: "500",
                                    color: "#ffffff",
                                }}
                                value={this.state.email}
                                keyboardType={"email-address"}
                                onChangeText={value => this.onChangeText("email", value)}/>

                        </View>

                        <MowButton
                            buttonText={mowStrings.button.submit}
                            onPress={() => this.props.navigation.navigate("ExtraSecurity")}
                            style={{marginTop: hp("3%")}}
                            containerStyle={{marginTop: hp("5%")}}
                            textStyle={{color: mowColors.mainColor, fontWeight: "normal", letterSpacing: 0}}
                            type={"default"}/>

                    </View>

                </KeyboardAwareScrollView>

                {/* logo */}
                <Image
                    resizeMode={"contain"}
                    style={{
                        marginBottom: hp("5%"),
                        alignSelf: "center",
                        width: wp("60%"),
                        height: hp("4%")
                    }}
                    source={require("../../../../assets/logo/logo_white_horizontal.png")}/>

            </MowContainer>
        )
    }

}
