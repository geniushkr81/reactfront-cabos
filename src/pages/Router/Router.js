import * as React from 'react';

// screens
import Home from "../NotLogin/Home";
import HomeScreen from "../Login/HomeScreen";
import Settings from "../Login/Settings/Settings";
import HomeFilter from "../Login/Filter/HomeFilter";
import Filter from "../Login/Filter/Filter";
import TrendCampaigns from "../Login/Campaign/TrendCampaigns";
import Categories from "../Login/Categories/Categories";
import CategoryDetail from "../Login/Categories/CategoryDetail";
import ProductList from "../Login/Product/ProductList";
import ProductDetail from "../Login/Product/ProductDetail";
import Cart from "../Login/CartOperations/Cart";
import CompleteOrder from "../Login/CartOperations/CompleteOrder";
import Notifications from "../Login/Notification/Notifications";
import PaymentInformation from "../Login/CartOperations/PaymentInformation";
import AddressList from "../Login/Address/AddressList";
import NewAddress from "../Login/Address/NewAddress";
import OrderList from "../Login/Orders/OrderList";
import OrderDetail from "../Login/Orders/OrderDetail";
import CargoTracking from "../Login/Orders/CargoTracking";
import RateProduct from "../Login/Orders/RateProduct";
import Profile from "../Login/User/Profile";
import Password from "../Login/User/Password";
import Favorites from "../Login/Favorites/Favorites";
import Feedback from "../Login/Feedback/Feedback";
import AboutUs from "../Login/AboutUs/AboutUs";
import Privacy from "../Login/Privacy/Privacy";
import ContactUs from "../Login/ContactUs/ContactUs";
import FAQ from "../Login/FAQ/FAQ";
import ReturnRequest from "../Login/Orders/ReturnRequest";
import NormalLogin from "../NotLogin/NormalLogin";
import NormalRegister from "../NotLogin/Register/NormalRegister";
import Verification from "../NotLogin/Register/Verification";
import ForgotPassword from "../NotLogin/Register/ForgotPassword/ForgotPassword";
import ExtraSecurity from "../NotLogin/Register/ForgotPassword/ExtraSecurity";
import ChangePassword from "../NotLogin/Register/ForgotPassword/ChangePassword";

import { navigationRef, isMountedRef } from '../../RootMethods/RootNavigation';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginContext} from '../../contexts/LoginContext';
import {LoadingContext} from '../../contexts/LoadingContext';
import {DialogContext} from '../../contexts/DialogContext';
import {ToastContext} from "../../contexts/ToastContext";
import {deviceWidth, isTablet} from "../../values/Constants/MowConstants";
import {MowDrawerMenu} from "../../components/ui/Core/DrawerMenu/MowDrawerMenu";
import MyAccount from "../Login/User/MyAccount";
import AccountInformation from "../Login/User/AccountInformation";
import ShopList from "../Login/Shop/ShopList";
import ShopDetail from "../Login/Shop/ShopDetail";
import MyMessages from "../Login/User/MyMessages";
import IntroScreen from "../NotLogin/IntroScreen";
import ProductDetail2 from "../Login/Product/ProductDetail2";
import ProductDetail3 from "../Login/Product/ProductDetail3";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const StackScreens = () => {
    return(
        <Stack.Navigator
            screenOptions={{headerShown: false}}
            initialRouteName="HomeScreen">
            <Stack.Screen name={"Home"} component={HomeScreen} />
            <Stack.Screen name={"HomeFilter"} component={HomeFilter} />
            <Stack.Screen name={"Filter"} component={Filter} />
            <Stack.Screen name={"TrendCampaigns"} component={TrendCampaigns} />
            <Stack.Screen name={"Settings"} component={Settings} />
            <Stack.Screen name={"Categories"} component={Categories} />
            <Stack.Screen name={"CategoryDetail"} component={CategoryDetail} />
            <Stack.Screen name={"ProductList"} component={ProductList} />
            <Stack.Screen name={"ProductDetail"} component={ProductDetail} />
            <Stack.Screen name={"ProductDetail2"} component={ProductDetail2} />
            <Stack.Screen name={"ProductDetail3"} component={ProductDetail3} />
            <Stack.Screen name={"Cart"} component={Cart} />
            <Stack.Screen name={"CompleteOrder"} component={CompleteOrder} />
            <Stack.Screen name={"Notifications"} component={Notifications} />
            <Stack.Screen name={"PaymentInformation"} component={PaymentInformation} />
            <Stack.Screen name={"AddressList"} component={AddressList} />
            <Stack.Screen name={"NewAddress"} component={NewAddress} />
            <Stack.Screen name={"OrderList"} component={OrderList} />
            <Stack.Screen name={"OrderDetail"} component={OrderDetail} />
            <Stack.Screen name={"CargoTracking"} component={CargoTracking} />
            <Stack.Screen name={"RateProduct"} component={RateProduct} />
            <Stack.Screen name={"Profile"} component={Profile} />
            <Stack.Screen name={"Password"} component={Password} />
            <Stack.Screen name={"Favorites"} component={Favorites} />
            <Stack.Screen name={"Feedback"} component={Feedback} />
            <Stack.Screen name={"AboutUs"} component={AboutUs} />
            <Stack.Screen name={"Privacy"} component={Privacy} />
            <Stack.Screen name={"ContactUs"} component={ContactUs} />
            <Stack.Screen name={"FAQ"} component={FAQ} />
            <Stack.Screen name={"ReturnRequest"} component={ReturnRequest} />
            <Stack.Screen name={"MyAccount"} component={MyAccount} />
            <Stack.Screen name={"AccountInformation"} component={AccountInformation} />
            <Stack.Screen name={"ShopList"} component={ShopList} />
            <Stack.Screen name={"ShopDetail"} component={ShopDetail} />
            <Stack.Screen name={"MyMessages"} component={MyMessages} />
        </Stack.Navigator>
    )
};

export const Router = (props) => {

    // required for navigating screens
    React.useEffect(() => {
        isMountedRef.current = true;

        return () => (isMountedRef.current = false);
    }, []);

    let toastContext = React.useContext(ToastContext);

    let loadingContext = React.useContext(LoadingContext);

    let dialogContext = React.useContext(DialogContext);

    let loginContext = React.useContext(LoginContext);

    // to get user login status from login context
    let isLogin = loginContext.isLogin;

    global.__loadingContext = loadingContext;

    global.__loginContext = loginContext;

    global.__dialogContext = dialogContext;

    global.__toastContext = toastContext;

    return (
        <NavigationContainer
            ref={navigationRef}>

            {
                (isLogin) // if login true

                    ?

                    <Drawer.Navigator
                        drawerStyle={{width: isTablet ? (deviceWidth * 0.5) : deviceWidth * 0.75, backgroundColor: "transparent" }}
                        drawerContent={(props) => <MowDrawerMenu {...props} />}
                        initialRouteName="HomeScreen">
                        <Drawer.Screen name={"HomeScreen"} component={StackScreens} />
                    </Drawer.Navigator>

                    :

                    <Stack.Navigator
                        screenOptions={{headerShown: false}}
                        initialRouteName="Home">
                      {/*  <Stack.Screen name={"IntroScreen"} component={IntroScreen}/>*/}
                        <Stack.Screen name={"Home"} component={Home}/>
                        <Stack.Screen name={"NormalLogin"} component={NormalLogin}/>
                        <Stack.Screen name={"NormalRegister"} component={NormalRegister}/>
                        <Stack.Screen name={"Verification"} component={Verification}/>
                        <Stack.Screen name={"ForgotPassword"} component={ForgotPassword}/>
                        <Stack.Screen name={"ExtraSecurity"} component={ExtraSecurity}/>
                        <Stack.Screen name={"ChangePassword"} component={ChangePassword}/>
                    </Stack.Navigator>
            }

        </NavigationContainer>
    );
};
