
const TodaysBestDiscounts = [

    {
        title: "Nike Jordan Unisex",
        discountRate: "10%",
        firstPrice: "$300",
        lastPrice: "$270",
        star: 4,
        voteCount: "171",
        image: require("../assets/image/todaysBestDiscount/nike_jordan_unisex.png")
    },
    {
        title: "Apple iPad Tablet",
        discountRate: "15%",
        firstPrice: "$699",
        lastPrice: "$499 ",
        star: 3,
        voteCount: "46",
        image: require("../assets/image/todaysBestDiscount/apple_tablet.png")
    },
    {
        title: "Google Smart Watch C350",
        discountRate: "15%",
        firstPrice: "$429",
        lastPrice: "$369",
        star: 4,
        voteCount: "7",
        image: require("../assets/image/todaysBestDiscount/watch_google.png")
    },
    {
        title: "Apple iWatch Smart Watch",
        discountRate: "15%",
        firstPrice: "$429",
        lastPrice: "$369",
        star: 4,
        voteCount: "7",
        image: require("../assets/image/todaysBestDiscount/apple_watch.png")
    },

];

export default TodaysBestDiscounts;
