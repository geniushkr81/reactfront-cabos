

const BestSeller = [

    {
        image: require("../assets/image/bestSeller/under_armour_unisex_sport_bag.png"),
        title: "Under Armour Unisex Sport Bag",
        star: 5,
        voteCount: "21",
        price: "$33",
        stock: true,
        new: true
    },
    {
        image: require("../assets/image/bestSeller/adidas_unisex_shoes.png"),
        title: "Adidas Unisex Shoes",
        star: 2,
        voteCount: "4",
        price: "$319",
        stock: false,
        new: false
    },
    {
        image: require("../assets/image/bestSeller/phlips_shaving_machine.png"),
        title: "Philips Shaving Machine",
        star: 4,
        voteCount: "1.211",
        price: "$74",
        stock: true,
        new: false
    },
    {
        image: require("../assets/image/bestSeller/plhilips_vacuum_cleaner.png"),
        title: "Philips Vacuum Cleaner",
        star: 1,
        voteCount: "1",
        price: "$199",
        stock: true,
        new: false
    },
    {
        image: require("../assets/image/bestSeller/bosh_washing_machine.png"),
        title: "Bosh Washing Machine",
        star: 4,
        voteCount: "41",
        price: "$1199",
        stock: true,
        new: false
    }

];

export default BestSeller;
