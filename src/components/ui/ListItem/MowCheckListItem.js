import React from "react";
import PropTypes from "prop-types";
import {Text, TouchableOpacity, View} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {borderStyle, fontFamily} from "../../../values/Styles/MowStyles";
import {MowCheckBox} from "../Common/CheckBox/MowCheckBox";
import {mowColors} from "../../../values/Colors/MowColors";

export default class MowCheckListItem extends React.Component {

    static propTypes = {
        selected: PropTypes.bool,
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        subtitle: PropTypes.string,
        titleTextStyle: PropTypes.object,
        subtitleTextStyle: PropTypes.object,
        item: PropTypes.object
    };

    state = {
        checkboxChecked: false
    };

    _onPress(){
        let flag = !this.state.checkboxChecked;
        this.setState({
            checkboxChecked: flag
        });

        if(typeof this.props.onPress == "function"){
            this.props.onPress(this.props.item, flag);
        }
    }

    componentDidMount() {
        this.setState({
            checkboxChecked: this.props.selected,
        })
    }


    render() {

        let {style, subtitle, subtitleTextStyle, titleTextStyle, iconName, item} = this.props;
        let {checkboxChecked} = this.state;

        return (
            <TouchableOpacity
                onPress={() => {this._onPress()}}
                style={{
                    height: hp("8%"),
                    flexDirection: "row",
                    backgroundColor: "transparent",
                    ...borderStyle,
                    marginHorizontal: 10,
                    marginVertical: 5,
                    alignItems: "center",
                    paddingHorizontal: 20,
                    justifyContent: "space-between",
                    ...style
                }}>

                <View
                    style={{backgroundColor: null, justifyContent: "center"}}>

                    <Text
                        style={{
                            fontSize: hp("1.8%"),
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "left",
                            color: mowColors.titleTextColor,
                            fontFamily: fontFamily.bold,
                            ...this.props.titleTextStyle
                        }}>

                        {this.props.title}

                    </Text>

                    {
                        this.props.subtitle != "" && this.props.subtitle &&

                        <Text
                            numberOfLines={3}
                            ellipsizeMode={"tail"}
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp("1.9%"),
                                ...subtitleTextStyle
                            }}>

                            {subtitle}

                        </Text>

                    }

                </View>

                <MowCheckBox
                    onPress={() => {this._onPress()}}
                    checkedColor={mowColors.mainColor}
                    checked={this.state.checkboxChecked}
                    containerStyle={{alignSelf: "center", right: wp("5%")}}/>


            </TouchableOpacity>
        )
    }
}
