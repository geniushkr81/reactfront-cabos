import React from "react";
import PropTypes from "prop-types";
import {TouchableOpacity, View} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {borderStyle} from "../../../values/Styles/MowStyles";
import {MowCheckBox} from "../Common/CheckBox/MowCheckBox";
import {mowColors} from "../../../values/Colors/MowColors";

export default class MowCheckColorListItem extends React.Component {

    static propTypes = {
        color: PropTypes.string,
        checkboxChecked: PropTypes.bool
    };

    state = {
        checkboxChecked: false,
    };

    _onPress(){
        let flag = !this.state.checkboxChecked;
        this.setState({
            checkboxChecked: flag
        });

        if(typeof this.props.onPress == "function"){
            this.props.onPress(this.props.item, flag);
        }
    }

    componentDidMount() {
        this.setState({
            checkboxChecked: this.props.selected,
        })
    }

    render() {

        let {color, style} = this.props;
        let {checkboxChecked} = this.state;

        return (
            <TouchableOpacity
                onPress={() => {this._onPress()}}
                style={{
                    height: hp("7%"),
                    flexDirection: "row",
                    backgroundColor: "transparent",
                    ...borderStyle,
                    marginHorizontal: 10,
                    marginVertical: 5,
                    ...style
                }}>

                <View
                    style={{
                        flex: 1,
                        justifyContent: "space-between",
                        alignItems: "center",
                        flexDirection: "row",
                        paddingHorizontal: 20
                    }}>

                    <View
                        style={{
                            width: hp("3%"),
                            height: hp("3%"),
                            backgroundColor: color,
                            borderRadius: 100
                        }}/>

                    <MowCheckBox
                        onPress={() => {this._onPress()}}
                        checkedColor={mowColors.mainColor}
                        checked={checkboxChecked}
                        containerStyle={{alignSelf: "center"}}/>

                </View>

            </TouchableOpacity>
        )
    }
}
