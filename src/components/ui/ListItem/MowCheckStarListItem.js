import React from "react";
import PropTypes from "prop-types";
import {TouchableOpacity, View} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {borderStyle} from "../../../values/Styles/MowStyles";
import {MowStarView} from "../AppSpecifics/StarView/MowStarView";
import {MowCheckBox} from "../Common/CheckBox/MowCheckBox";
import {mowColors} from "../../../values/Colors/MowColors";

export default class MowCheckStarListItem extends React.Component {

    static propTypes = {
        selected: false,
        item: PropTypes.object,
        score: PropTypes.number
    };

    state = {
        checkboxChecked: false
    };

    _onPress(){
        let flag = !this.state.checkboxChecked;
        this.setState({
            checkboxChecked: flag
        });

        if(typeof this.props.onPress == "function"){
            this.props.onPress(this.props.item, flag);
        }
    }

    componentDidMount() {
        this.setState({
            checkboxChecked: this.props.selected,
        })
    }

    render() {

        let {score, style} = this.props;
        let {checkboxChecked} = this.state;

        return (
            <TouchableOpacity
                onPress={() => {this._onPress()}}
                style={{
                    height: hp("7%"),
                    flexDirection: "row",
                    backgroundColor: "transparent",
                    ...borderStyle,
                    marginHorizontal: 10,
                    marginVertical: 5,
                    justifyContent: "space-between",
                    alignItems: "center",
                    paddingHorizontal: 20,
                    ...style
                }}>

                <MowStarView
                    width={wp("25%")}
                    height={hp("2.5%")}
                    score={score}/>

                <MowCheckBox
                    onPress={() => {this._onPress()}}
                    checkedColor={mowColors.mainColor}
                    checked={checkboxChecked}
                    containerStyle={{alignSelf: "center"}}/>

            </TouchableOpacity>
        )
    }
}
