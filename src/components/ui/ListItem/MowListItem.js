import React from "react";
import PropTypes from "prop-types";
import {Text, TouchableOpacity, View, Image} from "react-native";
import FaIcon from "react-native-vector-icons/FontAwesome";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {mowColors} from "../../../values/Colors/MowColors";
import {fontFamily} from "../../../values/Styles/MowStyles";
import {isTablet} from "../../../values/Constants/MowConstants";

export const MowListItem = (props) => {

    let {
        imagePath,
        imageUrl,
        titleTextStyle,
        subtitleTextStyle,
        iconName,
        title,
        subtitle,
        selected,
        onPress,
        style
    } = props;

    return (

        <TouchableOpacity
            onPress={onPress}
            style={[{
                height: hp("12%"),
                flexDirection: "row",
                ...style
            }]}>

            {
                (imageUrl || imagePath) &&

                <View
                    style={{flex: 2.5, justifyContent: "center", alignItems: "center"}}>

                    {
                        imagePath &&
                        <Image
                            style={{width: hp("8%"), height: hp("10%")}}
                            resizeMode={"contain"}
                            source={imagePath}/>
                    }

                    {
                        imageUrl &&
                        <Image
                            style={{width: hp("8%"), height: hp("10%")}}
                            resizeMode={"contain"}
                            source={{uri: imageUrl}}/>
                    }

                </View>
            }

            <View
                style={{
                    flex: 5,
                    marginLeft: (imageUrl || imagePath) ? 0 : 30,
                    backgroundColor: null,
                    flexDirection: "column",
                    justifyContent: "center"
                }}>

                <Text
                    numberOfLines={1}
                    style={{
                        fontSize: hp("1.8%"),
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.titleTextColor,
                        fontFamily: fontFamily.bold,
                        right: (imagePath ||imageUrl) ? 0 : wp(5),
                        left: isTablet ? wp(1) : 0,
                        ...titleTextStyle
                    }}>

                    {title}

                </Text>

                {
                    !(subtitle === "" || !subtitle || (typeof subtitle === "undefined")) &&

                    <Text
                        numberOfLines={3}
                        ellipsizeMode={"tail"}
                        style={{
                            color: "#afafaf",
                            fontSize: hp("1.5%"),
                            right: (imagePath ||imageUrl) ? 0 : wp(5),
                            left: isTablet ? wp(1) : 0,
                            ...subtitleTextStyle
                        }}>

                        {subtitle}

                    </Text>

                }

            </View>

            <View
                style={{flex: 1, backgroundColor: null, justifyContent: "center", alignItems: "center"}}>

                {
                    selected

                        ?

                        <FaIcon
                            style={{fontSize: hp("3%"), color: "green"}}
                            name={"check"} />

                        :

                        <FaIcon
                            style={{fontSize: hp("3%"), color: "#e0e0e0"}}
                            name={iconName || "chevron-right"}/>
                }

            </View>

        </TouchableOpacity>

    )
};

// list item props
MowListItem.propTypes = {
    selected: PropTypes.bool,
    imagePath: PropTypes.number,
    imageUrl: PropTypes.string,
    title: PropTypes.string,
    titleTextStyle: PropTypes.object,
    subtitleTextStyle: PropTypes.object,
    subtitle: PropTypes.string,
    onPress: PropTypes.func,
    iconName: PropTypes.string
};

MowListItem.defaultProps = {
    selected: false,
};
