import React from "react";
import {FlatList, Image, Text, TouchableOpacity, View} from "react-native"
import PropTypes from 'prop-types';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {isTablet, platform} from "../../../values/Constants/MowConstants";
import FAIcon from "react-native-vector-icons/FontAwesome";
import Carousel, {Pagination} from "react-native-snap-carousel";
import {mowColors} from "../../../values/Colors/MowColors";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {borderStyle, fontFamily, paginationStyle} from "../../../values/Styles/MowStyles";
import {MowStarView} from "./StarView/MowStarView";
import {MowButton} from "../Common/Button/MowButton";
import {ShowToast} from "../../global/common/Toasts";
import {navigate} from "../../../RootMethods/RootNavigation";

let self;

export class MowProductList extends React.Component {

    static propTypes = {
        boxView: PropTypes.bool,
        listStyle: PropTypes.object,
        listData: PropTypes.array,
        activeSlide: PropTypes.array,
        productListKey: PropTypes.number
    };

    static defaultProps = {
        boxView: true,
        productListKey: 0
    };

    state = {
        activeSlide: [],
        activeIndex: 0,
        activeDotIndex: 0
    }

    componentDidMount() {
        // to set all active index as 0
        let arr = [];
        let length = this.props.listData.length;
        for (let i = 0; i < length; i++){
            arr[i] = 0;
        }
        this.setState({activeSlide: arr});

        self = this;
    }

    _addToCart(product) {
        ShowToast.success(mowStrings.productAdded)
    }

    // image pagination style
    pagination (data, index) {
        if (self) {
            return (
                <Pagination
                    dotsLength={data.length}
                    activeDotIndex={this.state.activeSlide[index]}
                    containerStyle={paginationStyle.container}
                    dotStyle={[paginationStyle.activeDot, {backgroundColor: mowColors.pagination.activeDot}]}
                    inactiveDotStyle={[paginationStyle.passiveDot, {backgroundColor: mowColors.pagination.passiveDot}]}
                    inactiveDotOpacity={paginationStyle.inactiveDotOpacity}
                    inactiveDotScale={paginationStyle.inactiveDotScale}/>
            );
        }
    }

    // to handle active slide for all items
    _handleActiveSlide(activeSlide, index) {
        let activeSlideArr = this.state.activeSlide;
        activeSlideArr[index] = activeSlide;
        this.setState({
            activeSlide: activeSlideArr,
        });
    }

    _renderImages ({item, index}) {
        return (
            <TouchableOpacity
                onPress={() => {navigate("ProductDetail", {product: self.props.listData[index]})}}
                key={index}>

                <Image
                    style={{height: "100%", width: "100%", borderRadius: 10}}
                    resizeMode={"contain"}
                    source={item["image"]}/>

            </TouchableOpacity>
        );
    }

    render() {

        let {boxView, listStyle, listData, productListKey} = this.props;

        return(

            <View
                style={{flex: 1}}>

                {/* product list */}
                {
                    boxView

                        ?

                        // box view
                        <FlatList
                            key={productListKey}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={isTablet ? 3 : 2}
                            style={listStyle}
                            data={listData}
                            renderItem={({ item, index }) => (

                                //product item
                                <View
                                    key={index}
                                    style={
                                        isTablet

                                        ?

                                            {
                                                margin: 5,
                                                width: wp(29),
                                                marginHorizontal: wp(2)
                                            }

                                        :
                                            {
                                                margin: 5,
                                                flex: 1,
                                            }
                                    }>

                                    {/* image view */}
                                    <View
                                        style={{
                                            height: platform === "android" ? hp("25") : hp("21"),
                                            width: "100%",
                                            borderRadius: 10,
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 10, right: 10, zIndex: 99}}>

                                            <FAIcon
                                                style={{
                                                    color: "grey",
                                                    fontSize: hp("2%")
                                                }}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        {/* didn't use swiper, because multiple swiper causes android ui problem */}
                                        {/* product image slider */}
                                        <Carousel
                                            removeClippedSubviews={false}
                                            ref={(c) => {this._carousel = c}}
                                            data={item["images"]}
                                            onSnapToItem={(activeSlide) => this._handleActiveSlide(activeSlide, index)}
                                            sliderWidth={wp("45%")}
                                            itemWidth={wp("45%")}
                                            renderItem={this._renderImages}/>

                                        {/* image pagination */}
                                        {this.pagination(item["images"], index)}

                                        {
                                            item["new"] &&

                                            <View
                                                style={{
                                                    position: "absolute",
                                                    backgroundColor: mowColors.mainColor,
                                                    top: 10,
                                                    left: 10,
                                                    borderRadius: 200,
                                                    width: hp("5%"),
                                                    height: hp("5%"),
                                                    justifyContent: "center"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        textAlign: "center",
                                                        color: "#ffffff",
                                                        fontSize: hp(1.7)
                                                    }}>

                                                    {mowStrings.homeScreen.new}

                                                </Text>

                                            </View>
                                        }

                                        {
                                            !item["stock"] &&

                                            // out of stock view
                                            <View
                                                style={{
                                                    position: "absolute",
                                                    opacity: 0.8,
                                                    backgroundColor: "#848484",
                                                    width: "100%"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontSize: hp("1.8%"),
                                                        fontWeight: "normal",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "center",
                                                        color: "#ffffff"
                                                    }}>

                                                    {mowStrings.homeScreen.outOfStock}

                                                </Text>

                                            </View>

                                        }

                                    </View>

                                    <View
                                        style={{height: hp(11.5)}}>

                                        {/* title text */}
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                                marginTop: 5,
                                                fontSize: hp("1.8%"),
                                                fontWeight: "normal",
                                                fontStyle: "normal",
                                                letterSpacing: 0,
                                                textAlign: "left",
                                                color: mowColors.titleTextColor,
                                                fontFamily: fontFamily.regular
                                            }}>

                                            {item["title"]}

                                        </Text>

                                        {/* star view */}
                                        <View
                                            style={{flexDirection: "row", alignItems: "center", marginTop: 1}}>

                                            {/* stars*/}
                                            <MowStarView
                                                score={item["star"]}/>

                                            {/* vote count text */}
                                            <Text
                                                style={{
                                                    marginLeft: 3,
                                                    fontSize: hp("1.5%"),
                                                    letterSpacing: 0,
                                                    textAlign: "left",
                                                    color: mowColors.textColor,
                                                    fontFamily: fontFamily.regular,
                                                }}>

                                                {"("}{item["voteCount"]}{")"}

                                            </Text>

                                        </View>

                                        {/* price & discount view */}
                                        {
                                            item["discountRate"]

                                                ?

                                                <View
                                                    style={{flexDirection: "row", marginTop: 3, alignItems: "center"}}>

                                                    {/* discount rate view */}
                                                    <View
                                                        style={{
                                                            backgroundColor: mowColors.successColor,
                                                            borderRadius: 5,
                                                            justifyContent: "center",
                                                            alignItems: "center",
                                                            width: hp("5%"),
                                                            height: hp("5%")
                                                        }}>

                                                        <Text
                                                            style={{
                                                                fontSize: hp("2%"),
                                                                fontWeight: "bold",
                                                                fontStyle: "normal",
                                                                letterSpacing: 0,
                                                                textAlign: "left",
                                                                color: "#ffffff"
                                                            }}>

                                                            {item["discountRate"]}

                                                        </Text>

                                                    </View>

                                                    {/* price view */}
                                                    <View
                                                        style={{marginLeft: 10, marginTop: 3}}>

                                                        {/* first price text view  */}
                                                        <View
                                                            style={{alignItems: "center", justifyContent: "center"}}>

                                                            {/* first price text */}
                                                            <Text
                                                                style={{
                                                                    fontSize: hp("1.8%"),
                                                                    fontWeight: "300",
                                                                    fontStyle: "normal",
                                                                    letterSpacing: 0,
                                                                    textAlign: "center",
                                                                    color: mowColors.textColor,
                                                                    fontFamily: fontFamily.light
                                                                }}>

                                                                {item["currency"]}{item["firstPrice"]}

                                                            </Text>

                                                            <View
                                                                style={{
                                                                    backgroundColor: mowColors.mainColor,
                                                                    width: "100%",
                                                                    height: hp("0.1%"),
                                                                    position: "absolute",
                                                                }}/>

                                                        </View>

                                                        {/* last price text */}
                                                        <Text
                                                            style={{
                                                                fontSize: hp("2%"),
                                                                fontWeight: "bold",
                                                                fontStyle: "normal",
                                                                letterSpacing: 0,
                                                                textAlign: "center",
                                                                color: mowColors.titleTextColor,
                                                                fontFamily: fontFamily.bold
                                                            }}>

                                                            {item["currency"]}{item["lastPrice"]}

                                                        </Text>

                                                    </View>

                                                </View>

                                                :

                                                <Text
                                                    style={{
                                                        fontSize: hp("2%"),
                                                        fontWeight: "bold",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "left",
                                                        color: mowColors.titleTextColor,
                                                        marginTop: 5,
                                                        fontFamily: fontFamily.bold
                                                    }}>

                                                    {item["currency"]}{item["lastPrice"]}

                                                </Text>
                                        }

                                    </View>

                                    {/* add to cart button */}
                                    <MowButton
                                        buttonText={mowStrings.button.addToCart}
                                        onPress={() => {this._addToCart(item)}}
                                        containerStyle={{marginBottom: 0, marginTop: 10, borderColor: mowColors.textColor}}
                                        textStyle={{color: mowColors.textColor}}
                                        type={"success"}
                                        size={"small"}
                                        filled={false}/>

                                </View>

                            )}
                        />

                        :

                        // list view
                        <FlatList
                            key={productListKey}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={isTablet ? 2 : 1}
                            style={listStyle}
                            data={listData}
                            renderItem={({ item, index }) => (

                                //product item
                                <View
                                    key={index}
                                    style={
                                        isTablet

                                        ?

                                            {
                                                margin: 5,
                                                marginVertical: 8,
                                                flexDirection: "row",
                                                ...borderStyle,
                                                height: hp(18),
                                                width: "49%",
                                                borderRadius: 10
                                            }

                                        :

                                            {
                                                margin: 5,
                                                marginVertical: 8,
                                                flex: 1,
                                                flexDirection: "row",
                                                ...borderStyle,
                                                height: hp(18),
                                                width: "98%",
                                                borderRadius: 10
                                            }
                                    }>

                                    {/* image view */}
                                    <View
                                        style={{
                                            marginRight: 10,
                                            borderRadius: 10,
                                            alignItems: "center",
                                            justifyContent: "center"
                                        }}>

                                        {/* hearth icon touchable */}
                                        <TouchableOpacity
                                            style={{position: "absolute", top: 5, right: 5, zIndex: 99}}>

                                            <FAIcon
                                                style={{color: "grey", fontSize: hp("1.5%")}}
                                                name={"heart"}/>

                                        </TouchableOpacity>

                                        {/* product image slider */}
                                        <Carousel
                                            removeClippedSubviews={false}
                                            ref={(c) => {this._carousel = c}}
                                            data={item["images"]}
                                            sliderWidth={isTablet ? wp(20) : wp(30)}
                                            itemWidth={isTablet ? wp(20) : wp(30)}
                                            renderItem={this._renderImages}/>

                                        {
                                            item["new"] &&

                                            <View
                                                style={{
                                                    position: "absolute",
                                                    backgroundColor: mowColors.mainColor,
                                                    top: 5,
                                                    left: 5,
                                                    borderRadius: 200,
                                                    width: hp("3.2%"),
                                                    height: hp("3.2%"),
                                                    justifyContent: "center"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        textAlign: "center",
                                                        color: "#ffffff",
                                                        fontSize: hp(1.3)
                                                    }}>

                                                    {mowStrings.homeScreen.new}

                                                </Text>

                                            </View>
                                        }

                                        {
                                            !item["stock"] &&

                                            // out of stock view
                                            <View
                                                style={{
                                                    position: "absolute",
                                                    opacity: 0.8,
                                                    backgroundColor: "#848484",
                                                    width: "100%"
                                                }}>

                                                <Text
                                                    style={{
                                                        fontSize: hp("1.8%"),
                                                        fontWeight: "normal",
                                                        fontStyle: "normal",
                                                        letterSpacing: 0,
                                                        textAlign: "center",
                                                        color: "#ffffff"
                                                    }}>

                                                    {mowStrings.homeScreen.outOfStock}

                                                </Text>

                                            </View>

                                        }

                                    </View>

                                    {/* info view */}
                                    <View>

                                        <View
                                            style={isTablet ? {flex: 1, width: wp(25)} : {flex: 1}}>

                                            {/* title text */}
                                            <Text
                                                numberOfLines={2}
                                                style={{
                                                    marginTop: 1,
                                                    marginBottom: 3,
                                                    fontSize: hp(1.8),
                                                    fontWeight: "500",
                                                    fontStyle: "normal",
                                                    letterSpacing: 0,
                                                    textAlign: "left",
                                                    color: mowColors.titleTextColor,
                                                    fontFamily: fontFamily.regular
                                                }}>

                                                {item["title"]}

                                            </Text>

                                            {/* star view */}
                                            <View
                                                style={{flexDirection: "row", alignItems: "center", marginVertical: 1}}>

                                                {/* stars*/}
                                                <MowStarView
                                                    width={hp(8)}
                                                    height={hp(1.5)}
                                                    score={item["star"]}/>

                                                {/* vote count text */}
                                                <Text
                                                    style={{
                                                        marginLeft: 3,
                                                        fontSize: hp("1.3%"),
                                                        letterSpacing: 0,
                                                        textAlign: "left",
                                                        color: mowColors.textColor,
                                                        fontFamily: fontFamily.regular
                                                    }}>

                                                    {"("}{item["voteCount"]}{")"}

                                                </Text>

                                            </View>

                                            {/* price & discount view */}
                                            {
                                                item["discountRate"]

                                                    ?

                                                    <View
                                                        style={{flexDirection: "row", marginVertical: 1, alignItems: "center"}}>

                                                        {/* discount rate view */}
                                                        <View
                                                            style={{
                                                                backgroundColor: mowColors.successColor,
                                                                borderRadius: 5,
                                                                justifyContent: "center",
                                                                alignItems: "center",
                                                                width: wp("9%"),
                                                                height: hp("3.5%")
                                                            }}>

                                                            <Text
                                                                style={{
                                                                    fontSize: hp("1.6%"),
                                                                    fontWeight: "bold",
                                                                    fontStyle: "normal",
                                                                    letterSpacing: 0,
                                                                    textAlign: "left",
                                                                    color: "#ffffff",
                                                                    fontFamily: fontFamily.bold
                                                                }}>

                                                                {item["discountRate"]}

                                                            </Text>

                                                        </View>

                                                        {/* price view */}
                                                        <View
                                                            style={{marginLeft: 10}}>

                                                            {/* first price text view  */}
                                                            <View
                                                                style={{alignItems: "center", justifyContent: "center"}}>

                                                                {/* first price text */}
                                                                <Text
                                                                    style={{
                                                                        fontSize: hp("1.5%"),
                                                                        fontWeight: "300",
                                                                        fontStyle: "normal",
                                                                        letterSpacing: 0,
                                                                        textAlign: "center",
                                                                        color: mowColors.textColor,
                                                                        fontFamily: fontFamily.light
                                                                    }}>

                                                                    {item["currency"]}{item["firstPrice"]}

                                                                </Text>

                                                                <View
                                                                    style={{
                                                                        backgroundColor: mowColors.textColor,
                                                                        width: "100%",
                                                                        height: hp("0.1%"),
                                                                        position: "absolute",
                                                                    }}/>

                                                            </View>

                                                            {/* last price text */}
                                                            <Text
                                                                style={{
                                                                    fontSize: hp("1.7%"),
                                                                    fontWeight: "bold",
                                                                    fontStyle: "normal",
                                                                    letterSpacing: 0,
                                                                    textAlign: "center",
                                                                    color: mowColors.titleTextColor,
                                                                    fontFamily: fontFamily.bold
                                                                }}>

                                                                {item["currency"]}{item["lastPrice"]}

                                                            </Text>

                                                        </View>

                                                    </View>

                                                    :

                                                    <Text
                                                        style={{
                                                            fontSize: hp("1.8%"),
                                                            fontWeight: "bold",
                                                            fontStyle: "normal",
                                                            textAlign: "left",
                                                            color: mowColors.titleTextColor,
                                                            marginVertical: 5,
                                                            fontFamily: fontFamily.bold
                                                        }}>

                                                        {item["currency"]}{item["lastPrice"]}

                                                    </Text>
                                            }

                                        </View>

                                        <View
                                            style={{width: isTablet ? wp(25) : wp(35), marginBottom: 10}}>

                                            {/* add to cart button */}
                                            <MowButton
                                                buttonText={mowStrings.button.addToCart}
                                                containerStyle={{marginBottom: 0, marginTop: 10, borderColor: mowColors.textColor}}
                                                textStyle={{color: mowColors.textColor}}
                                                onPress={() => {this._addToCart(item)}}
                                                type={"success"}
                                                size={"xSmall"}
                                                filled={false}/>

                                        </View>

                                    </View>

                                </View>

                            )}
                        />

                }

            </View>

        )

    }
}
