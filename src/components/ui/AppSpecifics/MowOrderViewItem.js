import React from "react";
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity, Image, FlatList} from "react-native";
import FeatherIcon from "react-native-vector-icons/Feather";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {mowColors} from "../../../values/Colors/MowColors";
import {navigate} from "../../../RootMethods/RootNavigation";
import {borderStyle, fontFamily, shadowStyle} from "../../../values/Styles/MowStyles";
import {mowStrings} from "../../../values/Strings/MowStrings";
import {isTablet} from "../../../values/Constants/MowConstants";

export default class MowOrderViewItem extends React.Component {

    static propTypes = {
        product: PropTypes.object,
        listKey: PropTypes.number,
    };

    orderStyle = {
        icon: {
            color: mowColors.mainColor,
            fontSize: hp("2.5%")
        }
    };

    render() {

        let {listKey, product} = this.props;

        return(

            <View
                style={{
                    marginHorizontal: wp("2%"),
                    marginVertical: hp("1%"),
                    paddingHorizontal: isTablet ? wp(1) : wp(5),
                    backgroundColor: mowColors.categoryBGColor,
                    padding: 10,
                    ...borderStyle,
                    borderWidth: 1.5,
                    ...shadowStyle,
                }}
                key={listKey}>

                <View
                    style={{flexDirection: "row", justifyContent: "space-between"}}>

                    {/* date & price view */}
                    <View>

                        {/* date text */}
                        <Text
                            style={{
                                fontSize: hp("1.7%"),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor,
                            }}>

                            {product["date"]}

                        </Text>

                        {/* price text */}
                        <Text
                            style={{
                                fontSize: hp("1.7%"),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: "#48bb20",
                                marginTop: 5
                            }}>

                            {product["currency"]}{product["price"]}

                        </Text>

                    </View>

                    {/* detail button */}
                    <TouchableOpacity
                        onPress={() => {navigate("OrderDetail", {product: product})}}
                        style={{flexDirection: "row", justifyContent: "center", alignItems: "center"}}>

                        <Text
                            style={{
                                fontSize: hp("1.7%"),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: mowColors.titleTextColor,
                            }}>

                            {mowStrings.productDetail.detail}

                        </Text>

                        <FeatherIcon
                            name={"chevron-right"}
                            style={{
                                color: mowColors.mainColor,
                                fontSize: hp(2)
                            }}/>

                    </TouchableOpacity>

                </View>

                {/* delivery info view */}
                <View
                    style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start", marginTop: 30}}>

                    {
                        product["delivery"] &&

                        <FeatherIcon
                            name={"truck"}
                            style={[this.orderStyle.icon]}/>
                    }

                    {
                        product["complete"] &&

                        <FeatherIcon
                            name={"check-circle"}
                            style={[this.orderStyle.icon, {color: "#65b707"}]}/>
                    }

                    {
                        product["cancel"] &&

                        <FeatherIcon
                            name={"x-circle"}
                            style={this.orderStyle.icon}/>
                    }

                    {
                        product["return"] &&

                        <FeatherIcon
                            name={"x-circle"}
                            style={this.orderStyle.icon}/>
                    }

                    {/* order situation */}
                    <Text
                        style={{
                            marginLeft: 5,
                            fontSize: hp("1.4%"),
                            fontWeight: "bold",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "left",
                            color: mowColors.titleTextColor
                        }}>

                        {product["productInfo"]}

                    </Text>

                </View>

                {/* image list */}
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={product["images"]}
                    style={{marginTop: 10}}
                    renderItem={({ item, index }) => (

                        <View
                            key={index}
                            style={{marginRight: 10}}>

                            <Image
                                source={item["path"]}
                                resizeMode={"contain"}
                                style={{height: hp("8%"), width: isTablet ? hp("6%") : hp(8), borderRadius: 10}}/>

                        </View>

                    )}/>

                <Text
                    style={{
                        marginTop: 10,
                        color: mowColors.textColor,
                        fontSize: hp(1.8),
                        fontFamily: fontFamily.regular
                    }}>

                    {product["count"]} {product["count"] > 1 ? mowStrings.orderList.deliveredProducts : mowStrings.orderList.deliveredProduct}

                </Text>

            </View>

        )

    }
}
