import React from "react";
import PropTypes from 'prop-types';
import {View, Text, Image} from "react-native";
import FeatherIcon from "react-native-vector-icons/Feather";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {mowColors} from "../../../values/Colors/MowColors";
import {TouchableOpacity} from "react-native-gesture-handler";
import {navigate} from "../../../RootMethods/RootNavigation";
import ShopListData from "../../../SampleData/Shop/ShopListData";
import {mowStrings} from "../../../values/Strings/MowStrings";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {isTablet, platform} from "../../../values/Constants/MowConstants";

export default class MowProductInfoView extends React.Component {

    static propTypes = {
        product: PropTypes.object,
        opacity: PropTypes.bool
    };

    static defaultProps = {
        opacity: false
    };

    render() {

        let {opacity, product} = this.props;

        return(

            <View
                style={{width: "100%"}}>

                <View
                    style={{flexDirection: "row", justifyContent: "space-between", marginBottom: 15}}>

                    {/* go to dealer shop screen button */}
                    <TouchableOpacity
                        onPress={() => navigate("ShopDetail", {shop: ShopListData[0]})}
                        style={{
                            flexDirection: "row",
                            alignItems: "center"
                        }}>

                        {/* dealer title text */}
                        <Text
                            style={{
                                color: mowColors.textColor,
                                fontSize: hp(1.7),
                                fontWeight: "500"
                            }}>

                            {mowStrings.orderDetail.dealer}

                        </Text>

                        {/* dealer text */}
                        <Text
                            style={{
                                marginLeft: 5,
                                color: mowColors.titleTextColor,
                                fontSize: hp(1.7),
                                fontWeight: "600"
                            }}>

                            {product["dealer"]}

                        </Text>

                        <FAIcon
                            name={"angle-right"}
                            style={{
                                marginLeft: 5,
                                color: mowColors.textColor,
                                fontSize: hp(2.5)
                            }}/>

                    </TouchableOpacity>

                    <View
                        style={{flexDirection: "row", alignItems: "center"}}>

                        {
                            product["delivery"] &&

                            <FeatherIcon
                                name={"truck"}
                                style={[{color: mowColors.mainColor, fontSize: hp("2.5%")}]}/>
                        }

                        {
                            product["complete"] &&

                            <FeatherIcon
                                name={"check-circle"}
                                style={[{color: "#65b707", fontSize: hp("2.5%")}]}/>
                        }

                        {
                            product["cancel"] &&

                            <FeatherIcon
                                name={"x-circle"}
                                style={[{color: mowColors.mainColor, fontSize: hp("2.5%")}]}/>
                        }

                        {
                            product["return"] &&

                            <FeatherIcon
                                name={"x-circle"}
                                style={[{color: mowColors.mainColor, fontSize: hp("2.5%")}]}/>
                        }

                        {/* order situation */}
                        <Text
                            style={{
                                marginLeft: 5,
                                fontSize: hp("1.6%"),
                                fontWeight: "bold",
                                fontStyle: "normal",
                                letterSpacing: 0,
                                textAlign: "left",
                                color: this.props.opacity ? mowColors.textColor : mowColors.titleTextColor
                            }}>

                            {product["productInfo"]}

                        </Text>

                    </View>

                </View>

               <View
                style={{flexDirection: "row"}}>

                   {/* product image */}
                   <View
                       style={{
                           flex: isTablet ? 0.3 : (platform === "android" ? 0.4 : 0.5),
                           alignItems: "center",
                           justifyContent: "center"
                       }}>

                       <Image
                           source={product["image"]}
                           resizeMode={"contain"}
                           style={{
                               height: hp("12%"),
                               width: isTablet ? hp(10) : hp(10),
                               borderRadius: 10,
                           }}/>

                       {
                           opacity &&

                           <View
                               style={{
                                   height: hp("12%"),
                                   width: hp("12%"),
                                   position: "absolute",
                                   backgroundColor: "rgb(238, 238, 238)",
                                   zIndex: 1,
                                   borderRadius: 10,
                                   opacity: 0.83,
                               }}/>
                       }

                   </View>

                   {/* info view */}
                   <View
                       style={{marginLeft: 10, flex: 2, justifyContent: "center"}}>

                       {/* title text */}
                       <Text
                           style={{
                               fontSize: hp("1.7%"),
                               fontWeight: "normal",
                               fontStyle: "normal",
                               letterSpacing: 0,
                               textAlign: "left",
                               color: mowColors.titleTextColor
                           }}>

                           {product["title"]}

                       </Text>

                       {/* price text */}
                       <Text
                           style={{
                               marginVertical: 10,
                               fontSize: hp("1.8%"),
                               fontWeight: "bold",
                               fontStyle: "normal",
                               letterSpacing: 0,
                               textAlign: "left",
                               color: this.props.opacity ? "#b6babe" : mowColors.textColor
                           }}>

                           {product["currency"]}{product["price"]}

                       </Text>

                   </View>

               </View>

            </View>

        )

    }
}
