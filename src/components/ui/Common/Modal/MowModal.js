import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import {View, Modal, Text} from "react-native";
import {mowColors} from "../../../../values/Colors/MowColors";
import {navbarHeight} from "../../../../values/Constants/MowConstants";
import {mowStrings} from "../../../../values/Strings/MowStrings";
import {fontFamily} from "../../../../values/Styles/MowStyles";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import MowStatusBar from "../../Core/StatusBar/MowStatusBar";
import {MowButton} from "../Button/MowButton";

export const MowModal = (props) => {
    let {modalVisible, onClosed, title, closeButton} = props;

    const [isModalVisible, setModalVisible] = useState(modalVisible);

    useEffect(() => {
        setModalVisible(modalVisible)
    }, [modalVisible]);

    if (isModalVisible) {
        return (
            <Modal
                supportedOrientations={['portrait', 'landscape']}
                animationType="slide"
                transparent={false}
                {...props}>

                <MowStatusBar/>

                <View
                    style={{backgroundColor: mowColors.mainColor, height: navbarHeight, flexDirection: "row", alignItems: "center"}}>

                    {/* modal close button */}
                    {
                        closeButton &&

                        <MowButton
                            buttonText={mowStrings.button.close}
                            stickyIcon={true}
                            filled={false}
                            size={"small"}
                            leftIconStyle={{
                                position: "relative",
                                left: 0,
                                marginRight: 5,
                            }}
                            containerStyle={{
                                width: "25%",
                                alignSelf: "flex-start",
                                elevation: 0,
                                margin: 0,
                                padding: 0,
                                height: navbarHeight,
                                borderWidth: 0,
                                alignItems: "center"
                            }}
                            leftIcon={"x-circle"}
                            onPress={() => {
                                setModalVisible(false);
                                onClosed();
                            }}/>
                    }

                    {
                        title &&

                        <Text
                            style={{
                                alignSelf: "center",
                                width: closeButton ? "50%" : "100%",
                                textAlign: "center",
                                color: "white",
                                fontFamily: fontFamily.bold,
                                fontSize: hp("2%")
                            }}>

                            {title}

                        </Text>
                    }

                </View>

                {props.children}

            </Modal>
        )
    }
    else {
        return null;
    }
}

MowModal.propTypes = {
    ...Modal.props,
    modalVisible: PropTypes.bool,
    onClosed: PropTypes.func,
    title: PropTypes.string,
    closeButton: PropTypes.bool
};

MowModal.defaultProps = {
    onClosed: null,
    closeButton: true
}