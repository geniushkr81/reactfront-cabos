import React from "react";
import {TouchableOpacity, Text} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from 'prop-types';
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import {mowColors} from "../../../../values/Colors/MowColors";

export const MowCheckBox = (props) => {

    let {checked, checkedColor, containerStyle, boxSize, onPress, text, textStyle} = props;

    return(

        <TouchableOpacity
            onPress={() => {
                if (typeof onPress == "function") {
                    onPress();
                }
            }}
            style={{
                flexDirection: "row",
                ...containerStyle
            }}>

            <FAIcon
                style={{color: checked ? checkedColor : "grey", fontSize: boxSize}}
                name={checked ? "check-square-o" : "square-o"}/>

            {
                text !== "" &&

                    <Text
                        style={{
                            marginLeft: 5,
                            fontWeight: "400",
                            fontSize: hp(1.8),
                            ...textStyle
                        }}>

                        {text}

                    </Text>
            }

        </TouchableOpacity>

    )

};

MowCheckBox.propTypes = {
    onPress: PropTypes.func,
    checkedColor: PropTypes.string,
    containerStyle: PropTypes.object,
    checked: PropTypes.bool,
    boxSize: PropTypes.number,
    text: PropTypes.string,
    textStyle: PropTypes.object
};

MowCheckBox.defaultProps = {
    text: "",
    checked: false,
    checkedColor: mowColors.mainColor,
    boxSize: hp(2.5)
};
