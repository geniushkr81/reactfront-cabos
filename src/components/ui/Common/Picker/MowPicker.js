import React, {useState, useEffect} from "react";
import PropTypes from 'prop-types';
import {TouchableOpacity, View, Text, FlatList} from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import {heightPercentageToDP as hp} from "react-native-responsive-screen"
import {Dialog} from "react-native-popup-dialog/src";
import {SlideAnimation} from "react-native-popup-dialog";
import {deviceWidth} from "../../../../values/Constants/MowConstants";
import {_MF} from "../../../global/MowFunctions/MowFunctions";
import {MowInput} from "../Input/MowInput";
import {mowStrings} from "../../../../values/Strings/MowStrings";
import {mowColors} from "../../../../values/Colors/MowColors";

export const MowPicker = (props) => {

    let {pickerVisible, data, onSelect, pickerOnClose, search, selectedValue} = props;

    const [pickerData, setPickerData] = useState(data);
    const [searchText, setSearchText] = useState("");

    useEffect(() => {
        setPickerData(data)
    }, [data]);

    const _search = (searchText) => {
        // to set entered text to the picker state
        setSearchText(searchText);

        if (searchText.length >= 3) {
            //search here!
            let searchedData = _MF.mowSearch(data, "title", searchText);
            setPickerData(searchedData);
        }

        if (searchText == "") {
            setPickerData(data);
        }
    };

    // to render picker items
    const renderItem = (item, key) => {

        let selected = item["id"] == selectedValue;

        return (

            <TouchableOpacity
                key={key}
                onPress={() => {
                    // to call on select method to let know the parent
                    onSelect(item);
                    // to set "" string value for setting as initial
                    setSearchText("");
                    // to set all data to the picker like as default
                    setPickerData(data);
                    // to let know the parent about pickerOnClose method called
                    pickerOnClose();
                }}
                style={{
                    marginTop: 10,
                    width: "100%",
                    flexDirection: "row",
                    padding: 10,
                    flex: 1,
                    paddingLeft: 20,
                    height: "100%"
                }}>

                <View
                    style={{width: "90%", flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>

                    <Text
                        style={{
                            fontSize: hp("2%"),
                            color: "grey",
                            fontWeight: "500",
                        }}>

                        {item["title"]}

                    </Text>

                    <View
                        style={{
                            width: hp(2),
                            height: hp(2),
                            borderRadius: 100,
                            backgroundColor: selected ? mowColors.mainColor : null,
                            borderColor: selected ? mowColors.mainColor : "#afafaf",
                            borderWidth: 1,
                            marginRight: 20
                        }}/>

                </View>

            </TouchableOpacity>
        )

    };

    return (

        <Dialog
            overlayBackgroundColor={"#454545"}
            width={deviceWidth * 0.9}
            height={hp(70)}
            visible={pickerVisible}
            dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}
            dialogStyle={{zIndex: 999, backgroundColor: "transparent", marginBottom: hp(5)}}>

            <View
                style={{height: "92%", backgroundColor: mowColors.pageBGColor}}>

                {/* search view */}
                {
                    search &&

                        <View>

                            <MowInput
                                value={searchText}
                                onChangeText={value => _search(value)}
                                multiline={false}
                                placeholder={mowStrings.search}
                                containerStyle={{
                                    borderWidth: 0,
                                    borderBottomWidth: 1,
                                    borderBottomColor: mowColors.text,
                                }}
                                placeHolderTextColor={"grey"}
                                leftIcon={"search"}/>

                        </View>
                }

                <View>

                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={pickerData}
                        renderItem={({item, key}) => renderItem(item, key)}/>

                </View>


            </View>

            <View
                style={{height: "8%", backgroundColor: "transparent"}}>

                {/* close button */}
                <TouchableOpacity
                    onPress={() => {
                        // to set "" string value for setting as initial
                        setSearchText("");
                        // to set all data to the picker like as default
                        setPickerData(data);
                        // to let know the parent picker close button clicked
                        pickerOnClose();
                    }}
                    style={{
                        position: "absolute",
                        bottom: 0,
                        alignItems: "center",
                        justifyContent: "center",
                        width: hp(5),
                        height: hp(5),
                        alignSelf: "center"
                    }}>

                    <FAIcon
                        style={{fontSize: hp(5), color: mowColors.pageBGColor}}
                        name={"times"}/>

                </TouchableOpacity>

            </View>


        </Dialog>
    );

};

MowPicker.propTypes = {
    search: PropTypes.bool,
    pickerVisible: PropTypes.bool,
    data: PropTypes.array,
    searchKey: PropTypes.string,
    onSelect: PropTypes.func,
    pickerOnClose: PropTypes.func,
    selectedValue: PropTypes.number,
};

MowPicker.defaultProps = {
    searchKey: "title",
    search: false
};
