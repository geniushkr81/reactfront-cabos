import DatePicker from "react-native-datepicker";
import React, {useState} from "react";
import PropTypes from 'prop-types';
import {View} from 'react-native';
import DeviceInformation from '../../../../device/DeviceInformation';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import FAIcon from "react-native-vector-icons/FontAwesome";
import {mowColors} from "../../../../values/Colors/MowColors";
import {mowStrings} from "../../../../values/Strings/MowStrings";

export const MowDatePicker = (props) => {

    let {leftIcon, textInputStyle, rightIcon, iconColor, containerStyle, selectedDate} = props;

    let language = new DeviceInformation().getLocaleLanguage();

    const [date, setDate] = useState("");

    return (

        <View
            style={{
                borderWidth: 0,
                flexDirection: "row",
                marginVertical: 10,
                height: hp(5.5),
                borderRadius: 5,
                alignSelf: "center",
                backgroundColor: "transparent",
                orderStyle: "solid",
                borderBottomWidth: 1,
                borderBottomColor: mowColors.mainColor,
                ...containerStyle
            }}>

            {
                leftIcon &&

                <View
                    style={{
                        flex: 3,
                        alignSelf: "center",
                        height: "100%",
                        alignItems: "center",
                        justifyContent: "center"
                    }}>

                    <FAIcon
                        name={leftIcon}
                        style={{
                            textAlign: "center",
                            fontSize: hp(2.5),
                            color: iconColor || mowColors.mainColor,
                        }}/>

                </View>
            }

            {/* dummy view*/}
            {
                !leftIcon &&

                <View
                    style={{flex: 3}}/>
            }

            <DatePicker
                style={{height: hp(5), flex: rightIcon ? 14 : 17}}
                date={date}
                locale={language}
                mode="date"
                showIcon={false}
                format="YYYY-MM-DD"
                confirmBtnText={mowStrings.button.choose}
                cancelBtnText={mowStrings.button.cancel}
                customStyles={{
                    dateInput: {
                        fontSize: hp(2),
                        borderWidth: 0,
                    },
                    dateText: {
                        width: "100%",
                        fontSize: hp(2),
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        textAlign: "left",
                        color: mowColors.mainColor,
                        height: "100%",
                        paddingTop: hp(1.2),
                        ...textInputStyle
                    },
                }}
                onDateChange={(date2) => {
                    // to set selected date
                    setDate(date2);
                    // to send selected date to the parent
                    selectedDate(date2);
                }}
            />

            {
                rightIcon &&

                <View
                    style={{
                        flex: 3,
                        alignSelf: "center",
                        height: "100%",
                        alignItems: "center",
                        justifyContent: "center"
                    }}>

                    <FAIcon
                        name={rightIcon}
                        style={{
                            textAlign: "center",
                            fontSize: hp(2.5),
                            color: iconColor || mowColors.mainColor,
                        }}/>

                </View>
            }

            {/* dummy view*/}
            {
                !rightIcon &&

                    <View
                        style={{flex: 3}}/>
            }

        </View>

    )

};

MowDatePicker.propTypes = {
    leftIcon: PropTypes.string,
    rightIcon: PropTypes.string,
    iconColor: PropTypes.string,
    containerStyle: PropTypes.object,
    textInputStyle: PropTypes.object,
    selectedDate: PropTypes.func,
};

MowDatePicker.defaultProps = {

};