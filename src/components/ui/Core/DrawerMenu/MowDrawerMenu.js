import React from "react";
import {View, TouchableOpacity, Text, ScrollView, Image} from "react-native";
import FeatherIcon from "react-native-vector-icons/Feather";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {closeDrawer, navigate} from "../../../../RootMethods/RootNavigation";
import {LoginContext} from "../../../../contexts/LoginContext";
import {User} from "../../../global/user/User";
import {mowColors} from "../../../../values/Colors/MowColors";
import {fontFamily} from "../../../../values/Styles/MowStyles";
import {mowStrings} from "../../../../values/Strings/MowStrings";
import {statusBarHeight} from "../../../../values/Constants/MowConstants";

export class MowDrawerMenu extends React.Component {

    static contextType = LoginContext;

    _handleLogout() {
        // to store user login status to the app locale
        let user = new User();
        user.setLogin(false);
        user.clearAllUserData();

        // to let know the navigator user has no more login
        this.context.setLogin(false);

        // to close drawer menu after logout
        closeDrawer();
        // to navigate the Home screen
        navigate("Home");
    }

    _goToProfilePage() {
        // navigate("ProfilePage")
    }

    _renderMenuItem(icon, text, navigateTo, logout = false) {
        return (

            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: hp("1.5%"),
                    marginVertical: hp("2%"),
                    marginLeft: wp("1%"),
                }}
                onPress={() => {
                    if (!logout) {
                        navigate(navigateTo);
                    }
                    else {
                        this._handleLogout();
                    }
                }}>

                <FeatherIcon
                    name={icon}
                    style={{
                        flex: 2,
                        fontSize: hp("2.5%"),
                        color: 'white',
                        textAlign: "center"
                    }}/>

                <Text
                    style={{
                        flex: 8,
                        fontSize: hp("2%"),
                        color: 'white',
                        marginLeft: 5,
                        fontWeight: "500",
                        fontStyle: "normal",
                        letterSpacing: 0,
                        fontFamily: fontFamily.medium
                    }}>

                    {text}

                </Text>

            </TouchableOpacity>

        )
    }

    render() {

        return (

            <View
                style={{flex: 1, backgroundColor: mowColors.mainColor, borderTopRightRadius: 50, borderBottomRightRadius: 50}}>

                {/* header view */}
                <View
                    style={{
                        marginTop: 10,
                        paddingTop: statusBarHeight,
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        alignSelf: 'center'
                    }}>

                    {/* profile image view */}
                    <View
                        style={{
                            height: hp(10.5),
                            width: hp(10.5),
                            borderRadius: 100,
                            borderWidth: 2,
                            borderColor: "white",
                            alignItems: "center",
                            justifyContent: "center"
                        }}>

                        <Image
                            style={{height: hp(10), width: hp(10)}}
                            resizeMode={"contain"}
                            source={require("../../../../assets/image/guest.png")}/>

                    </View>

                    {/* name text */}
                    <Text
                        style={{
                            marginTop: 20,
                            fontSize: hp(2),
                            fontWeight: "600",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            textAlign: "center",
                            color: "white"
                        }}>

                        info@mowega.com

                    </Text>

                    <Image
                        style={{width: "100%", marginTop: 20}}
                        resizeMode={"contain"}
                        source={require("../../../../assets/image/line.png")}/>

                </View>

                <ScrollView
                    style={{marginTop: hp("1%"), marginBottom: hp("1%")}}>

                    {/* settings button*/}
                    {/*this._renderMenuItem("settings", mowStrings.drawerMenu.settings, "Settings", false)*/}

                    {/* profile button */}
                    {/*this._renderMenuItem("user", mowStrings.drawerMenu.profile, "Profile", false)*/}

                    {/* password button */}
                    {/*this._renderMenuItem("lock", mowStrings.drawerMenu.password, "Password", false)*/}

                    {/* favorites button */}
                    {/*this._renderMenuItem("heart", mowStrings.drawerMenu.favorites, "Favorites", false)*/}

                    {/* notification button */}
                    {this._renderMenuItem("bell", mowStrings.drawerMenu.notification, "Notifications", false)}

                    {/* cart button */}
                    {/*this._renderMenuItem("shopping-bag", mowStrings.drawerMenu.cart, "Cart", false)*/}

                    {/* feedback button */}
                    {/*this._renderMenuItem("calendar", mowStrings.drawerMenu.feedback, "Feedback", false)*/}

                    {/* faq button */}
                    {this._renderMenuItem("message-square", mowStrings.drawerMenu.faq, "FAQ", false)}

                    {/* customer service button */}
                    {this._renderMenuItem("phone-call", mowStrings.drawerMenu.customerService, "", false)}

                    {/* privacy button */}
                    {this._renderMenuItem("shield", mowStrings.drawerMenu.privacy, "Privacy", false)}

                    {/* about us button */}
                    {this._renderMenuItem("info", mowStrings.drawerMenu.aboutUs, "AboutUs", false)}

                    {/* contact us button */}
                    {this._renderMenuItem("command", mowStrings.drawerMenu.contactUs, "ContactUs", false)}

                    {/* logout button */}
                    {this._renderMenuItem("log-out", mowStrings.drawerMenu.logout, "", true)}

                </ScrollView>

                {/*<Image
                    source={require("../../../../assets/logo/logo_white_horizontal.png")}
                    resizeMode={"contain"}
                    style={{
                        marginBottom: hp("3%"),
                        alignSelf: "center",
                        width: wp("40%"),
                        height: hp("5%")
                    }}/>
                */}
            </View>
        );
    }
}
