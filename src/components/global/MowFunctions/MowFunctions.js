class MowFunctions {

    mowSearch(fullData, searchKey, searchValue){
        let searchedData = [];
        fullData.find((el) => {
            if (el[searchKey].toLocaleLowerCase().search(searchValue.toLocaleLowerCase()) !== -1) {
                searchedData.push(el);
            }
        });
        return searchedData;
    }

    mowEmailValidationControl(inputText) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg.test(inputText) !== false;
    }

    mowInArray(dataArr, searchKey, searchValue){
        let flag = false;
        dataArr.find((obj) => {
            if (obj[searchKey].valueOf() == searchValue) {
                flag = true;
            }
        });

        return flag;
    }

}

export const _MF = new MowFunctions();
