/**
 *
 * @param title                     --> dialog title
 * @param message                   --> dialog message
 * @param bpt                       --> dialog Button Positive Text
 * @param bnt                       --> dialog Button Negative Text
 * @param twoButton                 --> dialog button number(boolean)
 * @param dialogType                --> dialog type
 * @returns {Promise<unknown>}
 */
import {mowStrings} from "../../../values/Strings/MowStrings";

export function successDialog(title, message, bpt = mowStrings.button.ok, bnt = "", twoButton = false) {

    return new Promise(function (resolve, reject) {

        /**
         *  type,
         *  title,
         *  message,
         *  positive button text,
         *  negative button text,
         *  positive button callback,
         *  negative button callback
         */
        __dialogContext.showDialog(
            "success",
            title,
            message,
            bpt,
            bnt,
            twoButton,
            function() {
                resolve();
            },
            function () {
                reject();
            }
        );

    });
}

export function warningDialog(title, message, bpt = mowStrings.button.ok, bnt = "", twoButton = false) {

    return new Promise(function (resolve, reject) {

        /**
         *  type,
         *  title,
         *  message,
         *  positive button text,
         *  negative button text,
         *  positive button callback,
         *  negative button callback
         */
        __dialogContext.showDialog(
            "warning",
            title,
            message,
            bpt,
            bnt,
            twoButton,
            function() {
                resolve();
            },
            function () {
                reject();
            }
        );

    });

}

export function errorDialog(title, message, bpt = mowStrings.button.ok, bnt = "", twoButton = false) {

    return new Promise(function (resolve, reject) {

        /**
         *  type,
         *  title,
         *  message,
         *  positive button text,
         *  negative button text,
         *  positive button callback,
         *  negative button callback
         */
        __dialogContext.showDialog(
            "error",
            title,
            message,
            bpt,
            bnt,
            twoButton,
            function() {
                resolve();
            },
            function () {
                reject();
            }
        );

    });

}
