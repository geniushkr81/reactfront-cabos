import {mowColors} from "../../../values/Colors/MowColors";

class Toast {

    duration = 1500;

    success(text, duration) {
        this.show(mowColors.successColor, text, duration);
    }

    error(text, duration) {
        this.show(mowColors.errorColor, text, duration);
    }

    info(text, duration) {
        this.show(mowColors.infoColor, text, duration);
    }

    warning(text, duration) {
        this.show(mowColors.warningColor, text, duration);
    }

    show(bgColor, text, duration) {
        if (!duration) duration = this.duration;

        __toastContext.showToast(
            bgColor,
            text,
            duration
        );

        setTimeout(() => {
            __toastContext.hideToast();
        }, duration);
    }
}

export const ShowToast = new Toast();
