import React, {Component} from 'react';
import {View, Image, Text, StatusBar} from 'react-native';
import SyncStorage from "sync-storage";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import CustomContextProvider from "./src/contexts/CustomContext";
import {mowColorFunction} from "./src/values/Colors/MowColors";
import {mowStrings} from "./src/values/Strings/MowStrings";
import {Router} from "./src/pages/Router/Router";
export default class App extends Component {

    state = {
        isReady: false
    };

    async componentDidMount() {

        console.disableYellowBox = true;

        // to init local storage data, for retrieving data when entered the app
        const data = await SyncStorage.init();

        // to set theme color according to the user selection
        // store data color
        //let color = SyncStorage.get("color");
        mowColorFunction("DARK");

        // to set selected language from user
        let lang = SyncStorage.get("language");

        if (!lang) {
            mowStrings.setLanguage("en");
        }
        else {
            // to update selected language
            mowStrings.setLanguage(lang);
        }

        try {
            // for showing custom splash screen
            window.setTimeout(() => {

                this.setState({
                    isReady: true,
                })
            }, 500)

        } catch (error) {
            // console.log('App.js error: ', error);
        }

    }

    render() {

        return (

            this.state.isReady !== true ?

                // splash ui here
                <View
                    style={{flex: 1, width: "100%", height: "100%", alignItems: "center", justifyContent: "center"}}>

                    <StatusBar hidden />

                    {/* logo view */}
                    <View
                        style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
{/*
                        <Image
                            style={{height: hp("25%"), width: wp("80%")}}
                            resizeMode={"contain"}
                            source={require("./src/assets/logo/logo_vertical.png")}/>
*/}
                    </View>

                </View>

                :

                // after timeout, go to router
                <CustomContextProvider>

                    <Router/>

                </CustomContextProvider>

        );
    }

}
